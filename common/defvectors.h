/*
 * defvectors.h
 *
 *  Created on: 14.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef DEFVECTOR_H
#define DEFVECTOR_H

#include "common/vector.h"
#include "math/vec2.h"
#include "math/vec3.h"
#include "math/vec4.h"
#include "math/mat4.h"
#include "math/quat.h"
#include "types/vertex.h"
#include "types/triangle.h"

VECTOR_Declare(Vec2_t);
VECTOR_Declare(Vec3_t);
VECTOR_Declare(Vec4_t);
VECTOR_Declare(Mat4_t);
VECTOR_Declare(Quat_t);
VECTOR_Declare(Vertex_t);
VECTOR_Declare(Triangle_t);
VECTOR_Declare(int16_t);
VECTOR_Declare(uint16_t);
VECTOR_Declare(int32_t);
VECTOR_Declare(uint32_t);
VECTOR_Declare(float);

#endif /* DEFVECTOR_H */
