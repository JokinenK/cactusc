/*
 * hash.h
 *
 *  Created on: 12.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef HASH_H
#define HASH_H

#include <string.h>
#include "common/types.h"

/*-----------------------------------------------
 * MACRO DEFINITIONS
 *-----------------------------------------------
 */

#define HASH_Const(str)     (HASH_String(STRINGIFY(str)))

#define HASH_String(str)    (HASH_StringWithLength(str, strlen(str)))

/*-----------------------------------------------
 * PUBLIC METHOD DECLARATIONS
 *-----------------------------------------------
 */

/**
 * Generates hash from input string
 * See: http://www.cse.yorku.ca/~oz/hash.html
 *
 * @param[in] str   Input string to hash
 * @return          Calculated hash
 */
static inline uint32_t HASH_StringWithLength(const char* str, uint32_t len)
{
    uint32_t hash = 5381;

    while (len > 0) {
        /* hash * 33 + c */
        hash = ((hash << 5) + hash) + (uint32_t)(*str++);
        len--;
    }

    return hash;
}

#endif /* HASH_H */
