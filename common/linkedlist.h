/*
 * linkedlist.h
 *
 *  Created on: 7.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include "common/types.h"

#define LINKEDLIST_Prepend(list, node)                                      \
    LINKEDLIST_PrependWithField(list, node, next)

#define LINKEDLIST_PrependWithField(list, node, next)                       \
    do {                                                                    \
        (node)->next = (list);                                              \
        (list) = (node);                                                    \
    } while(0);


#define LINKEDLIST_Append(list, node)                                       \
    LINKEDLIST_AppendWithField(list, node, next)

#define LINKEDLIST_AppendWithField(list, node, next)                        \
    do {                                                                    \
        if (list) {                                                         \
            (node)->next = list;                                            \
                                                                            \
            while((node)->next->next) {                                     \
                (node)->next = (node)->next->next;                          \
            }                                                               \
                                                                            \
            (node)->next->next = (node);                                    \
        }                                                                   \
        else {                                                              \
            (list) = (node);                                                \
        }                                                                   \
                                                                            \
        (node)->next = NULL;                                                \
    } while(0);


#define LINKEDLIST_Delete(list, del)                                        \
    LINKEDLIST_DeleteWithField(list, del, next)

#define LINKEDLIST_DeleteWithField(list, del, next)                         \
    do {                                                                    \
        if ((list) == (del)) {                                              \
            (list)=(list)->next;                                            \
        } else {                                                            \
            char* listTmp = (char*)(list);                                  \
                                                                            \
            while ((list)->next                                             \
                    && ((list)->next != (del))) {                           \
                list = (list)->next;                                        \
            }                                                               \
            if ((list)->next) {                                             \
                (list)->next = ((del)->next);                               \
            }                                                               \
            {                                                               \
                char **listAlias = (char**)&(list);                         \
                *listAlias = listTmp;                                       \
            }                                                               \
        }                                                                   \
    } while (0)

#define LINKEDLIST_ForEach(list, node)                                      \
    LINKEDLIST_ForEachWithField(list, node, next)

#define LINKEDLIST_ForEachWithField(list, node, next)                       \
    for (node = list; node != NULL; node = (node)->next)

#define LINKEDLIST_Count(list, node, counter)                               \
    LINKEDLIST_CountWithField(list, node, counter, next)

#define LINKEDLIST_CountWithField(list, node, counter, next)                \
    do {                                                                    \
        counter = 0;                                                        \
        LINKEDLIST_ForEachWithField(list, node, next) {                     \
            counter++;                                                      \
        }                                                                   \
    } while(0);


#define LINKEDLIST_SearchScalar(list, node, field, value)                   \
    LINKEDLIST_SearchScalarWithField(list, node, field, value, next)

#define LINKEDLIST_SearchScalarWithField(list, node, field, value, next)    \
    do {                                                                    \
        LINKEDLIST_ForEachWithField(list, node, next) {                     \
            if ((node)->field == (value)) {                                 \
                break;                                                      \
            }                                                               \
        }                                                                   \
    } while(0)


#define LINKEDLIST_Search(list, node, result, cmp)                          \
    LINKEDLIST_SearchWithField(list, node, result, cmp, next)

#define LINKEDLIST_SearchWithField(list, node, result, cmp, next)           \
    do {                                                                    \
        LINKEDLIST_ForEachWithField(list, result, next) {                   \
            if ((cmp(result, node)) == TRUE) break;                         \
        }                                                                   \
    } while(0)


#endif /* LINKEDLIST_H */
