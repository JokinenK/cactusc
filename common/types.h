/*
 * types.h
 *
 *  Created on: 7.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef TYPES_H
#define TYPES_H

#include <stdint.h>
#include <math.h>

#define ARRAY_SIZE(x)                   (sizeof(x) / sizeof(x[0]))
#define MIN(a, b)                       (a < b ? a : b)
#define MAX(a, b)                       (a > b ? a : b)
#define CLAMP(val, min, max)            (MAX(MIN(val, min), max)))
#define INDEX_VALIDATE(i, size, value)  (i < 0 ? NULL : i >= size ? NULL : value)
#define STRINGIFY(str)                  (#str)
#define CONSIDER_SAME                   (1e-5)
#define FLOATS_EQ(a, b)                 (fabs((a) - (b)) < CONSIDER_SAME)
#define FLOATS_NEQ(a, b)                (!(FLOATS_EQ(a, b)))
#define DEGREES_TO_RADIANS(degrees)     ((M_PI * degrees) / 180.0f)
#define RADIANS_TO_DEGREES(radians)     (radians * (180.0f / M_PI))

#define NEW(type)                       ((type*)calloc(1, sizeof(type)))
#define DELETE(obj)                     (free(obj))

typedef enum {
	FALSE = 0,
	TRUE = !FALSE
} boolean;

#endif /* TYPES_H */
