/*
 * vector.h
 *
 *  Created on: 14.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef VECTOR_H
#define VECTOR_H

#include "common/types.h"
#include <stdlib.h>
#include <string.h>

#define VECTOR_INCREASE_CAPACITY 5
#define VECTOR(T)  VECTOR_##T


#define VECTOR_Create(T, var)                                               \
    VECTOR(T) var;                                                          \
    VECTOR_Init(T, var);


#define VECTOR_Declare(T)                                                   \
    typedef struct VECTOR(T) {                                              \
        uint32_t size;                                                      \
        uint32_t capasity;                                                  \
        uint32_t itemSize;                                                  \
        T* items;                                                           \
    } VECTOR(T)


#define VECTOR_Init(T, vector)                                              \
    do {                                                                    \
        (vector).size = 0;                                                  \
        (vector).capasity = VECTOR_INCREASE_CAPACITY;                       \
        (vector).itemSize = sizeof(T);                                      \
        (vector).items = (T*)(calloc(                                       \
                (vector).capasity,                                          \
                (vector).itemSize));                                        \
    } while(0);


#define VECTOR_Free(vector)                                                 \
    do {                                                                    \
        free((vector).items);                                               \
    } while(0);


#define VECTOR_Data(vector)                                                 \
    (vector).items


#define VECTOR_Push(T, vector, item)                                        \
    do {                                                                    \
        if ((vector).size == (vector).capasity) {                           \
            (vector).capasity += VECTOR_INCREASE_CAPACITY;                  \
                                                                            \
            T* newItems = (T*)(realloc(                                     \
                    (vector).items,                                         \
                    (vector).capasity * (vector).itemSize));                \
                                                                            \
            if (newItems != NULL) {                                         \
                (vector).items = newItems;                                  \
            }                                                               \
        }                                                                   \
        memcpy(                                                             \
            &(vector).items[(vector).size],                                 \
            &(item),                                                         \
            (vector).itemSize);                                             \
                                                                            \
        (vector).size++;                                                    \
    } while(0);


#define VECTOR_Replace(vector, index, item)                                 \
    do {                                                                    \
        if (index >= 0 && index < (vector).size) {                          \
            memcpy(                                                         \
                (vector).items + index,                                     \
                (item),                                                     \
                (vector).itemSize);                                         \
        }                                                                   \
    } while(0);


#define VECTOR_At(vector, index)                                            \
    (INDEX_VALIDATE(index, (vector).size, (vector).items + index))


#define VECTOR_Size(vector)                                                 \
    ((vector).size)


#define VECTOR_ForEach(vector, item)                                        \
    for (                                                                   \
        item = (vector).items;                                              \
        item < (vector).items + (vector).size;                              \
        item++)


#define VECTOR_Find(vector, item, out, cmp)                                 \
    do {                                                                    \
        boolean foundMatch = FALSE;                                         \
        VECTOR_ForEach(vector, out) {                                       \
            if (cmp(item, out)) {                                           \
                foundMatch = TRUE;                                          \
                break;                                                      \
            }                                                               \
        }                                                                   \
        if (!foundMatch) {                                                  \
            out = NULL;                                                     \
        }                                                                   \
    } while(0);


#define VECTOR_IndexOf(T, vector, item, index, cmp)                         \
    do {                                                                    \
        T* loopItem;                                                        \
        boolean foundMatch = FALSE;                                         \
        VECTOR_Foreach(vector, out) {                                       \
            if (cmp(item, loopItem)) {                                      \
                foundMatch = TRUE;                                          \
                break;                                                      \
            }                                                               \
            index++;                                                        \
        }                                                                   \
        if (!foundMatch) {                                                  \
            index = -1;                                                     \
        }                                                                   \
    } while(0);


#define VECTOR_RemoveIndex(vector, index)                                   \
    do {                                                                    \
        if (index >= 0 && index < (vector).size) {                          \
            uint32_t itemsToMove = (vector).size - index - 1;               \
                                                                            \
            memmove((vector).items + index,                                 \
                    (vector).items + index + 1,                             \
                    (vector).itemSize * itemsToMove);                       \
        }                                                                   \
    } while(0);


#define VECTOR_Remove(T, vector, item, cmp)                                 \
    do {                                                                    \
        int32_t itemIndex;                                                  \
        VECTOR_IndexOf(T, vector, item, itemIndex, cmp);                    \
        VECTOR_RemoveIndex(vector, itemIndex);                              \
    } while(0);


#endif /* VECTOR_H */
