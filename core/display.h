/*
 * display.h
 *
 *  Created on: 7.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef DISPLAY_H
#define DISPLAY_H

#include "common/types.h"

typedef struct Display_s {
    /**
     * Initializes the display
     *
     * @param[in]   handle  Display instance
     * @param[in]   title   Title for the window
     * @param[in]   width   Width of the window
     * @param[in]   height  Height of the window
     */
    void (*init)(
            struct Display_s* handle,
            char* title,
            uint32_t width,
            uint32_t height);

    /**
     * Releases the display
     *
     * @param[in]   handle  Display instance
     */
    void (*release)(struct Display_s* handle);

    /**
     * Swaps the buffer (double buffering)
     *
     * @param[in]   handle  Display instance
     */
    void (*swapBuffers)(const struct Display_s* handle);

    /**
     * Checks if the window is closed
     *
     * @param[in]   handle  Display instance
     * @return      TRUE    Window is closed
     *              FALSE   Window is still open
     */
    boolean (*isClosed)(const struct Display_s* handle);

    /**
     * Returns the width of the screen
     *
     * @param[in]   handle  Display instance
     * @return              Width of the window
     */
    uint32_t (*width)(const struct Display_s* handle);

    /**
     * Returns the height of the screen
     *
     * @param[in]   handle  Display instance
     * @return              Height of the window
     */
    uint32_t (*height)(const struct Display_s* handle);

    /**
     * Calculates the aspect ratio of the given resolution
     *
     * @param[in]   handle  Display instance
     * @return              Aspect ratio of the window
     */
    float (*aspectRatio)(const struct Display_s* handle);

} Display_t;

#define DISPLAY_Init(display, title, width, height) \
    display->init(display, title, width, height)

#define DISPLAY_Release(display) \
    display->release(display)

#define DISPLAY_SwapBuffers(display) \
    display->swapBuffers(display)

#define DISPLAY_IsClosed(display) \
    display->isClosed(display)

#define DISPLAY_Width(display) \
    display->width(display)

#define DISPLAY_Height(display) \
    display->height(display)

#define DISPLAY_AspectRatio(display) \
    display->aspectRatio(display)

#endif /* DISPLAY_H */
