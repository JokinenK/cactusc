#include "factory.h"
#include "core/sdl/display_sdl.h"
#include "core/sdl/input_sdl.h"
#include "renderer/opengl/openglrenderer.h"
#include "renderer/opengl/opengltexture.h"
#include "common/linkedlist.h"

/*-----------------------------------------------
 * MACRO DEFINITIONS
 *-----------------------------------------------
 */

#define FACTORYDISPLAY_Init SDLDISPLAY_Init
#define FACTORYINPUT_Init SDLINPUT_Init
#define FACTORYRENDERER_Init OPENGLRENDERER_Init
#define FACTORYTEXTURE_Init OPENGLTEXTURE_Init

/*-----------------------------------------------
 * PRIVATE TYPE DEFINITIONS
 *-----------------------------------------------
 */

typedef SDL_Display_t FACTORY_Display_t;
typedef SDL_Input_t FACTORY_Input_t;
typedef OPENGL_Renderer_t FACTORY_Renderer_t;
typedef OPENGL_Texture_t FACTORY_Texture_t;

/*-----------------------------------------------
 * PRIVATE VARIABLE DEFINITIONS
 *-----------------------------------------------
 */

static FACTORY_Display_t f_display;
static FACTORY_Input_t f_input;
static FACTORY_Renderer_t f_renderer;
static Texture_t* f_textureList = NULL;

/*-----------------------------------------------
 * PUBLIC METHOD IMPLEMENTATIONS
 *-----------------------------------------------
 */

Display_t* FACTORY_Display()
{
    return FACTORYDISPLAY_Init(&f_display);
}

Input_t* FACTORY_Input()
{
    return FACTORYINPUT_Init(&f_input);
}

Renderer_t* FACTORY_Renderer()
{
    return FACTORYRENDERER_Init(&f_renderer);
}

Texture_t* FACTORY_Texture()
{
    FACTORY_Texture_t* factoryTexture = NEW(FACTORY_Texture_t);
    Texture_t* texture = FACTORYTEXTURE_Init(factoryTexture);
    LINKEDLIST_Prepend(f_textureList, texture);
    return texture;
}

void FACTORY_Release()
{
    Texture_t* list = f_textureList;
    Texture_t* current = NULL;

    /**
     * Cannot use basic foreach here, must
     * read next before freeing previous
     */
    while (list != NULL) {
        current = list;
        list = list->next;

        free(current);
    }

    INPUT_Release(((Input_t*)&f_input));
    DISPLAY_Release(((Display_t*)&f_display));
}
