#ifndef FACTORY_H
#define FACTORY_H

#include <SDL_scancode.h>
#include "common/types.h"
#include "core/display.h"
#include "core/input.h"
#include "renderer/renderer.h"
#include "renderer/texture.h"

extern Display_t* FACTORY_Display();

extern Input_t* FACTORY_Input();

extern Renderer_t* FACTORY_Renderer();

extern Texture_t* FACTORY_Texture();

extern void FACTORY_Release();

#endif /* FACTORY_H */
