/*
 * fileio.c
 *
 *  Created on: 12.3.2016
 *      Author: Kalle Jokinen
 */

#include "fileio.h"
#include <stdio.h>
#include <errno.h>

/*-----------------------------------------------
 * PRIVATE METHOD DECLARATIONS
 *-----------------------------------------------
 */

static FILEIO_Status_t ReadLastError();

/*-----------------------------------------------
 * PUBLIC METHOD DEFINITIONS
 *-----------------------------------------------
 */

char* FILEIO_ReadFile(
        const char* filepath,
        uint32_t* outLength,
        FILEIO_Status_t* outStatus)
{
    FILEIO_Status_t status = FILEIO_OK;
    FILE* handle = fopen(filepath, "r");
    char* buffer = NULL;
    uint32_t length = 0;

    if (handle != NULL) {
        fseek(handle, 0, SEEK_END);
        length = ftell(handle);
        rewind(handle);

        buffer = (char*)(calloc(length, sizeof(char)));

        if (buffer != NULL) {
            fread(buffer, 1, length, handle);
        }
        else {
            status = FILEIO_MEM_ALLOC_FAILED;
        }
    }
    else {
        status = ReadLastError();
    }

    if (outLength != NULL) {
        *outLength = length;
    }

    if (outStatus != NULL) {
        *outStatus = status;
    }

    return buffer;
}

FILEIO_Status_t FILEIO_WriteFile(
        const char* filepath,
        const char* buffer,
        uint32_t bufferLen)
{
    FILEIO_Status_t status = FILEIO_OK;
    FILE* handle = fopen(filepath, "w");

    if (handle != NULL) {
        uint32_t bytesWritten = fwrite(buffer, 1, bufferLen, handle);

        if (bytesWritten != bufferLen) {
            status = FILEIO_WRITE_FAILURE;
        }
    }
    else {
        status = ReadLastError();
    }

    return status;
}

FILEIO_Status_t FILEIO_DeleteFile(
        const char* filepath)
{
    FILEIO_Status_t status = FILEIO_OK;

    if (unlink(filepath) != 0) {
        status = ReadLastError();
    }

    return status;
}

/*-----------------------------------------------
 * PRIVATE METHOD DEFINITIONS
 *-----------------------------------------------
 */

static FILEIO_Status_t ReadLastError()
{
    FILEIO_Status_t status;
    uint32_t errorNumber = errno;

    switch (errorNumber) {
    case EPERM:
    case EACCES:
        status = FILEIO_PERMISSION_DENIED;
        break;
    case EBUSY:
        status = FILEIO_FILE_BUSY;
        break;
    case ENOENT:
        status = FILEIO_FILE_NOT_FOUND;
        break;
    case EROFS:
        status = FILEIO_READ_ONLY;
        break;
    default:
        status = FILEIO_UNKNOWN_ERROR;
    }

    return status;
}

