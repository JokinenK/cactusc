/*
 * fileio.h
 *
 *  Created on: 12.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef FILEIO_H
#define FILEIO_H

#include "common/types.h"
#include <stdlib.h>

typedef enum {
    FILEIO_OK,
    FILEIO_FILE_BUSY,
    FILEIO_FILE_NOT_FOUND,
    FILEIO_WRITE_FAILURE,
    FILEIO_PERMISSION_DENIED,
    FILEIO_MEM_ALLOC_FAILED,
    FILEIO_READ_ONLY,
    FILEIO_UNKNOWN_ERROR
} FILEIO_Status_t;

extern char* FILEIO_ReadFile(
        const char* filepath,
        uint32_t* length,
        FILEIO_Status_t* status);

extern FILEIO_Status_t FILEIO_WriteFile(
        const char* filepath,
        const char* buffer,
        uint32_t bufferLen);

extern FILEIO_Status_t FILEIO_DeleteFile(
        const char* filepath);

#endif /* FILEIO_H */
