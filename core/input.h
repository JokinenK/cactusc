/*
 * input.h
 *
 *  Created on: 7.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef INPUT_H
#define INPUT_H

#include "common/types.h"

typedef struct Input_s {
    /**
     * Initializes the input
     *
     * @param[in]   input           Input instance
     */
    void (*init)(struct Input_s* input);

    /**
     * Releases the input
     *
     * @param[in]   input           Input instance
     */
    void (*release)(struct Input_s* input);

    /**
     * Requests update for the input
     *
     * @param[in]   input           Input instance
     */
    void (*update)(struct Input_s* input);

    /**
     * Checks if the key is just pressed
     *
     * @param[in]   input           Input instance
     * @param[in]   keyCode         Code of the key to be checked
     * @return      TRUE            Key is pressed
     *              FALSE           Key is not pressed
     */
    boolean (*isKeyPressed)(struct Input_s* input, int keyCode);

    /**
     * Checks if the key is held down
     *
     * @param[in]   input           Input instance
     * @param[in]   keyCode         Code of the key to be checked
     * @return      TRUE            Key is held down
     *              FALSE           Key is not held down
     */
    boolean (*isKeyDown)(struct Input_s* input, int keyCode);

    /**
     * Checks if the key was just released
     *
     * @param[in]   input           Input instance
     * @param[in]   keyCode         Code of the key to be checked
     * @return      TRUE            Key was just released
     *              FALSE           Key wasn't just released
     */
    boolean (*isKeyUp)(struct Input_s* input, int keyCode);

    /**
     * Checks if the mouse button is just pressed
     *
     * @param[in]   input           Input instance
     * @param[in]   button          Mouse button number
     * @return      TRUE            Mouse button is pressed
     *              FALSE           Mouse button is not pressed
     */
    boolean (*isMouseButtonPressed)(struct Input_s* input, int button);

    /**
     * Checks if the mouse button is held down
     *
     * @param[in]   input           Input instance
     * @param[in]   button          Mouse button number
     * @return      TRUE            Mouse button is held down
     *              FALSE           Mouse button is not held down
     */
    boolean (*isMouseButtonDown)(struct Input_s* input, int button);

    /**
     * Checks if the mouse button was just released
     *
     * @param[in]   input           Input instance
     * @param[in]   button          Mouse button number
     * @return      TRUE            Mouse button was just released
     *              FALSE           Mouse button wasn't just released
     */
    boolean (*isMouseButtonUp)(struct Input_s* input, int button);

    /**
     * Reads the current mouse coordinates
     *
     * @param[in]   input           Input instance
     * @param[out]  mouseX          Mouse X coordinate
     * @param[out]  mouseY          Mouse Y coordinate
     */
    void (*mousePosition)(
            struct Input_s* input,
            uint32_t* mouseX,
            uint32_t* mouseY);

    /**
     * Reads the mouse delta coordinates
     *
     * @param[in]   input           Input instance
     * @param[out]  mouseDeltaX     Change in mouse X coordinate
     * @param[out]  mouseDeltaY     Change in mouse Y coordinate
     */
    void (*mouseDeltaPosition)(
            struct Input_s* input,
            uint32_t* mouseDeltaX,
            uint32_t* mouseDeltaY);
} Input_t;

#define INPUT_Init(input) \
    input->init(input)

#define INPUT_Release(input) \
    input->release(input)

#define INPUT_Update(input) \
    input->update(input)

#define INPUT_IsKeyPressed(input, keyCode) \
    input->isKeyPressed(input, keyCode)

#define INPUT_IsKeyDown(input, keyCode) \
    input->isKeyDown(input, keyCode)

#define INPUT_IsKeyUp(input, keyCode) \
    input->isKeyUp(input, keyCode)

#define INPUT_IsMouseButtonPressed(input, keyCode) \
    input->isMouseButtonPressed(input, keyCode)

#define INPUT_IsMouseButtonDown(input, keyCode) \
    input->isMouseButtonDown(input, keyCode)

#define INPUT_IsMouseButtonUp(input, keyCode) \
    input->isMouseButtonUp(input, keyCode)

#define INPUT_MousePosition(input, x, y) \
    input->mousePosition(input, x, y)

#define INPUT_MouseDeltaPosition(input, x, y) \
    input->mouseDeltaPosition(input, x, y)

#endif /* INPUT_H */
