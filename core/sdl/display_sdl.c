/*
 * display.h
 *
 *  Created on: 7.3.2016
 *      Author: Kalle Jokinen
 */

#include "core/display.h"
#include "core/sdl/display_sdl.h"

/*-----------------------------------------------
 * PRIVATE METHOD DECLARATIONS
 *-----------------------------------------------
 */

static void Init(
        Display_t* display,
        char* title,
        uint32_t width,
        uint32_t height);

static void Release(Display_t* handle);

static void SwapBuffers(const Display_t* handle);

static boolean IsClosed(const Display_t* handle);

static uint32_t Width(const Display_t* handle);

static uint32_t Height(const Display_t* handle);

static float AspectRatio(const Display_t* handle);

/*-----------------------------------------------
 * PUBLIC METHOD IMPLEMENTATIONS
 *-----------------------------------------------
 */

Display_t* SDLDISPLAY_Init(SDL_Display_t* handle)
{
    Display_t* display = &handle->parent.display;

    if (display) {
        display->init = Init;
        display->release = Release;
        display->swapBuffers = SwapBuffers;
        display->isClosed = IsClosed;
        display->width = Width;
        display->height = Height;
        display->aspectRatio = AspectRatio;

        return display;
    }

    return 0;
}

/*-----------------------------------------------
 * PRIVATE METHOD IMPLEMENTATIONS
 *-----------------------------------------------
 */

void Init(
        Display_t* handle,
        char* title,
        uint32_t width,
        uint32_t height)
{
    SDL_Display_t* impl = (SDL_Display_t*)handle;

    if (impl) {
        impl->closed = FALSE;
        impl->width = width;
        impl->height = height;

        SDL_Init(SDL_INIT_EVERYTHING);

        SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

        impl->window = SDL_CreateWindow(
            title,
            SDL_WINDOWPOS_CENTERED,
            SDL_WINDOWPOS_CENTERED,
            width,
            height,
            SDL_WINDOW_OPENGL);

        impl->glContext = SDL_GL_CreateContext(impl->window);
    }
}

void Release(Display_t* handle)
{
    SDL_Display_t* impl = (SDL_Display_t*)handle;

    if (impl) {
        SDL_GL_DeleteContext(impl->glContext);
        SDL_DestroyWindow(impl->window);
        SDL_Quit();
    }
}

void SwapBuffers(const Display_t* handle)
{
    SDL_Display_t* impl = (SDL_Display_t*)handle;

    if (impl) {
        // Swap buffers (double buffering)
        SDL_GL_SwapWindow(impl->window);

        // Store event here
        SDL_Event evt;

        // Update events, look for quit event
        SDL_PumpEvents();
        SDL_PeepEvents(&evt, 1, SDL_PEEKEVENT, SDL_QUIT, SDL_QUIT);

        if (evt.type == SDL_QUIT)
        {
            // Quit requested
            impl->closed = TRUE;
        }
    }
}

boolean IsClosed(const Display_t* handle)
{
    SDL_Display_t* impl = (SDL_Display_t*)handle;
    boolean closed = TRUE;

    if (impl) {
        closed = impl->closed;
    }

    return closed;
}

uint32_t Width(const Display_t* handle)
{
    SDL_Display_t* impl = (SDL_Display_t*)handle;
    uint32_t width = 0;

    if (impl) {
        width = impl->width;
    }

    return width;
}

uint32_t Height(const Display_t* handle)
{
    SDL_Display_t* impl = (SDL_Display_t*)handle;
    uint32_t height = 0;

    if (impl) {
        height = impl->height;
    }

    return height;
}

float AspectRatio(const Display_t* handle)
{
    SDL_Display_t* impl = (SDL_Display_t*)handle;
    float ratio = 0;

    if (impl) {
        ratio = ((float)impl->width / (float)impl->height);
    }

    return ratio;
}
