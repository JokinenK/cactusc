/*
 * display.h
 *
 *  Created on: 7.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef DISPLAY_SDL_H
#define DISPLAY_SDL_H

#include <SDL.h>
#include "core/display.h"

typedef struct {
    union {
        Display_t display;
    } parent;

    SDL_Window* window;
    SDL_GLContext glContext;
    uint32_t width;
    uint32_t height;
    boolean closed;
} SDL_Display_t;

/**
 * Initializes the display object
 *
 * @param[in]   handle  SDL Display object to initialize
 * @return              Initialized display object
 */
extern Display_t* SDLDISPLAY_Init(SDL_Display_t* handle);

#endif /* DISPLAY_SDL_H */
