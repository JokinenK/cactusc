/*
 * handle_sdl.h
 *
 *  Created on: 7.3.2016
 *      Author: Kalle Jokinen
 */

#include "core/sdl/input_sdl.h"
#include <stdlib.h>
#include <string.h>

/*-----------------------------------------------
 * PRIVATE METHOD DECLARATIONS
 *-----------------------------------------------
 */

static void Init(Input_t* handle);

static void Release(Input_t* handle);

static void Update(Input_t* handle);

static boolean IsKeyPressed(Input_t* handle, int keyCode);

static boolean WasKeyPressed(Input_t* handle, int keyCode);

static boolean IsKeyDown(Input_t* handle, int keyCode);

static boolean IsKeyUp(Input_t* handle, int keyCode);

static boolean IsMouseButtonPressed(Input_t* handle, int button);

static boolean WasMouseButtonPressed(Input_t* handle, int button);

static boolean IsMouseButtonDown(Input_t* handle, int button);

static boolean IsMouseButtonUp(Input_t* handle, int button);

static void MousePosition(
        Input_t* handle,
        uint32_t* mouseX,
        uint32_t* mouseY);

static void MouseDeltaPosition(
        Input_t* handle,
        uint32_t* mouseDeltaX,
        uint32_t* mouseDeltaY);

/*-----------------------------------------------
 * PUBLIC METHOD IMPLEMENTATIONS
 *-----------------------------------------------
 */

Input_t* SDLINPUT_Init(SDL_Input_t* handle)
{
    Input_t* input = &handle->parent.input;

    if (input) {
        input->init = Init;
        input->update = Update;
        input->release = Release;

        input->isKeyPressed = IsKeyPressed;
        input->isKeyDown = IsKeyDown;
        input->isKeyUp = IsKeyUp;

        input->isMouseButtonPressed = IsMouseButtonPressed;
        input->isMouseButtonDown = IsMouseButtonDown;
        input->isMouseButtonUp = IsMouseButtonUp;

        input->mousePosition = MousePosition;
        input->mouseDeltaPosition = MouseDeltaPosition;

        return input;
    }

    return 0;
}

/*-----------------------------------------------
 * PRIVATE METHOD IMPLEMENTATIONS
 *-----------------------------------------------
 */

void Init(Input_t* handle)
{
    SDL_Input_t* impl = (SDL_Input_t*)handle;

    if (impl) {
        impl->keyStates = SDL_GetKeyboardState(&impl->numKeys);

        impl->lastKeyStates =
                (uint8_t*) malloc(sizeof(uint8_t) * impl->numKeys);

        memset(
                impl->lastKeyStates,
                0,
                sizeof(uint8_t) * impl->numKeys);

        impl->lastMouseState = 0;
    }
}

void Update(Input_t* handle)
{
    SDL_Input_t* impl = (SDL_Input_t*)handle;

    if (impl) {
        /* Copy key states */
        memcpy(
                impl->lastKeyStates,
                impl->keyStates,
                impl->numKeys);

        /* Copy mouse button states */
        impl->lastMouseState = impl->mouseState;
        impl->mousePrevX = impl->mouseX;
        impl->mousePrevY = impl->mouseY;

        /* Pump events so keyboard and mouse state gets updated */
        SDL_PumpEvents();

        /* Update mouse state and position */
        impl->mouseState =
                SDL_GetMouseState(&impl->mouseX, &impl->mouseY);
    }
}

void Release(Input_t* handle)
{
    SDL_Input_t* impl = (SDL_Input_t*)handle;

    if (impl) {
        free(impl->lastKeyStates);
    }
}

boolean IsKeyPressed(Input_t* handle, int keyCode)
{
    SDL_Input_t* impl = (SDL_Input_t*)handle;

    if (impl) {
        return (impl->keyStates[keyCode] == 1);
    }

    return FALSE;
}

boolean WasKeyPressed(Input_t* handle, int keyCode)
{
    SDL_Input_t* impl = (SDL_Input_t*)handle;

    if (impl) {
        return (impl->lastKeyStates[keyCode] == 1);
    }

    return FALSE;
}

boolean IsKeyDown(Input_t* handle, int keyCode)
{
    return IsKeyPressed(handle, keyCode)
            && WasKeyPressed(handle, keyCode);
}

boolean IsKeyUp(Input_t* handle, int keyCode)
{
    return !IsKeyPressed(handle, keyCode)
            && WasKeyPressed(handle, keyCode);
}

boolean IsMouseButtonPressed(Input_t* handle, int button)
{
    SDL_Input_t* impl = (SDL_Input_t*)handle;

    if (impl) {
        return ((impl->mouseState & SDL_BUTTON(button)) == 1);
    }

    return FALSE;
}

boolean WasMouseButtonPressed(Input_t* handle, int button)
{
    SDL_Input_t* impl = (SDL_Input_t*)handle;

    if (impl) {
        return ((impl->lastMouseState & SDL_BUTTON(button)) == 1);
    }

    return FALSE;
}

boolean IsMouseButtonDown(Input_t* handle, int button)
{
    return IsMouseButtonPressed(handle, button)
            && !WasMouseButtonPressed(handle, button);
}

boolean IsMouseButtonUp(Input_t* handle, int button)
{
    return !IsMouseButtonPressed(handle, button)
            && WasMouseButtonPressed(handle, button);
}

void MousePosition(
        Input_t* handle,
        uint32_t* mouseX,
        uint32_t* mouseY)
{
    SDL_Input_t* impl = (SDL_Input_t*)handle;

    if (impl) {
        *mouseX = impl->mouseX;
        *mouseY = impl->mouseY;
    }
}

void MouseDeltaPosition(
        Input_t* handle,
        uint32_t* mouseDeltaX,
        uint32_t* mouseDeltaY)
{
    SDL_Input_t* impl = (SDL_Input_t*)handle;

    if (impl) {
        *mouseDeltaX = (impl->mousePrevX - impl->mouseX);
        *mouseDeltaY = (impl->mousePrevY - impl->mouseY);
    }
}
