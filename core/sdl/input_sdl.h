/*
 * input_sdl.h
 *
 *  Created on: 7.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef INPUT_SDL_H
#define INPUT_SDL_H

#include <SDL.h>
#include "core/input.h"

typedef struct {
    union {
        Input_t input;
    } parent;

    int numKeys;

    const uint8_t* keyStates;
    uint8_t* lastKeyStates;

    int mouseState;
    int lastMouseState;

    uint32_t mouseX;
    uint32_t mouseY;

    uint32_t mousePrevX;
    uint32_t mousePrevY;
} SDL_Input_t;

/**
 * Initializes the input object
 *
 * @param   input   Input instance to initialize
 * @return          Initialized input instance
 */
extern Input_t* SDLINPUT_Init(SDL_Input_t* input);

#endif /* INPUT_SDL_H */
