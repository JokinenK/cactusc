set(TARGET "CactusEngine")
project(${TARGET})

set(SOURCES
    gameobject.c
    customstring.c
    transform.c
    timecalc.c
    components/component.c
    components/camera.c
    components/material.c
    components/mesh.c
    components/meshrenderer.c
    components/light/light.c
)

include_directories(
    ${TARGET_PATHS}
)

add_library(${TARGET}-static ${SOURCES})
target_link_libraries(${TARGET}-static
    CactusMath-static
)