/*
 * camera.c
 *
 *  Created on: 17.3.2016
 *      Author: Kalle Jokinen
 */

#include <stdio.h>
#include "camera.h"
#include "engine/transform.h"
#include "engine/gameobject.h"

/*-----------------------------------------------
 * PRIVATE METHOD DECLARATIONS
 *-----------------------------------------------
 */

static void Start(Component_t* parent);

/*-----------------------------------------------
 * PUBLIC METHOD DEFINITIONS
 *-----------------------------------------------
 */

Component_t* CAMERA_Init(
        Camera_t* instance,
        float fov,
        float aspect,
        float near,
        float far)
{
    Component_t* parent = NULL;

    if (instance != NULL) {
        parent = COMPONENT_Init(&instance->parent.component, Camera_t);
        parent->start = Start;

        MAT4_InitPerspective(
                &instance->projection,
                DEGREES_TO_RADIANS(fov),
                aspect,
                near,
                far);
    }

    return parent;
}

void CAMERA_GetViewProjection(
        Camera_t* instance,
        Mat4_t* result)
{
    if (instance != NULL && result != NULL) {
        Vec3_t cameraPosition;
        TRANSFORM_GetTransformedPosition(instance->transform, &cameraPosition);
        VEC3_Scale(&cameraPosition, &cameraPosition, -1.0f);

        Mat4_t cameraTranslationMatrix;
        MAT4_InitTranslateVec3(&cameraTranslationMatrix, &cameraPosition);

        Quat_t cameraRotation;
        TRANSFORM_GetTransformedRotation(instance->transform, &cameraRotation);
        QUAT_Conjugate(&cameraRotation, &cameraRotation);

        Mat4_t cameraRotationMatrix;
        MAT4_RotateMatrixFromQuat(&cameraRotationMatrix, &cameraRotation);

        MAT4_Multiply(result, &cameraRotationMatrix, &cameraTranslationMatrix);
        MAT4_Multiply(result, &instance->projection, result);

        /*Vec3_t forward;
        VEC3_RotateQuat(&forward, &rotation, &VEC3_FORWARD);

        Vec3_t up;
        VEC3_RotateQuat(&up, &rotation, &VEC3_UP);

        VEC3_Cross(&up, &up, &forward);
        VEC3_Cross(&up, &up, &forward);

        Vec3_t center;
        VEC3_Add(&center, &position, &forward);

        Mat4_t lookAt;
        MAT4_LookAt(&lookAt, &position, &center, &up);

        MAT4_Multiply(result, &instance->projection, &lookAt);*/
    }
}

extern void CAMERA_GetModelViewProjection(
        Camera_t* instance,
        Transform_t* transform,
        Mat4_t* result)
{
    if (instance != NULL
            && transform != NULL
            && result != NULL) {

        Mat4_t viewProjection;
        CAMERA_GetViewProjection(instance, &viewProjection);

        Mat4_t transformation;
        TRANSFORM_GetTransformation(transform, &transformation);

        /* viewProjection * transformation */
        MAT4_Multiply(result, &viewProjection, &transformation);
    }
}

/*-----------------------------------------------
 * PRIVATE METHOD DEFINITIONS
 *-----------------------------------------------
 */

static void Start(Component_t* parent)
{
    if (parent != NULL) {
        Camera_t* instance = (Camera_t*)parent;

        instance->transform = GAMEOBJECT_GetTransform(parent->parent);
    }
}
