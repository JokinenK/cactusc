/*
 * camera.h
 *
 *  Created on: 17.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef CAMERA_H
#define CAMERA_H

#include "component.h"
#include "math/vec3.h"
#include "math/mat4.h"

typedef struct Transform_s Transform_t;

/*-----------------------------------------------
 * MACRO DEFINITIONS
 *-----------------------------------------------
 */

/* Rotation vectors */
#define CAMERA_ROTATE_UP        (VEC3_X_NEG)
#define CAMERA_ROTATE_DOWN      (VEC3_X_POS)
#define CAMERA_ROTATE_LEFT      (VEC3_Y_POS)
#define CAMERA_ROTATE_RIGHT     (VEC3_Y_NEG)
#define CAMERA_ROLL_LEFT        (VEC3_Z_NEG)
#define CAMERA_ROLL_RIGHT       (VEC3_Z_POS)

/*-----------------------------------------------
 * PUBLIC TYPE DEFINITIONS
 *-----------------------------------------------
 */

typedef struct Camera_s {
    union {
        Component_t component;
    } parent;

    Mat4_t projection;
    Transform_t* transform;
} Camera_t;

/*-----------------------------------------------
 * PUBLIC METHOD DECLARATIONS
 *-----------------------------------------------
 */

extern Component_t* CAMERA_Init(
        Camera_t* camera,
        float fov,
        float aspect,
        float near,
        float far);

extern void CAMERA_GetViewProjection(
        Camera_t* camera,
        Mat4_t* viewProjection);

extern void CAMERA_GetModelViewProjection(
        Camera_t* camera,
        Transform_t* transform,
        Mat4_t* modelViewProjection);

#endif /* CAMERA_H */
