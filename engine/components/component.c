/*
 * component.c
 *
 *  Created on: 8.3.2016
 *      Author: Kalle Jokinen
 */

#include "component.h"

/*-----------------------------------------------
 * PRIVATE METHOD DECLARATIONS
 *-----------------------------------------------
 */

static void Start(Component_t* instance);

static void Stop(Component_t* instance);

static void FixedUpdate(Component_t* instance);

static void Update(Component_t* instance);

static void LateUpdate(Component_t* instance);

static void Pause(Component_t* instance);

static void Unpause(Component_t* instance);

/*-----------------------------------------------
 * PUBLIC METHOD IMPLEMENTATIONS
 *-----------------------------------------------
 */

Component_t* COMPONENT_InitWithTypeId(
        Component_t* instance,
        uint32_t componentTypeId)
{
    if (instance != NULL) {
        memset(instance, 0, sizeof(Component_t));
        instance->componentTypeId = componentTypeId;
    }

    return instance;
}

void COMPONENT_ReceiveState(
        Component_t* instance,
        ComponentState_t state)
{
    switch (state)
    {
    case COMPONENTSTATE_START:
        Start(instance);
        break;
    case COMPONENTSTATE_STOP:
        Stop(instance);
        break;
    case COMPONENTSTATE_FIXEDUPDATE:
        FixedUpdate(instance);
        break;
    case COMPONENTSTATE_UPDATE:
        Update(instance);
        break;
    case COMPONENTSTATE_LATEUPDATE:
        LateUpdate(instance);
        break;
    case COMPONENTSTATE_PAUSE:
        Pause(instance);
        break;
    case COMPONENTSTATE_UNPAUSE:
        Unpause(instance);
        break;
    }
}

void COMPONENT_Render(
        Component_t* instance,
        Renderer_t* renderer,
        Shader_t* shader,
        Light_t* light)
{
    if (!instance->disabled && instance->render != 0) {
        instance->render(instance, renderer, shader, light);
    }
}

/*-----------------------------------------------
 * PRIVATE METHOD IMPLEMENTATIONS
 *-----------------------------------------------
 */

void Start(Component_t* instance)
{
    if (!instance->disabled && instance->start != 0) {
        instance->start(instance);
    }
}

void Stop(Component_t* instance)
{
    if (!instance->disabled && instance->stop != 0) {
        instance->stop(instance);
    }
}

void FixedUpdate(Component_t* instance)
{
    if (!instance->disabled && instance->fixedUpdate != 0) {
        instance->fixedUpdate(instance);
    }
}

void Update(Component_t* instance)
{
    if (!instance->disabled && instance->update != 0) {
        instance->update(instance);
    }
}

void LateUpdate(Component_t* instance)
{
    if (!instance->disabled && instance->lateUpdate != 0) {
        instance->lateUpdate(instance);
    }
}

void Pause(Component_t* instance)
{
    if (!instance->disabled && instance->pause != 0) {
        instance->pause(instance);
    }
}

void Unpause(Component_t* instance)
{
    if (!instance->disabled && instance->unpause != 0) {
        instance->unpause(instance);
    }
}
