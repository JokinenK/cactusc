/*
 * component.h
 *
 *  Created on: 8.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef COMPONENT_H
#define COMPONENT_H

#include "common/types.h"
#include "common/hash.h"
#include "componentstate.h"

/*-----------------------------------------------
 * MACRO DEFINITIONS
 *-----------------------------------------------
 */

#define COMPONENT_Type(component)       \
    COMPONENT_TypeString(STRINGIFY(component))

#define COMPONENT_TypeString(component) \
    HASH_String(component)

#define COMPONENT_Init(instance, componentType) \
    COMPONENT_InitWithTypeId(instance, COMPONENT_Type(componentType))

/*-----------------------------------------------
 * FORWARD DECLARATIONS
 *-----------------------------------------------
 */

typedef struct GameObject_s GameObject_t;
typedef struct Renderer_s Renderer_t;
typedef struct Shader_s Shader_t;
typedef struct Light_s Light_t;

/*-----------------------------------------------
 * PUBLIC TYPE DEFINITIONS
 *-----------------------------------------------
 */

typedef struct Component_s {
    /**
     * Parent game object
     */
    GameObject_t* parent;

    /**
     * Is the component disabled
     */
    boolean disabled;

    /**
     * Unique id of the component type
     */
    uint32_t componentTypeId;

    /**
     * Linked list next element
     */
    struct Component_s* next;

    /**
     * Handles the event COMPONENTSTATE_START
     *
     * @param[in] component Component instance
     */
    void (*start)(struct Component_s* component);

    /**
     * Handles the event COMPONENTSTATE_STOP
     *
     * @param[in] component Component instance
     */
    void (*stop)(struct Component_s* component);

    /**
     * Handles the event COMPONENTSTATE_FIXEDUPDATE
     *
     * @param[in] component Component instance
     */
    void (*fixedUpdate)(struct Component_s* component);

    /**
     * Handles the event COMPONENTSTATE_UPDATE
     *
     * @param[in] component Component instance
     */
    void (*update)(struct Component_s* component);

    /**
     * Handles the event COMPONENTSTATE_LATEUPDATE
     *
     * @param[in] component Component instance
     */
    void (*lateUpdate)(struct Component_s* component);

    /**
     * Handles the event COMPONENTSTATE_PAUSE
     *
     * @param[in] component Component instance
     */
    void (*pause)(struct Component_s* component);

    /**
     * Handles the event COMPONENTSTATE_UNPAUSE
     *
     * @param[in] component Component instance
     */
    void (*unpause)(struct Component_s* component);

    /**
     * Handles the rendering call for the component
     *
     * @param[in] component Component instance
     * @param[in] renderer  Renderer instance
     * @param[in] shader    Shader to be used
     */
    void (*render)(
            struct Component_s* component,
            Renderer_t* renderer,
            Shader_t* shader,
            Light_t* light);

} Component_t;

/*-----------------------------------------------
 * PUBLIC METHOD DECLARATIONS
 *-----------------------------------------------
 */

extern Component_t* COMPONENT_InitWithTypeId(
        Component_t* instance,
        uint32_t componentTypeId);

extern void COMPONENT_ReceiveState(
        Component_t* instance,
        ComponentState_t state);

extern void COMPONENT_Render(
        Component_t* instance,
        Renderer_t* renderer,
        Shader_t* shader,
        Light_t* light);

#endif /* COMPONENT_H */
