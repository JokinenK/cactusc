/*
 * componentstate.h
 *
 *  Created on: 12.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef COMPONENTSTATE_H
#define COMPONENTSTATE_H

typedef enum {
    COMPONENTSTATE_START,
    COMPONENTSTATE_STOP,
    COMPONENTSTATE_FIXEDUPDATE,
    COMPONENTSTATE_UPDATE,
    COMPONENTSTATE_LATEUPDATE,
    COMPONENTSTATE_PAUSE,
    COMPONENTSTATE_UNPAUSE,

} ComponentState_t;

#endif /* COMPONENTSTATE_H */
