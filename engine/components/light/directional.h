#ifndef DIRECTIONLIGHT_H
#define DIRECTIONLIGHT_H

#include "light.h"

typedef struct DirectionLight_s {
    /**
     * Inheritance
     */
    union {
        /**
         * Base light instance
         */
        Light_t light;
    } parent;

    /**
     * Initializes new light instance
     *
     * @param[in]   light       Pointer to light
     * @param[in]   transform   Light transform
     * @param[in]   shader      Shader to be used with light
     */
    void (*init)(struct Light_s* light,
            const Transform_t* transform,
            const Shader_t* shader,
            Color_t color,
            float intensity);

    /**
     * Reads the direction of the light
     *
     * @param[in]   light       Light instance
     * @return                  Direction vector of the light
     */
    Vec3_t* (*getDirection)(Light_t* light);
} DirectionLight_t;

extern Light_t* DIRECTIONLIGHT_Init(DirectionLight_t* directionlight);

#endif /* DIRECTIONLIGHT_H */

