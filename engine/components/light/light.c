#include "light.h"
#include "engine/gameobject.h"
#include "engine/transform.h"


static void Start(Component_t* parent);

Light_t* LIGHT_Init(
        Light_t* instance,
        Shader_t* shader,
        Color_t color,
        float intensity)
{
    Component_t* parent = NULL;

    if (instance != NULL) {
        parent = COMPONENT_Init(&instance->parent.component, Light_t);
        parent->start = Start;

        instance->shader = shader;
        instance->color = color;
        instance->intensity = intensity;
        instance->transform = NULL;
    }

    return instance;
}

const Vec3_t* LIGHT_GetPosition(const Light_t* instance, Vec3_t* position)
{
    if (instance != NULL) {
        if (instance->transform != NULL) {
            TRANSFORM_GetTransformedPosition(
                    instance->transform,
                    position);
        }
    }

    return position;
}

const Color_t* LIGHT_GetColor(const Light_t* instance)
{
    if (instance != NULL) {
        return &instance->color;
    }

    return NULL;
}

float LIGHT_GetIntensity(const Light_t* instance)
{
    if (instance != NULL) {
        return instance->intensity;
    }

    return 0.0f;
}

static void Start(Component_t* parent)
{
    if (parent != NULL) {
        Light_t* instance = (Light_t*)parent;
        instance->transform = GAMEOBJECT_GetTransform(parent->parent);
    }
}
