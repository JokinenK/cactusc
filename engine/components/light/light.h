#ifndef LIGHT_H
#define LIGHT_H

#include "engine/components/component.h"
#include "common/types.h"
#include "types/color.h"
#include "math/vec3.h"

typedef struct Transform_s Transform_t;
typedef struct Shader_s Shader_t;

typedef struct Light_s {
    union {
        Component_t component;
    } parent;

    /* Transformation of the light */
    Transform_t* transform;

    /* Shader assigned to light */
    Shader_t* shader;

    /* Color of the light */
    Color_t color;

    /* Lights intensity */
    float intensity;

    /* Next in linked list */
    struct Light_s* next;

} Light_t;

extern Light_t* LIGHT_Init(
        Light_t* instance,
        Shader_t* shader,
        Color_t color,
        float intensity);

extern const Vec3_t* LIGHT_GetPosition(const Light_t* instance, Vec3_t* position);

extern const Color_t* LIGHT_GetColor(const Light_t* instance);

extern float LIGHT_GetIntensity(const Light_t* instance);

#endif /* LIGHT_H */

