#ifndef POINTLIGHT_H
#define POINTLIGHT_H

#include "light.h"
#include "types/attenuation.h"

typedef struct PointLight_s {
    /**
     * Inheritance
     */
    union {
        /**
         * Base light instance
         */
        Light_t light;
    } parent;

    /**
     * Lights range
     */
    float range;

    /**
     * Light attenuation
     */
    Attenuation_t attenuation;

    /**
     * Initializes new spotlight instance
     *
     * @param[in]   light       Pointer to light
     * @param[in]   transform   Light transform
     * @param[in]   shader      Shader to be used with light
     */
    void (*init)(struct Light_s* light,
            const Transform_t* transform,
            const Shader_t* shader,
            Color_t color,
            float intensity,
            Attenuation_t attenuation);

} PointLight_t;

extern Light_t* POINTLIGHT_Init(PointLight_t* pointLight);

#endif /* POINTLIGHT_H */

