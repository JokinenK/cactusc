#ifndef SPOTLIGHT_H
#define SPOTLIGHT_H

#include "pointlight.h"
#include "types/attenuation.h"

typedef struct SpotLight_s {
    /**
     * Inheritance
     */
    union {
        /**
         * Base light instance
         */
        Light_t light;

        /**
         * Point light instance
         */
        PointLight_t pointLight;
    } parent;

    /**
     * Light's cutoff distance
     */
    float cutoff;

    /**
     * Initializes new spotlight instance
     *
     * @param[in]   light       Pointer to light
     * @param[in]   transform   Light transform
     * @param[in]   shader      Shader to be used with light
     */
    void (*init)(struct Light_s* light,
            const Transform_t* transform,
            const Shader_t* shader,
            Color_t color,
            float intensity,
            Attenuation_t attenuation,
            float cutoff);

    /**
     * Reads the direction of the light
     *
     * @param[in]   light       Light instance
     * @return                  Direction vector of the light
     */
    Vec3_t* (*getDirection)(Light_t* light);

} SpotLight_t;

extern Light_t* SPOTLIGHT_Init(SpotLight_t* light);

#endif /* SPOTLIGHT_H */

