#include "material.h"
#include "renderer/texture.h"

Component_t* MATERIAL_Init(
        Material_t* instance,
        const Texture_t* texture,
        Color_t color,
        Specular_t specular)
{
    Component_t* parent = NULL;

    if (instance != NULL) {
        parent = COMPONENT_Init(&instance->parent.component, Material_t);

        instance->texture = texture;
        instance->color = color;
        instance->specular = specular;
    }

    return parent;
}

void MATERIAL_Bind(const Material_t* instance, uint32_t textureUnit)
{
    TEXTURE_Bind(instance->texture, textureUnit);
}
