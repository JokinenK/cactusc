#ifndef MATERIAL_H
#define MATERIAL_H

#include "component.h"
#include "types/color.h"
#include "types/specular.h"

typedef struct Texture_s Texture_t;

typedef struct Material_s {
    union {
        Component_t component;
    } parent;

    const Texture_t* texture;
    Color_t color;
    Specular_t specular;
} Material_t;

extern Component_t* MATERIAL_Init(
        Material_t* material,
        const Texture_t* texture,
        Color_t color,
        Specular_t specular);

extern void MATERIAL_Bind(const Material_t* material, uint32_t textureUnit);

#endif /* MATERIAL_H */
