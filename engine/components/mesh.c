/*
 * mesh.c
 *
 *  Created on: 17.3.2016
 *      Author: Kalle Jokinen
 */

#include "mesh.h"

/*-----------------------------------------------
 * PUBLIC METHOD DEFINITIONS
 *-----------------------------------------------
 */

Component_t* MESH_Init(
        Mesh_t* instance,
        const char* meshPath,
        MeshType_t meshType)
{
    Component_t* parent = NULL;

    if (instance != NULL) {
        parent = COMPONENT_Init(&instance->parent.component, Mesh_t);

        instance->meshPath = meshPath;
        instance->meshType = meshType;
    }

    return parent;
}
