/*
 * mesh.h
 *
 *  Created on: 17.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef MESH_H
#define MESH_H

#include "component.h"

/*-----------------------------------------------
 * PUBLIC TYPE DEFINITIONS
 *-----------------------------------------------
 */

typedef enum {
    MESH_TYPE_OBJ,
    MESH_TYPE_COLLADA
} MeshType_t;

typedef struct Mesh_s {
    union {
        Component_t component;
    } parent;

    MeshType_t meshType;
    const char* meshPath;
} Mesh_t;

/*-----------------------------------------------
 * PUBLIC METHOD DECLARATIONS
 *-----------------------------------------------
 */

extern Component_t* MESH_Init(
        Mesh_t* mesh,
        const char* meshPath,
        MeshType_t meshType);

#endif /* MESH_H */
