/*
 * meshrenderer.c
 *
 *  Created on: 17.3.2016
 *      Author: Kalle Jokinen
 */

#include <stdio.h>
#include "meshrenderer.h"
#include "mesh.h"
#include "material.h"
#include "renderer/renderer.h"
#include "renderer/obj/objparser.h"
#include "engine/gameobject.h"
#include "engine/transform.h"
#include "shaders/shader.h"

/*-----------------------------------------------
 * PRIVATE METHOD DECLARATIONS
 *-----------------------------------------------
 */

static void Start(Component_t* component);

static void Stop(Component_t* component);

static void Render(
        Component_t* component,
        Renderer_t* renderer,
        Shader_t* shader,
        Light_t* light);

/*-----------------------------------------------
 * PUBLIC METHOD DEFINITIONS
 *-----------------------------------------------
 */

Component_t* MESHRENDERER_Init(
        MeshRenderer_t* instance,
        Renderer_t* renderer,
        Camera_t* camera)
{
    Component_t* parent = NULL;

    if (instance != NULL) {
        parent = COMPONENT_Init(&instance->parent.component, MeshRenderer_t);
        parent->start = Start;
        parent->stop = Stop;
        parent->render = Render;

        instance->renderer = renderer;
        instance->camera = camera;
        instance->allocated = FALSE;
    }

    return parent;
}

/*-----------------------------------------------
 * PRIVATE METHOD DEFINITIONS
 *-----------------------------------------------
 */

static void Start(Component_t* parent)
{
    if (parent != NULL) {
        MeshRenderer_t* instance = (MeshRenderer_t*)parent;

        if (!instance->allocated) {
            Mesh_t* mesh = GAMEOBJECT_GetComponent(
                    parent->parent,
                    Mesh_t);

            Material_t* material = GAMEOBJECT_GetComponent(
                    parent->parent,
                    Material_t);

            if (mesh != NULL
                    && material != NULL) {

                Model_t* model = NULL;

                switch (mesh->meshType) {
                case MESH_TYPE_OBJ:
                    model = OBJPARSER_Parse(mesh->meshPath);
                    break;

                case MESH_TYPE_COLLADA:
                    model = NULL;
                    break;
                }

                /* Add model to engine and read handle */
                if (model != NULL) {
                    instance->modelHandle = RENDERER_AddModel(
                            instance->renderer,
                            model);

                    MODEL_Free(&model);

                    instance->allocated = TRUE;
                    instance->material = material;
                    instance->transform = GAMEOBJECT_GetTransform(
                            parent->parent);
                }
                else {
                    printf("Unable to load mesh: %s\n", mesh->meshPath);
                }
            }
        }
    }
}

static void Stop(Component_t* parent)
{
    if (parent != NULL) {
        MeshRenderer_t* instance = (MeshRenderer_t*)parent;

        if (instance->allocated) {
            RENDERER_FreeModel(instance->renderer, instance->modelHandle);
        }
    }
}

static void Render(
        Component_t* parent,
        Renderer_t* renderer,
        Shader_t* shader,
        Light_t* light)
{
    if (parent != NULL) {
        MeshRenderer_t* instance = (MeshRenderer_t*)parent;

        if (instance->allocated) {
            MATERIAL_Bind(instance->material, 0);

            SHADER_Bind(shader);

            SHADER_Update(
                    shader,
                    instance->camera,
                    light,
                    instance->transform,
                    instance->material);

            RENDERER_RequestRender(renderer, instance->modelHandle);
        }
    }
}
