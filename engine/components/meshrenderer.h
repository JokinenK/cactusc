/*
 * meshrenderer.h
 *
 *  Created on: 17.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef MESHRENDERER_H
#define MESHRENDERER_H

#include "component.h"
#include "renderer/renderer.h"

typedef struct Renderer_s Renderer_t;
typedef struct GameObject_s GameObject_t;
typedef struct Camera_s Camera_t;
typedef struct Material_s Material_t;
typedef struct Transform_s Transform_t;

/*-----------------------------------------------
 * PUBLIC TYPE DEFINITIONS
 *-----------------------------------------------
 */

typedef struct MeshRenderer_s {
    union {
        Component_t component;
    } parent;

    boolean allocated;
    Renderer_t* renderer;
    Camera_t* camera;
    Material_t* material;
    Transform_t* transform;
    ModelHandle_t modelHandle;
} MeshRenderer_t;

/*-----------------------------------------------
 * PUBLIC METHOD DECLARATIONS
 *-----------------------------------------------
 */

extern Component_t* MESHRENDERER_Init(
        MeshRenderer_t* mesh,
        Renderer_t* renderer,
        Camera_t* camera);

#endif /* MESHRENDERER_H */
