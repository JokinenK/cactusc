/*
 * customstring.c
 *
 *  Created on: 8.3.2016
 *      Author: Kalle Jokinen
 */

#include <string.h>
#include <stdlib.h>

#include "customstring.h"

CustomString_t* CUSTOMSTRING_InitWithLength(
        CustomString_t* string,
        const char* value,
        uint32_t length)
{
    string->length = length * sizeof(char);
    string->value = (char*)(malloc(string->length));
    memcpy(string->value, value, string->length);

    return string;
}

void CUSTOMSTRING_Free(CustomString_t* tag) {
    if (tag != NULL) {
        free((tag)->value);
    }
}

boolean CUSTOMSTRING_Compare(const CustomString_t* a, const CustomString_t* b)
{
    if (a->length == b->length) {
        return (memcmp(a->value, b->value, a->length) == 0);
    }

    return FALSE;
}
