/*
 * customstring.h
 *
 *  Created on: 8.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef CUSTOMSTRING_H
#define CUSTOMSTRING_H

#include "common/types.h"


/*-----------------------------------------------
 * MACRO DEFINITIONS
 *-----------------------------------------------
 */

#define CUSTOMSTRING_Init(string, str) \
    CUSTOMSTRING_InitWithLength(string, str, strlen(str))

/*-----------------------------------------------
 * PUBLIC TYPE DEFINITIONS
 *-----------------------------------------------
 */

typedef struct CustomString_s {
    char* value;
    uint32_t length;
} CustomString_t;

/*-----------------------------------------------
 * PUBLIC METHOD DECLARATIONS
 *-----------------------------------------------
 */

extern CustomString_t* CUSTOMSTRING_InitWithLength(
        CustomString_t* string,
        const char* value,
        uint32_t length);

extern void CUSTOMSTRING_Free(CustomString_t* string);

extern boolean CUSTOMSTRING_Compare(
        const CustomString_t* a,
        const CustomString_t* b);

#endif /* CUSTOMSTRING_H */
