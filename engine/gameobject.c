/*
 * gameobject.h
 *
 *  Created on: 8.3.2016
 *      Author: Kalle Jokinen
 */

#include <string.h>
#include <stdio.h>
#include "gameobject.h"
#include "common/linkedlist.h"
#include "components/component.h"

/*-----------------------------------------------
 * PRIVATE METHOD DECLARATIONS
 *-----------------------------------------------
 */

static boolean CompareComponents(
        const Component_t* a,
        const Component_t* b);

static boolean CompareGameObjects(
        const GameObject_t* a,
        const GameObject_t* b);

static void FindGameObjectsWithTag(
        GameObject_t* gameObject,
        GameObject_t** matches,
        const CustomString_t* tag);

/*-----------------------------------------------
 * PUBLIC METHOD IMPLEMENTATIONS
 *-----------------------------------------------
 */

void GAMEOBJECT_Init(
        GameObject_t* gameObject,
        const char* tag)
{
    CUSTOMSTRING_Init(&gameObject->tag, tag);

    gameObject->search = 0;
    gameObject->children = 0;
    gameObject->components = 0;

    TRANSFORM_InitEmpty(&gameObject->transform);
}

void GAMEOBJECT_Free(GameObject_t* gameObject)
{
    CUSTOMSTRING_Free(&gameObject->tag);
}

GameObject_t* GAMEOBJECT_GetRoot(GameObject_t* gameObject)
{
    GameObject_t* rootObject = gameObject;

    if (gameObject->parent != NULL) {
        rootObject = GAMEOBJECT_GetRoot(gameObject->parent);
    }

    return rootObject;
}

Transform_t* GAMEOBJECT_GetTransform(GameObject_t* instance)
{
    return &instance->transform;
}

GameObject_t* GAMEOBJECT_FindGameObjectsWithTag(
        GameObject_t* instance,
        const char* tag)
{
    GameObject_t* matches = NULL;
    CustomString_t stringTag;
    CUSTOMSTRING_Init(&stringTag, tag);

    FindGameObjectsWithTag(instance, &matches, &stringTag);

    CUSTOMSTRING_Free(&stringTag);

    return matches;
}

void GAMEOBJECT_SendState(
        GameObject_t* instance,
        ComponentState_t state)
{
    GameObject_t* child;
    LINKEDLIST_ForEachWithField(instance->children, child, next) {
        GAMEOBJECT_SendState(child, state);
    }

    Component_t* component;
    LINKEDLIST_ForEachWithField(instance->components, component, next) {
        COMPONENT_ReceiveState(component, state);
    }
}

void GAMEOBJECT_Render(
        GameObject_t* instance,
        Renderer_t* renderer,
        Shader_t* shader,
        Light_t* light)
{
    GameObject_t* child;
    LINKEDLIST_ForEachWithField(instance->children, child, next) {
        GAMEOBJECT_Render(child, renderer, shader, light);
    }

    Component_t* component;
    LINKEDLIST_ForEachWithField(instance->components, component, next) {
        COMPONENT_Render(component, renderer, shader, light);
    }
}

boolean GAMEOBJECT_AddComponent(
        GameObject_t* instance,
        Component_t* component)
{
    boolean success = FALSE;

    if (instance != NULL && component != NULL) {
        Component_t* result;

        LINKEDLIST_Search(
                instance->components,
                component,
                result,
                CompareComponents);

        if (result == NULL) {
            component->parent = instance;

            LINKEDLIST_PrependWithField(
                    instance->components,
                    component,
                    next);

            success = TRUE;
        }
    }

    return success;
}

boolean GAMEOBJECT_AddChild(
        GameObject_t* instance,
        GameObject_t* child)
{
    boolean success = FALSE;

    if (instance != NULL && child != NULL) {
        GameObject_t* result;

        LINKEDLIST_Search(
                instance->children,
                child,
                result,
                CompareGameObjects);

        if (result == NULL) {
            child->parent = instance;

            LINKEDLIST_PrependWithField(
                    instance->children,
                    child,
                    next);

            success = TRUE;
        }
    }

    return success;
}

Component_t* GAMEOBJECT_GetComponentWithTypeId(
        GameObject_t* instance,
        uint32_t componentTypeId)
{
    Component_t* result = NULL;
    LINKEDLIST_ForEach(instance->components, result) {
        if (result->componentTypeId == componentTypeId) {
            break;
        }
    }

    return result;
}

/*-----------------------------------------------
 * PRIVATE METHOD IMPLEMENTATIONS
 *-----------------------------------------------
 */

static boolean CompareComponents(
        const Component_t* a,
        const Component_t* b)
{
    //return memcmp(a, b, sizeof(Component_t)) == 0;
    return a == b;
}

static boolean CompareGameObjects(
        const GameObject_t* a,
        const GameObject_t* b)
{
    //return memcmp(a, b, sizeof(GameObject_t)) == 0;
    return a == b;
}

static void FindGameObjectsWithTag(
        GameObject_t* instance,
        GameObject_t** matches,
        const CustomString_t* tag)
{
    if (CUSTOMSTRING_Compare(&instance->tag, tag)) {
        LINKEDLIST_PrependWithField(*matches, instance, search);
    }

    GameObject_t* child;
    LINKEDLIST_ForEachWithField(instance->children, child, next) {
        FindGameObjectsWithTag(child, matches, tag);
    }
}
