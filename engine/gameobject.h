/*
 * gameobject.h
 *
 *  Created on: 8.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include "transform.h"
#include "customstring.h"
#include "components/componentstate.h"

/*-----------------------------------------------
 * MACRO DEFINITIONS
 *-----------------------------------------------
 */

#define GAMEOBJECT_GetComponent(gameObject, component)  \
    (component*)GAMEOBJECT_GetComponentWithTypeId(      \
        gameObject,                                     \
        HASH_Const(component))

/*-----------------------------------------------
 * FORWARD DECLARATIONS
 *-----------------------------------------------
 */

typedef struct CustomString_s CustomString_t;
typedef struct Shader_s Shader_t;
typedef struct Light_s Light_t;
typedef struct Renderer_s Renderer_t;
typedef struct Component_s Component_t;

/*-----------------------------------------------
 * PUBLIC TYPE DEFINITIONS
 *-----------------------------------------------
 */

typedef struct GameObject_s {
    CustomString_t tag;
    Transform_t transform;
    Component_t* components;
    struct GameObject_s* children;
    struct GameObject_s* parent;
    struct GameObject_s* search;
    struct GameObject_s* next;
} GameObject_t;

/*-----------------------------------------------
 * PUBLIC METHOD DECLARATIONS
 *-----------------------------------------------
 */

extern void GAMEOBJECT_Init(GameObject_t* gameObject, const char* tag);

extern void GAMEOBJECT_Free(GameObject_t* gameObject);

extern GameObject_t* GAMEOBJECT_GetRoot(GameObject_t* gameObject);

extern Transform_t* GAMEOBJECT_GetTransform(GameObject_t* gameObject);

extern GameObject_t* GAMEOBJECT_FindGameObjectsWithTag(
        GameObject_t* gameObject,
        const char* tag);

extern void GAMEOBJECT_SendState(
        GameObject_t* gameObject,
        ComponentState_t state);

extern void GAMEOBJECT_Render(
        GameObject_t* gameObject,
        Renderer_t* renderer,
        Shader_t* shader,
        Light_t* light);

extern boolean GAMEOBJECT_AddComponent(
        GameObject_t* gameObject,
        Component_t* component);

extern boolean GAMEOBJECT_AddChild(
        GameObject_t* gameObject,
        GameObject_t* child);

extern Component_t* GAMEOBJECT_GetComponentWithTypeId(
        GameObject_t* gameObject,
        uint32_t componentTypeId);

#endif /* GAMEOBJECT_H */
