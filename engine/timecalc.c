/*
 * timecalc.c
 *
 *  Created on: 12.3.2016
 *      Author: Kalle Jokinen
 */

#include <time.h>
#include <stdio.h>
#include "timecalc.h"

/*-----------------------------------------------
 * MACRO DEFINITIONS
 *-----------------------------------------------
 */

#define NUM_SAMPLES 30

/*-----------------------------------------------
 * PRIVATE VARIABLE DEFINITIONS
 *-----------------------------------------------
 */

uint32_t f_samples[NUM_SAMPLES] = {0};
uint32_t f_sampleIndex;
uint32_t f_sampleSum;

float f_t;
float f_lag;
float f_deltaTime;
float f_currentTime;
float f_prevTime;
float f_frameTime;
float f_clocksToSec;

/*-----------------------------------------------
 * PRIVATE METHOD DECLARATIONS
 *-----------------------------------------------
 */

static float CurrentTime();

/*-----------------------------------------------
 * PUBLIC METHOD DEFINITIONS
 *-----------------------------------------------
 */

void TIMECALC_Init(uint16_t targetFrameRate)
{
    f_t = 0.0f;
    f_lag = 0.0f;
    f_frameTime = 1.0f / targetFrameRate;
    f_clocksToSec = 1.0f / CLOCKS_PER_SEC;

    f_sampleIndex = 0;
    f_sampleSum = 0;

    f_currentTime = 0.0f;
    f_prevTime = CurrentTime();
}

void TIMECALC_StartOfFrame()
{
    f_currentTime = CurrentTime();
    f_deltaTime = f_currentTime - f_prevTime;
    f_prevTime = f_currentTime;
    f_lag += f_deltaTime;
}

void TIMECALC_EndOfFrame()
{
    uint32_t sample = 1.0f / f_deltaTime;

    f_sampleSum -= f_samples[f_sampleIndex];
    f_sampleSum += sample;
    f_samples[f_sampleIndex] = sample;

    if (++f_sampleIndex == NUM_SAMPLES) {
        f_sampleIndex = 0;
    }
}

float TIMECALC_GetT()
{
    return f_t;
}

float TIMECALC_DeltaTime()
{
    return f_deltaTime;
}

int TIMECALC_Fps()
{
    return f_sampleSum / NUM_SAMPLES;
}

int TIMECALC_RawFps()
{
    return f_samples[f_sampleIndex - 1];
}

boolean TIMECALC_UpdateFixed()
{
    if (f_lag >= f_frameTime) {
        f_lag -= f_frameTime;
        f_t += f_frameTime;

        return TRUE;
    }

    return FALSE;
}

/*-----------------------------------------------
 * PRIVATE METHOD DEFINITIONS
 *-----------------------------------------------
 */

static float CurrentTime() {
    return ((float)clock() * f_clocksToSec);
}
