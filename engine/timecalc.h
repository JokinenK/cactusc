/*
 * timecalc.h
 *
 *  Created on: 12.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef TIMECALC_H
#define TIMECALC_H

#include "common/types.h"

extern void TIMECALC_Init(uint16_t frameRate);

extern void TIMECALC_StartOfFrame();
extern void TIMECALC_EndOfFrame();


extern float TIMECALC_GetT();
extern float TIMECALC_DeltaTime();
extern int TIMECALC_Fps();
extern int TIMECALC_RawFps();
extern boolean TIMECALC_UpdateFixed();

#endif /* TIMECALC_H */
