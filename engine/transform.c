/*
 * transform.c
 *
 *  Created on: 8.3.2016
 *      Author: Kalle Jokinen
 */

#include <math.h>
#include "transform.h"
#include "gameobject.h"
#include "math/vec4.h"
#include "math/mat4.h"
#include "engine/components/camera.h"

/*-----------------------------------------------
 * PRIVATE METHOD DECLARATIONS
 *-----------------------------------------------
 */

static void UpdatePrevious(Transform_t* instance, boolean changed);

/*-----------------------------------------------
 * PUBLIC METHOD DEFINITIONS
 *-----------------------------------------------
 */

void TRANSFORM_InitEmpty(Transform_t* instance)
{
    VEC3_Init(&instance->position, 0.0f, 0.0f, 0.0f);
    VEC3_Init(&instance->scale,    1.0f, 1.0f, 1.0f);
    QUAT_Identity(&instance->rotation);

    VEC3_Init(&instance->previous.position, 0.0f, 0.0f, 0.0f);
    VEC3_Init(&instance->previous.scale,    1.0f, 1.0f, 1.0f);
    QUAT_Identity(&instance->previous.rotation);

    instance->parent = NULL;
    instance->parentTransform = NULL;
}

void TRANSFORM_Init(
        Transform_t* instance,
        const Vec3_t* position,
        const Quat_t* rotation,
        const Vec3_t* scale)
{
    TRANSFORM_InitEmpty(instance);

    TRANSFORM_SetPosition(instance, position);
    TRANSFORM_SetRotation(instance, rotation);
    TRANSFORM_SetScale(instance, scale);
}

void TRANSFORM_SetPosition(
        Transform_t* instance,
        const Vec3_t* position)
{
    VEC3_Assign(&instance->position, position);
}

void TRANSFORM_SetRotation(
        Transform_t* instance,
        const Quat_t* rotation)
{
    QUAT_Assign(&instance->rotation, rotation);
}

void TRANSFORM_SetScale(
        Transform_t* instance,
        const Vec3_t* scale)
{
    VEC3_Assign(&instance->scale, scale);
}

void TRANSFORM_Translate(
        Transform_t* instance,
        const Vec3_t* direction,
        float amount)
{
    Vec3_t delta;
    VEC3_Scale(&delta, direction, amount);
    VEC3_Add(&instance->position, &instance->position, &delta);
}

void TRANSFORM_Rotate(Transform_t* instance, const Vec3_t* axis, float angle)
{
    Quat_t rotateQuat;
    QUAT_InitRotateVec3(&rotateQuat, axis, angle);
    QUAT_Multiply(&instance->rotation, &instance->rotation, &rotateQuat);
}

void TRANSFORM_RotateAlternative(Transform_t* instance, const Vec3_t* axis, float angle)
{
    Quat_t rotateQuat;
    QUAT_InitRotateVec3(&rotateQuat, axis, angle);
    QUAT_Multiply(&instance->rotation, &rotateQuat, &instance->rotation);
}

void TRANSFORM_SetParent(
        Transform_t* instance,
        GameObject_t* parent)
{
    instance->parent = parent;
    instance->parentTransform = &parent->transform;
}

void TRANSFORM_LookAt(Transform_t* instance, const Vec3_t* point, const Vec3_t* up)
{
    Quat_t rotation;
    TRANSFORM_GetLookAtRotation(instance, &rotation, point, up);
    TRANSFORM_SetRotation(instance, &rotation);
}

void TRANSFORM_GetLookAtRotation(Transform_t* instance, Quat_t* result, const Vec3_t* point, const Vec3_t* up)
{
    Vec3_t positionVector;
    VEC3_Substract(&positionVector, point, &instance->position);
    VEC3_Normalize(&positionVector, &positionVector);

    Mat4_t rotationMatrix;
    MAT4_InitRotateForwardUp(&rotationMatrix, &positionVector, up);
    QUAT_InitRotateMat4(result, &rotationMatrix);
}

void TRANSFORM_GetTransformation(
        Transform_t* instance,
        Mat4_t* result)
{
    Mat4_t parentMatrix;
    TRANSFORM_GetParentMatrix(instance, &parentMatrix);

    Mat4_t scaleMatrix;
    MAT4_InitScaleVec3(&scaleMatrix, &instance->scale);

    Mat4_t rotationMatrix;
    MAT4_RotateMatrixFromQuat(&rotationMatrix, &instance->rotation);

    Mat4_t translateMatrix;
    MAT4_InitTranslateVec3(&translateMatrix, &instance->position);

    //scaleMatrix * translateMatrix * rotateMatrix * getParentMatrix();
    MAT4_Multiply(result, &rotationMatrix,  &parentMatrix);
    MAT4_Multiply(result, &translateMatrix, result);
    MAT4_Multiply(result, &scaleMatrix,     result);
}

void TRANSFORM_GetTransformedPosition(
        Transform_t* instance,
        Vec3_t* result)
{
    Mat4_t parentMatrix;
    TRANSFORM_GetParentMatrix(instance, &parentMatrix);
    VEC3_Transform(result, &instance->position, &parentMatrix);
}

void TRANSFORM_GetTransformedRotation(
        Transform_t* instance,
        Quat_t* result)
{
    Quat_t parentRotation;
    QUAT_Identity(&parentRotation);

    if (instance->parentTransform != NULL) {
        TRANSFORM_GetTransformedRotation(
                instance->parentTransform,
                &parentRotation);
    }

    QUAT_Multiply(result, &instance->rotation, &parentRotation);
}

void TRANSFORM_GetParentMatrix(
        Transform_t* instance,
        Mat4_t* result)
{
    if (instance->parentTransform != NULL
            && TRANSFORM_HasChanged(instance->parentTransform)) {

        TRANSFORM_GetTransformation(
                instance->parentTransform,
                result);
    }
    else {
        MAT4_Identity(result);
    }
}

boolean TRANSFORM_HasChanged(Transform_t* instance)
{
    boolean changed = TRUE;

    if (instance != NULL) {
        if (instance->parentTransform != NULL
                && TRANSFORM_HasChanged(instance->parentTransform)) {
            changed = TRUE;
        }
        else if (!VEC3_Equals(
                &instance->position,
                &instance->previous.position)) {
            changed = TRUE;
        }
        else if (!QUAT_Equals(
                &instance->rotation,
                &instance->previous.rotation)) {
            changed = TRUE;
        }
        else if (!VEC3_Equals(
                &instance->scale,
                &instance->previous.scale)) {
            changed = TRUE;
        }

        UpdatePrevious(instance, changed);
    }

    return changed;
}

Vec3_t* TRANSFORM_GetForward(Transform_t* instance, Vec3_t* result)
{
    return VEC3_RotateQuat(result, &instance->rotation, &VEC3_FORWARD);
}

Vec3_t* TRANSFORM_GetBack(Transform_t* instance, Vec3_t* result)
{
    return VEC3_RotateQuat(result, &instance->rotation, &VEC3_BACK);
}

Vec3_t* TRANSFORM_GetUp(Transform_t* instance, Vec3_t* result)
{
    return VEC3_RotateQuat(result, &instance->rotation, &VEC3_UP);
}

Vec3_t* TRANSFORM_GetDown(Transform_t* instance, Vec3_t* result)
{
    return VEC3_RotateQuat(result, &instance->rotation, &VEC3_DOWN);
}

Vec3_t* TRANSFORM_GetRight(Transform_t* instance, Vec3_t* result)
{
    return VEC3_RotateQuat(result, &instance->rotation, &VEC3_RIGHT);
}

Vec3_t* TRANSFORM_GetLeft(Transform_t* instance, Vec3_t* result)
{
    return VEC3_RotateQuat(result, &instance->rotation, &VEC3_LEFT);
}

Vec3_t* TRANSFORM_GetTurnRight(Transform_t* instance, Vec3_t* result)
{
    return VEC3_RotateQuat(result, &instance->rotation, &CAMERA_ROTATE_RIGHT);
}

Vec3_t* TRANSFORM_GetTurnLeft(Transform_t* instance, Vec3_t* result)
{
    return VEC3_RotateQuat(result, &instance->rotation, &CAMERA_ROTATE_LEFT);
}

/*-----------------------------------------------
 * PRIVATE METHOD DEFINITIONS
 *-----------------------------------------------
 */

static void UpdatePrevious(
        Transform_t* instance,
        boolean changed) {

    if (changed) {
        VEC3_Assign(&instance->previous.position, &instance->position);
        QUAT_Assign(&instance->previous.rotation, &instance->rotation);
        VEC3_Assign(&instance->previous.scale, &instance->scale);
    }
}
