/*
 * transform.h
 *
 *  Created on: 8.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "math/vec3.h"
#include "math/mat4.h"
#include "math/quat.h"

/*-----------------------------------------------
 * FORWARD TYPE DECLARATIONS
 *-----------------------------------------------
 */

typedef struct GameObject_s GameObject_t;

/*-----------------------------------------------
 * PUBLIC TYPE DEFINITIONS
 *-----------------------------------------------
 */

typedef struct Transform_s {
    Vec3_t position;
    Quat_t rotation;
    Vec3_t scale;

    struct {
        Vec3_t position;
        Quat_t rotation;
        Vec3_t scale;
    } previous;

    struct Transform_s* parentTransform;

    GameObject_t* parent;
} Transform_t;

/*-----------------------------------------------
 * PUBLIC METHOD DECLARATIONS
 *-----------------------------------------------
 */

extern void TRANSFORM_InitEmpty(Transform_t* instance);

extern void TRANSFORM_Init(
        Transform_t* transform,
        const Vec3_t* position,
        const Quat_t* rotation,
        const Vec3_t* scale);

extern void TRANSFORM_SetPosition(
        Transform_t* transform,
        const Vec3_t* position);

extern void TRANSFORM_SetRotation(
        Transform_t* transform,
        const Quat_t* rotation);

extern void TRANSFORM_SetScale(
        Transform_t* transform,
        const Vec3_t* scale);

extern void TRANSFORM_Translate(
        Transform_t* transform,
        const Vec3_t* direction,
        float amount);

extern void TRANSFORM_Rotate(
        Transform_t* transform,
        const Vec3_t* axis,
        float angle);

extern void TRANSFORM_RotateAlternative(
        Transform_t* instance,
        const Vec3_t* axis,
        float angle);

extern void TRANSFORM_SetParent(
        Transform_t* transform,
        GameObject_t* parent);

extern void TRANSFORM_LookAt(
        Transform_t* instance,
        const Vec3_t* point,
        const Vec3_t* up);

extern void TRANSFORM_GetLookAtRotation(
        Transform_t* instance,
        Quat_t* result,
        const Vec3_t* point,
        const Vec3_t* up);

extern void TRANSFORM_GetTransformation(
        Transform_t* transform,
        Mat4_t* transformation);

extern void TRANSFORM_GetTransformedPosition(
        Transform_t* transform,
        Vec3_t* transformedPosition);

extern void TRANSFORM_GetTransformedRotation(
        Transform_t* transform,
        Quat_t* transformedRotation);

extern void TRANSFORM_GetParentMatrix(
        Transform_t* transform,
        Mat4_t* parentMatrix);

extern boolean TRANSFORM_HasChanged(Transform_t* transform);

extern Vec3_t* TRANSFORM_GetForward(Transform_t* instance, Vec3_t* result);

extern Vec3_t* TRANSFORM_GetBack(Transform_t* instance, Vec3_t* result);

extern Vec3_t* TRANSFORM_GetUp(Transform_t* instance, Vec3_t* result);

extern Vec3_t* TRANSFORM_GetDown(Transform_t* instance, Vec3_t* result);

extern Vec3_t* TRANSFORM_GetRight(Transform_t* instance, Vec3_t* result);

extern Vec3_t* TRANSFORM_GetLeft(Transform_t* instance, Vec3_t* result);

extern Vec3_t* TRANSFORM_GetTurnRight(Transform_t* instance, Vec3_t* result);

extern Vec3_t* TRANSFORM_GetTurnLeft(Transform_t* instance, Vec3_t* result);

#endif /* TRANSFORM_H */
