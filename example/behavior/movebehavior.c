#include <stdio.h>
#include "movebehavior.h"
#include "engine/timecalc.h"
#include "core/input.h"
#include "engine/transform.h"
#include "engine/gameobject.h"
#include "engine/components/camera.h"
#include "SDL_scancode.h"

static void Start(Component_t* parent);
static void Update(Component_t* parent);

Component_t* MOVEBEHAVIOR_Init(MoveBehavior_t* instance, Input_t* input)
{
    Component_t* parent = NULL;

    if (instance != NULL) {
        parent = COMPONENT_Init(&instance->parent.component, MoveBehavior_t);
        parent->start = Start;
        parent->update = Update;

        instance->input = input;
    }

    return parent;
}

static void Start(Component_t* parent)
{
    if (parent != NULL) {
        MoveBehavior_t* instance = (MoveBehavior_t*)parent;

        instance->transform = GAMEOBJECT_GetTransform(parent->parent);
    }
}

static void Update(Component_t* parent)
{
    if (parent != NULL) {
        MoveBehavior_t* instance = (MoveBehavior_t*)parent;

        Input_t* input = instance->input;
        Transform_t* transform  = instance->transform;

        Quat_t rotation;
        TRANSFORM_GetTransformedRotation(transform, &rotation);

        Vec3_t forwardVector;
        TRANSFORM_GetForward(transform, &forwardVector);

        Vec3_t rightVector;
        TRANSFORM_GetRight(transform, &rightVector);

        float moveAmount = 5.0f * TIMECALC_DeltaTime();
        float rollAmount = DEGREES_TO_RADIANS(10.0f * moveAmount);

        if (INPUT_IsKeyPressed(input, SDL_SCANCODE_W)) {
            TRANSFORM_Translate(
                    transform,
                    &forwardVector,
                    moveAmount);
        }
        else if (INPUT_IsKeyPressed(input, SDL_SCANCODE_S)) {
            TRANSFORM_Translate(
                    transform,
                    &forwardVector,
                    -moveAmount);
        }

        if (INPUT_IsKeyPressed(input, SDL_SCANCODE_A)) {
            TRANSFORM_Translate(
                    transform,
                    &rightVector,
                    -moveAmount);
        }
        else if (INPUT_IsKeyPressed(input, SDL_SCANCODE_D)) {
            TRANSFORM_Translate(
                    transform,
                    &rightVector,
                    moveAmount);
        }

        if (INPUT_IsKeyPressed(input, SDL_SCANCODE_Q)) {
            TRANSFORM_Rotate(
                    transform,
                    &CAMERA_ROLL_LEFT,
                    rollAmount);
        }
        else if (INPUT_IsKeyPressed(instance->input, SDL_SCANCODE_E)) {
            TRANSFORM_Rotate(
                    transform,
                    &CAMERA_ROLL_RIGHT,
                    rollAmount);
        }

        if (INPUT_IsMouseButtonPressed(input, 1)) {
            int32_t mouseDeltaX = 0;
            int32_t mouseDeltaY = 0;

            INPUT_MouseDeltaPosition(input, &mouseDeltaX, &mouseDeltaY);

            Vec3_t rightTurn;
            TRANSFORM_GetTurnRight(transform, &rightTurn);

            float angleX = DEGREES_TO_RADIANS(mouseDeltaX);
            float angleY = DEGREES_TO_RADIANS(mouseDeltaY);

            if (mouseDeltaY != 0) {
                TRANSFORM_Rotate(transform, &CAMERA_ROTATE_UP, -angleY);
            }

            if (mouseDeltaX != 0) {
                TRANSFORM_Rotate(transform, &rightTurn, -angleX);
            }
        }
    }
}
