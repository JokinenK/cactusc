#ifndef MOVEBEHAVIOR_H
#define MOVEBEHAVIOR_H

#include "engine/components/component.h"

typedef struct Input_s Input_t;
typedef struct Transform_s Transform_t;

typedef struct MoveBehavior_s {
    union {
        Component_t component;
    } parent;
    Input_t* input;
    Transform_t* transform;
} MoveBehavior_t;

extern Component_t* MOVEBEHAVIOR_Init(MoveBehavior_t* instance, Input_t* input);

#endif /* MOVEBEHAVIOR_H */
