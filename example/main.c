#include <stdio.h>
#include <unistd.h>

#include "core/factory.h"
#include "engine/timecalc.h"
#include "engine/gameobject.h"
#include "engine/customstring.h"
#include "engine/components/mesh.h"
#include "engine/components/camera.h"
#include "engine/components/material.h"
#include "engine/components/meshrenderer.h"

#include "behavior/movebehavior.h"

#define WINDOW_TITLE "Hello World"
#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600
#define TARGET_FPS 60

int main(int argc, char* argv[])
{
    Display_t* display = FACTORY_Display();
    Input_t* input = FACTORY_Input();

    DISPLAY_Init(display, WINDOW_TITLE, WINDOW_WIDTH, WINDOW_HEIGHT);
    INPUT_Init(input);

    Renderer_t* renderer = FACTORY_Renderer();
    RENDERER_Init(renderer);

    GameObject_t rootObject = {0};

    /* Init camera */
    GameObject_t cameraObject;
    GAMEOBJECT_Init(&cameraObject, "Camera");

    Camera_t cameraCamera;
    CAMERA_Init(
            &cameraCamera,
            75.0f,
            DISPLAY_AspectRatio(display),
            0.1f,
            1000.0f);

    MoveBehavior_t cameraMove;
    MOVEBEHAVIOR_Init(&cameraMove, input);

    GAMEOBJECT_AddComponent(&cameraObject, (Component_t*)&cameraCamera);
    GAMEOBJECT_AddComponent(&cameraObject, (Component_t*)&cameraMove);

    /* Init monkey */
    GameObject_t monkeyObject;
    GAMEOBJECT_Init(&monkeyObject, "Player");

    Mesh_t monkeyMesh;
    MESH_Init(&monkeyMesh, "cube.obj", MESH_TYPE_OBJ);

    Texture_t* monkeyTexture = FACTORY_Texture();
    TEXTURE_Init(monkeyTexture, "fur.jpg");

    Specular_t monkeySpecular = {
            .intensity = 0.5f,
            .power = 4.0f
    };

    Color_t monkeyColor;
    COLOR_SetRgb(&monkeyColor, 0, 0, 255);

    Material_t monkeyMaterial;
    MATERIAL_Init(
            &monkeyMaterial,
            monkeyTexture,
            monkeyColor,
            monkeySpecular);

    MeshRenderer_t monkeyRenderer;
    MESHRENDERER_Init(&monkeyRenderer, renderer, &cameraCamera);

    Vec3_t monkeyPos;
    VEC3_Init(&monkeyPos, 0.0f, 0.0f, 1.0f);

    TRANSFORM_SetPosition(&monkeyObject.transform, &monkeyPos);
    GAMEOBJECT_AddComponent(&monkeyObject, (Component_t*)&monkeyMesh);
    GAMEOBJECT_AddComponent(&monkeyObject, (Component_t*)&monkeyMaterial);
    GAMEOBJECT_AddComponent(&monkeyObject, (Component_t*)&monkeyRenderer);

    GAMEOBJECT_AddChild(&rootObject, &cameraObject);
    GAMEOBJECT_AddChild(&rootObject, &monkeyObject);

    float seconds = 0.0f;
    Color_t clearColor;
    COLOR_SetRgb(&clearColor, 255, 0, 0);

    TIMECALC_Init(TARGET_FPS);
    RENDERER_SetClearColor(renderer, clearColor);

    GAMEOBJECT_SendState(&rootObject, COMPONENTSTATE_START);

    while(!DISPLAY_IsClosed(display)) {

        TIMECALC_StartOfFrame();
        INPUT_Update(input);

        RENDERER_Clear(renderer);
        RENDERER_Render(renderer, &rootObject);

        GAMEOBJECT_SendState(&rootObject, COMPONENTSTATE_UPDATE);

        seconds += TIMECALC_DeltaTime();

        if (seconds >= 1.0f) {
            seconds -= 1.0f;

            printf("FPS: %d\n", TIMECALC_Fps());
        }

        if (TIMECALC_UpdateFixed())
        {
            GAMEOBJECT_SendState(&rootObject, COMPONENTSTATE_FIXEDUPDATE);
        }

        GAMEOBJECT_SendState(&rootObject, COMPONENTSTATE_LATEUPDATE);

        DISPLAY_SwapBuffers(display);
        TIMECALC_EndOfFrame();
    }

    GAMEOBJECT_SendState(&rootObject, COMPONENTSTATE_STOP);

    FACTORY_Release();
}
