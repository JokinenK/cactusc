#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "vec2.h"
#include "vec3.h"
#include "vec4.h"
#include "mat4.h"
#include "quat.h"

/*-----------------------------------------------
 * PRIVATE METHOD DECLARATIONS
 *-----------------------------------------------
 */

static float* GenerateOuterProduct(
        float* dest,
        uint16_t rowSize,
        uint16_t columnSize,
        const float* rowData,
        const float* columnData);

/*-----------------------------------------------
 * PUBLIC METHOD DEFINITIONS
 *-----------------------------------------------
 */

Mat4_t* MAT4_Init(Mat4_t* result, float* values)
{
    memcpy(result, values, sizeof(Mat4_t));
    return result;
}

Mat4_t* MAT4_Assign(Mat4_t* result, const Mat4_t* source)
{
    memcpy(result, source, sizeof(Mat4_t));
    return result;
}

Mat4_t* MAT4_Clear(Mat4_t* result)
{
    memset(result, 0, sizeof(Mat4_t));
    return result;
}

Mat4_t* MAT4_Identity(Mat4_t* result)
{
    MAT4_Clear(result);

    result->x.x = 1.0f;
    result->y.y = 1.0f;
    result->z.z = 1.0f;
    result->w.w = 1.0f;

    return result;
}

Mat4_t* MAT4_Transpose(Mat4_t* result, const Mat4_t* source)
{
    uint16_t row = 0;
    uint16_t col = 0;
    Mat4_t tempMatrix;

    for (row = 0; row < MAT4_ROW_SIZE; row++)
    {
        for (col = 0; col < MAT4_COL_SIZE; col++)
        {
            /* Swap the columns and rows for the output */
            tempMatrix.data[row].data[col] = source->data[col].data[row];
        }
    }

    return MAT4_Assign(result, &tempMatrix);
}

Mat4_t* MAT4_Add(Mat4_t* result, const Mat4_t* a, const Mat4_t* b)
{
    Mat4_t tempMatrix;

    VEC4_Add(&tempMatrix.x, &a->x, &b->x);
    VEC4_Add(&tempMatrix.y, &a->y, &b->y);
    VEC4_Add(&tempMatrix.z, &a->z, &b->z);
    VEC4_Add(&tempMatrix.w, &a->w, &b->w);

    return MAT4_Assign(result, &tempMatrix);
}

Mat4_t* MAT4_Substract(Mat4_t* result, const Mat4_t* a, const Mat4_t* b)
{
    Mat4_t tempMatrix;

    VEC4_Substract(&tempMatrix.x, &a->x, &b->x);
    VEC4_Substract(&tempMatrix.y, &a->y, &b->y);
    VEC4_Substract(&tempMatrix.z, &a->z, &b->z);
    VEC4_Substract(&tempMatrix.w, &a->w, &b->w);

    return MAT4_Assign(result, &tempMatrix);
}

Mat4_t* MAT4_Scale(Mat4_t* result, const Mat4_t* source, float scale)
{
    return MAT4_ScaleXYZ(result, source, scale, scale, scale);
}

Mat4_t* MAT4_ScaleXYZ(
        Mat4_t* result,
        const Mat4_t* source,
        float scaleX,
        float scaleY,
        float scaleZ)
{
    Vec3_t scaleVec;
    VEC3_Init(&scaleVec, scaleX, scaleY, scaleZ);
    return MAT4_ScaleVec3(result, source, &scaleVec);
}

Mat4_t* MAT4_ScaleVec3(Mat4_t* result, const Mat4_t* source, const Vec3_t* scaleVec)
{
    Mat4_t scaleMatrix;
    MAT4_InitScaleVec3(&scaleMatrix, scaleVec);
    return MAT4_Multiply(result, source, &scaleMatrix);
}

Mat4_t* MAT4_InitScale(Mat4_t* result, float scale)
{
    return MAT4_InitScaleXYZ(result, scale, scale, scale);
}

Mat4_t* MAT4_InitScaleXYZ(Mat4_t* result, float scaleX, float scaleY, float scaleZ)
{
    Vec3_t scaleVec;
    VEC3_Init(&scaleVec, scaleX, scaleY, scaleZ);
    return MAT4_InitScaleVec3(result, &scaleVec);
}

Mat4_t* MAT4_InitScaleVec3(Mat4_t* result, const Vec3_t* scaleVec)
{
    MAT4_Clear(result);
    result->x.x = scaleVec->x;
    result->y.y = scaleVec->y;
    result->z.z = scaleVec->z;
    result->w.w = 1.0f;
    return result;
}

Mat4_t* MAT4_Multiply(Mat4_t* result, const Mat4_t* a, const Mat4_t* b)
{
    uint16_t row;
    uint16_t col;
    Vec4_t matrixCol;
    Vec4_t matrixRow;

    for(col = 0; col < MAT4_COL_SIZE; col++) {
        MAT4_Column(&matrixCol, b, col);

        for(row = 0; row < MAT4_ROW_SIZE; row++) {
            MAT4_Row(&matrixRow, a, row);
            result->data[col].data[row] = VEC4_Dot(&matrixCol, &matrixRow);
        }
    }

    return result;
}

Mat4_t* MAT4_InitTranslateXYZ(
        Mat4_t* result,
        float translateX,
        float translateY,
        float translateZ)
{
    Vec3_t translateVec;
    VEC3_Init(&translateVec, translateX, translateY, translateZ);
    return MAT4_InitTranslateVec3(result, &translateVec);
}

Mat4_t* MAT4_InitTranslateVec3(Mat4_t* result, const Vec3_t* translateVec)
{
    MAT4_Identity(result);
    result->w.x = translateVec->x;
    result->w.y = translateVec->y;
    result->w.z = translateVec->z;

    return result;
}

Mat4_t* MAT4_TranslateXYZ(Mat4_t* result, const Mat4_t* source, float translateX, float translateY, float translateZ)
{
    Vec3_t translateVec;
    VEC3_Init(&translateVec, translateX, translateY, translateZ);
    return MAT4_TranslateVec3(result, source, &translateVec);
}

Mat4_t* MAT4_TranslateVec3(Mat4_t* result, const Mat4_t* source, const Vec3_t* translateVec)
{
    Mat4_t translateMatrix;
    MAT4_InitTranslateVec3(&translateMatrix, translateVec);
    return MAT4_Multiply(result, source, &translateMatrix);
}

Mat4_t* MAT4_TranslateInPlace(Mat4_t* result, float x, float y, float z)
{
    Vec4_t translateVec;
    VEC4_Init(&translateVec, x, y, z, 0.0f);

    Vec4_t matrixCol;

    uint16_t col = 0;
    for (col = 0; col < MAT4_COL_SIZE; col++) {
        MAT4_Column(&matrixCol, result, col);
        VEC4_Multiply(&matrixCol, &matrixCol, &translateVec);

        result->w.data[col] =
                matrixCol.x +
                matrixCol.y +
                matrixCol.z +
                matrixCol.w;
    }

    return result;
}

Mat4_t* MAT4_OuterProductVec2(Mat4_t* result, const Vec2_t* a, const Vec2_t* b)
{
    uint16_t colSize = ARRAY_SIZE(a->data);
    uint16_t rowSize = ARRAY_SIZE(b->data);

    float data[colSize * rowSize];
    return MAT4_CastToMat4(
            result,
            colSize,
            rowSize,
            GenerateOuterProduct(
                    &data[0],
                    colSize,
                    rowSize,
                    &a->data[0],
                    &b->data[0]));
}

Mat4_t* MAT4_OuterProductVec3(Mat4_t* result, const Vec3_t* a, const Vec3_t* b)
{
    uint16_t colSize = ARRAY_SIZE(a->data);
    uint16_t rowSize = ARRAY_SIZE(b->data);

    float data[colSize * rowSize];
    return MAT4_CastToMat4(
            result,
            colSize,
            rowSize,
            GenerateOuterProduct(
                    &data[0],
                    colSize,
                    rowSize,
                    &a->data[0],
                    &b->data[0]));
}

Mat4_t* MAT4_OuterProductVec4(Mat4_t* result, const Vec4_t* a, const Vec4_t* b)
{
    uint16_t colSize = ARRAY_SIZE(a->data);
    uint16_t rowSize = ARRAY_SIZE(b->data);

    float data[colSize * rowSize];
    return MAT4_CastToMat4(
            result,
            colSize,
            rowSize,
            GenerateOuterProduct(
                    &data[0],
                    colSize,
                    rowSize,
                    &a->data[0],
                    &b->data[0]));
}

Mat4_t* MAT4_InitRotateXYZ(
        Mat4_t* result,
        float rotateX,
        float rotateY,
        float rotateZ)
{
    Vec3_t rotateVec;
    VEC3_Init(&rotateVec, rotateX, rotateY, rotateZ);
    return MAT4_InitRotateVec3(result, &rotateVec);
}

Mat4_t* MAT4_InitRotateVec3(Mat4_t* result, const Vec3_t* vector)
{
    Mat4_t rx;
    Mat4_t ry;
    Mat4_t rz;

    float x = DEGREES_TO_RADIANS(vector->x);
    float y = DEGREES_TO_RADIANS(vector->y);
    float z = DEGREES_TO_RADIANS(vector->z);

    float sinX = sinf(x);
    float cosX = cosf(x);

    float sinY = sinf(y);
    float cosY = cosf(y);

    float sinZ = sinf(z);
    float cosZ = cosf(z);

    rz.x.x = cosZ; rz.x.y = -sinZ; rz.x.z =  0.0f; rz.x.w = 0.0f;
    rz.y.x = sinZ; rz.y.y =  cosZ; rz.y.z =  0.0f; rz.y.w = 0.0f;
    rz.z.x = 0.0f; rz.z.y =  0.0f; rz.z.z =  1.0f; rz.z.w = 0.0f;
    rz.w.x = 0.0f; rz.w.y =  0.0f; rz.w.z =  0.0f; rz.w.w = 1.0f;

    rx.x.x = 1.0f; rx.x.y =  0.0f; rx.x.z =  0.0f; rx.x.w = 0.0f;
    rx.y.x = 0.0f; rx.y.y =  cosX; rx.y.z = -sinX; rx.y.w = 0.0f;
    rx.z.x = 0.0f; rx.z.y =  sinX; rx.z.z =  cosX; rx.z.w = 0.0f;
    rx.w.x = 0.0f; rx.w.y =  0.0f; rx.w.z =  0.0f; rx.w.w = 1.0f;

    ry.x.x = cosY; ry.x.y =  0.0f; ry.x.z = -sinY; ry.x.w = 0.0f;
    ry.y.x = 0.0f; ry.y.y =  1.0f; ry.y.z =  0.0f; ry.y.w = 0.0f;
    ry.z.x = sinY; ry.z.y =  0.0f; ry.z.z =  cosY; ry.z.w = 0.0f;
    ry.w.x = 0.0f; ry.w.y =  0.0f; ry.w.z =  0.0f; ry.w.w = 1.0f;

    MAT4_Multiply(result, &ry, &rz);
    MAT4_Multiply(result, result, &rz);

    return result;
}

Mat4_t* MAT4_InitRotateForwardUp(Mat4_t* result, const Vec3_t* forward, const Vec3_t* up)
{
    Vec3_t forwardVector;
    VEC3_Normalize(&forwardVector, forward);

    Vec3_t rightVector;
    VEC3_Normalize(&rightVector, up);

    VEC3_Cross(&rightVector, &rightVector, &forwardVector);

    Vec3_t upVector;
    VEC3_Cross(&upVector, &forwardVector, &rightVector);

    return MAT4_InitRotateForwardUpRight(
            result,
            &forwardVector,
            &upVector,
            &rightVector);
}

Mat4_t* MAT4_InitRotateForwardUpRight(Mat4_t* result, const Vec3_t* forward, const Vec3_t* up, const Vec3_t* right)
{
    result->x.x = right->x;
    result->x.y = right->y;
    result->x.z = right->z;
    result->x.w = 0.0f;

    result->y.x = up->x;
    result->y.y = up->y;
    result->y.z = up->z;
    result->y.w = 0.0f;

    result->z.x = forward->x;
    result->z.y = forward->y;
    result->z.z = forward->z;
    result->z.w = 0.0f;

    result->w.x = 0.0f;
    result->w.y = 0.0f;
    result->w.z = 0.0f;
    result->w.w = 1.0f;

    return result;
}

Mat4_t* MAT4_RotateXYZ(
        Mat4_t* result,
        const Mat4_t* source,
        float rotateX,
        float rotateY,
        float rotateZ,
        float angle)
{
    Vec3_t rotateVec;
    VEC3_Init(&rotateVec, rotateX, rotateY, rotateZ);
    return MAT4_RotateVec3(result, source, &rotateVec, angle);
}

Mat4_t* MAT4_RotateVec3(
        Mat4_t* result,
        const Mat4_t* source,
        const Vec3_t* rotateVec,
        float angle)
{
    Mat4_t rotateMatrix;
    MAT4_InitRotateVec3(&rotateMatrix, rotateVec);
    MAT4_Scale(&rotateMatrix, &rotateMatrix, angle);
    return MAT4_Multiply(result, source, &rotateMatrix);
}

Mat4_t* MAT4_Invert(Mat4_t* result, const Mat4_t* src)
{
    float s[6];
    float c[6];

    s[0] = (src->x.x * src->y.y) - (src->y.x * src->x.y);
    s[1] = (src->x.x * src->y.z) - (src->y.x * src->x.z);
    s[2] = (src->x.x * src->y.w) - (src->y.x * src->x.w);
    s[3] = (src->x.y * src->y.z) - (src->y.y * src->x.z);
    s[4] = (src->x.y * src->y.w) - (src->y.y * src->x.w);
    s[5] = (src->x.z * src->y.w) - (src->y.z * src->x.w);

    c[0] = (src->z.x * src->x.y) - (src->x.x * src->z.y);
    c[1] = (src->z.x * src->x.z) - (src->x.x * src->z.z);
    c[2] = (src->z.x * src->x.w) - (src->x.x * src->z.w);
    c[3] = (src->z.y * src->x.z) - (src->x.y * src->z.z);
    c[4] = (src->z.y * src->x.w) - (src->x.y * src->z.w);
    c[5] = (src->z.z * src->x.w) - (src->x.z * src->z.w);

    /* Assume it is invertible */
    float coefficient = 1.0f / ((s[0] * c[5]) - (s[1] * c[4]) + (s[2] * c[3]) + (s[3] * c[2]) - (s[4] * c[1]) + (s[5] * c[0]));

    result->x.x = ( src->y.y * c[5] - src->y.z * c[4] + src->y.w * c[3]) * coefficient;
    result->x.y = (-src->x.y * c[5] + src->x.z * c[4] - src->x.w * c[3]) * coefficient;
    result->x.z = ( src->x.y * s[5] - src->x.z * s[4] + src->x.w * s[3]) * coefficient;
    result->x.w = (-src->z.y * s[5] + src->z.z * s[4] - src->z.w * s[3]) * coefficient;

    result->y.x = (-src->y.x * c[5] + src->y.z * c[2] - src->y.w * c[1]) * coefficient;
    result->y.y = ( src->x.x * c[5] - src->x.z * c[2] + src->x.w * c[1]) * coefficient;
    result->y.z = (-src->x.x * s[5] + src->x.z * s[2] - src->x.w * s[1]) * coefficient;
    result->y.w = ( src->z.x * s[5] - src->z.z * s[2] + src->z.w * s[1]) * coefficient;

    result->z.x = ( src->y.x * c[4] - src->y.y * c[2] + src->y.w * c[0]) * coefficient;
    result->z.y = (-src->x.x * c[4] + src->x.y * c[2] - src->x.w * c[0]) * coefficient;
    result->z.z = ( src->x.x * s[4] - src->x.y * s[2] + src->x.w * s[0]) * coefficient;
    result->z.w = (-src->z.x * s[4] + src->z.y * s[2] - src->z.w * s[0]) * coefficient;

    result->x.x = (-src->y.x * c[3] + src->y.y * c[1] - src->y.z * c[0]) * coefficient;
    result->x.y = ( src->x.x * c[3] - src->x.y * c[1] + src->x.z * c[0]) * coefficient;
    result->x.z = (-src->x.x * s[3] + src->x.y * s[1] - src->x.z * s[0]) * coefficient;
    result->x.w = ( src->z.x * s[3] - src->z.y * s[1] + src->z.z * s[0]) * coefficient;

    return result;
}

Mat4_t* MAT4_RotateMatrixFromQuat(Mat4_t* result, const Quat_t* quat)
{
    MAT4_Clear(result);

    float xx = quat->x * quat->x;
    float xy = quat->x * quat->y;
    float xz = quat->x * quat->z;
    float xw = quat->x * quat->w;

    float yy = quat->y * quat->y;
    float yz = quat->y * quat->z;
    float yw = quat->y * quat->w;

    float zz = quat->z * quat->z;
    float zw = quat->z * quat->w;

    result->x.x = 1.0f - 2.0f * ( yy + zz );
    result->x.y =        2.0f * ( xy + zw );
    result->x.z =        2.0f * ( xz - yw );

    result->y.x =        2.0f * ( xy - zw );
    result->y.y = 1.0f - 2.0f * ( xx + zz );
    result->y.z =        2.0f * ( yz + xw );

    result->z.x =        2.0f * ( xz + yw );
    result->z.y =        2.0f * ( yz - xw );
    result->z.z = 1.0f - 2.0f * ( xx + yy );

    result->w.w = 1.0f;

    return result;
}

Mat4_t* MAT4_OrthoNormalize(Mat4_t* result, const Mat4_t* source)
{
    MAT4_Assign(result, source);

    Vec3_t tmp;

    float scale = 0.0f;

    Vec3_t* col0 = (Vec3_t*)&result->x;
    Vec3_t* col1 = (Vec3_t*)&result->y;
    Vec3_t* col2 = (Vec3_t*)&result->z;

    VEC3_Normalize(col2, col2);

    scale = VEC3_Dot(col1, col2);
    VEC3_Scale(&tmp, col2, scale);
    VEC3_Substract(col1, col1, &tmp);
    VEC3_Normalize(col1, col2);

    scale = VEC3_Dot(col1, col2);
    VEC3_Scale(&tmp, col2, scale);
    VEC3_Substract(col1, col1, &tmp);
    VEC3_Normalize(col1, col1);

    scale = VEC3_Dot(col0, col1);
    VEC3_Scale(&tmp, col1, scale);
    VEC3_Substract(col0, col0, &tmp);
    VEC3_Normalize(col0, col0);

    return result;
}

Mat4_t* MAT4_Initrustum(Mat4_t* result, float left, float right, float bottom, float top, float near, float far)
{
    result->x.x =  2.0f * near / (right - left);
    result->x.y =  result->x.z = result->x.w = 0.0f;

    result->y.y =  2.0f * near / (top - bottom);
    result->y.x =  result->y.z = result->y.w = 0.0f;

    result->z.x =  (right + left) / (right - left);
    result->z.y =  (top + bottom) / (top - bottom);
    result->z.z = -(far + near) / (far - near);
    result->z.w = -1.f;

    result->w.z = -2.0f * (far * near) / (far - near);
    result->w.x =  result->w.y = result->w.w = 0.0f;

    return result;
}
Mat4_t* MAT4_InitOrtho(Mat4_t* result, float left, float right, float bottom, float top, float near, float far)
{
    result->x.x = 2.0f / (right - left);
    result->x.y = result->x.z = result->x.w = 0.0f;

    result->y.y = 2.0f / (top - bottom);
    result->y.x = result->y.z = result->y.w = 0.0f;

    result->z.z = -2.0f / (far - near);
    result->z.x = result->z.y = result->z.w = 0.0f;

    result->w.x = -(right + left)   / (right - left);
    result->w.y = -(top   + bottom) / (top   - bottom);
    result->w.z = -(far   + near)   / (far   - near);
    result->w.w = 1.0f;

    return result;
}
Mat4_t* MAT4_InitPerspective(Mat4_t* result, float fov, float aspect, float near, float far)
{
    MAT4_Clear(result);

    float const coeff = 1.0f / tan(fov / 2.0f);

    result->x.x = coeff / aspect;
    result->x.y = 0.0f;
    result->x.z = 0.0f;
    result->x.w = 0.0f;

    result->y.x = 0.0f;
    result->y.y = coeff;
    result->y.z = 0.0f;
    result->y.w = 0.0f;

    result->z.x = 0.0f;
    result->z.y = 0.0f;
    result->z.z = -((far + near) / (far - near));
    result->z.w = -1.0f;

    result->w.x = 0.0f;
    result->w.y = 0.0f;
    result->w.z = -((2.0f * far * near) / (far - near));
    result->w.w = 0.0f;

    return result;
}

Mat4_t* MAT4_LookAt(Mat4_t* result, const Vec3_t* eye, const Vec3_t* center, const Vec3_t* up)
{
    /* Adapted from Android's OpenGL Matrix.java.                        */
    /* See the OpenGL GLUT documentation for gluLookAt for a description */
    /* of the algorithm. We implement it in a straightforward way:       */

    /* TODO: The negation of of can be spared by swapping the order of
     *       operands in the following cross products in the right way. */
    Vec3_t f;
    VEC3_Substract(&f, center, eye);
    VEC3_Normalize(&f, &f);

    Vec3_t s;
    VEC3_Cross(&s, &f, up);
    VEC3_Normalize(&s, &s);

    Vec3_t t;
    VEC3_Cross(&t, &s, &f);

    result->x.x =  s.x;
    result->x.y =  t.x;
    result->x.z = -f.x;
    result->x.w =  0.0f;

    result->y.x =  s.y;
    result->y.y =  t.y;
    result->y.z = -f.y;
    result->y.w =  0.0f;

    result->z.x =  s.z;
    result->z.y =  t.z;
    result->z.z = -f.z;
    result->z.w =  0.0f;

    result->w.x =  0.0f;
    result->w.y =  0.0f;
    result->w.z =  0.0f;
    result->w.w =  1.0f;

    return MAT4_TranslateInPlace(result, -eye->x, -eye->y, -eye->z);
}

Vec4_t* MAT4_Column(Vec4_t* result, const Mat4_t* source, int colNum)
{
    result->x = source->data[colNum].x;
    result->y = source->data[colNum].y;
    result->z = source->data[colNum].z;
    result->w = source->data[colNum].w;

    return result;
}

Vec4_t* MAT4_Row(Vec4_t* result, const Mat4_t* source, int rowNum)
{
    result->x = source->x.data[rowNum];
    result->y = source->y.data[rowNum];
    result->z = source->z.data[rowNum];
    result->w = source->w.data[rowNum];

    return result;
}

boolean MAT4_Equals(const Mat4_t* a, const Mat4_t* b)
{
    boolean equals = TRUE;

    equals &= VEC4_Equals(&a->x, &b->x);
    equals &= VEC4_Equals(&a->y, &b->y);
    equals &= VEC4_Equals(&a->z, &b->z);
    equals &= VEC4_Equals(&a->w, &b->w);

    return equals;
}

const float* MAT4_Data(const Mat4_t* matrix) {
    return &matrix->data[0].data[0];
}

Mat4_t* MAT4_CastToMat4(
        Mat4_t* result,
        uint16_t numCols,
        uint16_t numRows,
        float* source)
{
    MAT4_Clear(result);

    uint16_t col;
    uint16_t row;

    for (col = 0; col < numCols; col++) {
        for (row = 0; row < numRows; row++) {
            result->data[col].data[row] = source[col * numCols + row];
        }
    }

    return result;
}

void MAT4_Print(const Mat4_t* a, const char* message) {
    const float* value = MAT4_Data(a);

    printf("%s: \n"
            "\t(%f, %f, %f, %f)\n"
            "\t(%f, %f, %f, %f)\n"
            "\t(%f, %f, %f, %f)\n"
            "\t(%f, %f, %f, %f)\n",
            message,
            value[0], value[1], value[2], value[3],
            value[4], value[5], value[6], value[7],
            value[8], value[9], value[10], value[11],
            value[12], value[13], value[14], value[15]);
}

/*-----------------------------------------------
 * PRIVATE METHOD DEFINITIONS
 *-----------------------------------------------
 */

static float* GenerateOuterProduct(
        float* dest,
        uint16_t rowSize,
        uint16_t columnSize,
        const float* rowData,
        const float* columnData)
{
    uint16_t col;
    uint16_t row;

    for(col = 0; col < columnSize; col++) {
        for(row = 0; row < rowSize; row++) {
            dest[col * columnSize + row] = columnData[col] * rowData[row];
        }
    }

    return dest;
}
