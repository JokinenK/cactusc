#ifndef MAT4_H
#define MAT4_H

#include "common/types.h"
#include "vec4.h"

#define MAT4_ROW_SIZE 4
#define MAT4_COL_SIZE 4

/*-----------------------------------------------
 * FORWARD TYPE DECLARATIONS
 *-----------------------------------------------
 */

typedef union Quat_s Quat_t;
typedef union Vec2_s Vec2_t;
typedef union Vec3_s Vec3_t;
typedef union Vec4_s Vec4_t;

/*-----------------------------------------------
 * PUBLIC TYPE DEFINITIONS
 *-----------------------------------------------
 */

typedef union Mat4_s {
    struct {
        Vec4_t x;
        Vec4_t y;
        Vec4_t z;
        Vec4_t w;
    };
    Vec4_t data[4];
} Mat4_t;

/*-----------------------------------------------
 * PUBLIC METHOD DECLARATIONS
 *-----------------------------------------------
 */

extern Mat4_t* MAT4_Init(Mat4_t* result, float* array);

extern Mat4_t* MAT4_Clear(Mat4_t* result);

extern Mat4_t* MAT4_Identity(Mat4_t* result);

extern Mat4_t* MAT4_Assign(Mat4_t* result, const Mat4_t* source);

extern Mat4_t* MAT4_Transpose(Mat4_t* result, const Mat4_t* source);

extern Mat4_t* MAT4_Add(Mat4_t* result, const Mat4_t* a, const Mat4_t* b);

extern Mat4_t* MAT4_Substract(Mat4_t* result, const Mat4_t* a, const Mat4_t* b);

extern Mat4_t* MAT4_Scale(Mat4_t* result, const Mat4_t* a, float s);

extern Mat4_t* MAT4_ScaleXYZ(Mat4_t* result, const Mat4_t* a, float x, float y, float z);

extern Mat4_t* MAT4_ScaleVec3(Mat4_t* result, const Mat4_t* a, const Vec3_t* vec);

extern Mat4_t* MAT4_InitScale(Mat4_t* result, float scale);

extern Mat4_t* MAT4_InitScaleXYZ(Mat4_t* result, float scaleX, float scaleY, float scaleZ);

extern Mat4_t* MAT4_InitScaleVec3(Mat4_t* result, const Vec3_t* vec);

extern Mat4_t* MAT4_Multiply(Mat4_t* result, const Mat4_t* a, const Mat4_t* b);

extern Mat4_t* MAT4_MultiplyQuat(Mat4_t* result, const Mat4_t* matrix, const Quat_t* quat);

extern Mat4_t* MAT4_OuterProductVec2(Mat4_t* result, const Vec2_t* a, const Vec2_t* b);

extern Mat4_t* MAT4_OuterProductVec3(Mat4_t* result, const Vec3_t* a, const Vec3_t* b);

extern Mat4_t* MAT4_OuterProductVec4(Mat4_t* result, const Vec4_t* a, const Vec4_t* b);

extern Mat4_t* MAT4_InitTranslateXYZ(Mat4_t* result, float translateX, float translateY, float translateZ);

extern Mat4_t* MAT4_InitTranslateVec3(Mat4_t* result, const Vec3_t* vector);

extern Mat4_t* MAT4_TranslateXYZ(Mat4_t* result, const Mat4_t* source, float translateX, float translateY, float translateZ);

extern Mat4_t* MAT4_TranslateVec3(Mat4_t* result, const Mat4_t* source, const Vec3_t* vector);

extern Mat4_t* MAT4_TranslateInPlace(Mat4_t* result, float x, float y, float z);

extern Mat4_t* MAT4_InitRotateXYZ(Mat4_t* result, float rotateX, float rotateY, float rotateZ);

extern Mat4_t* MAT4_InitRotateVec3(Mat4_t* result, const Vec3_t* rotateVec);

extern Mat4_t* MAT4_InitRotateForwardUp(Mat4_t* result, const Vec3_t* forward, const Vec3_t* up);

extern Mat4_t* MAT4_InitRotateForwardUpRight(Mat4_t* result, const Vec3_t* forward, const Vec3_t* up, const Vec3_t* right);

extern Mat4_t* MAT4_RotateXYZ(Mat4_t* result, const Mat4_t* source, float x, float y, float z, float angle);

extern Mat4_t* MAT4_RotateVec3(Mat4_t* result, const Mat4_t* source, const Vec3_t* rotateVec, float angle);

extern Mat4_t* MAT4_Invert(Mat4_t* result, const Mat4_t* src);

extern Mat4_t* MAT4_RotateMatrixFromQuat(Mat4_t* result, const Quat_t* quat);

extern Mat4_t* MAT4_OrthoNormalize(Mat4_t* result, const Mat4_t* source);

extern Mat4_t* MAT4_InitFrustum(Mat4_t* result, float l, float r, float b, float t, float n, float f);

extern Mat4_t* MAT4_InitOrtho(Mat4_t* result, float l, float r, float b, float t, float n, float f);

extern Mat4_t* MAT4_InitPerspective(Mat4_t* result, float fov, float aspect, float near, float far);

extern Mat4_t* MAT4_LookAt(Mat4_t* result, const Vec3_t* eye, const Vec3_t* center, const Vec3_t* up);

extern Vec4_t* MAT4_Row(Vec4_t* result, const Mat4_t* source, int rowNum);

extern Vec4_t* MAT4_Column(Vec4_t* result, const Mat4_t* source, int colNum);

extern const float* MAT4_Data(const Mat4_t* matrix);

extern Mat4_t* MAT4_CastToMat4(
        Mat4_t* result,
        uint16_t numCols,
        uint16_t numRows,
        float* source);

extern void MAT4_Print(const Mat4_t* a, const char* message);

extern boolean MAT4_Equals(const Mat4_t* a, const Mat4_t* b);

#endif /* MAT4_H */
