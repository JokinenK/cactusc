#include <stdlib.h>
#include "quat.h"
#include "common/types.h"
#include "vec3.h"
#include "mat4.h"

/*-----------------------------------------------
 * PUBLIC METHOD DECLARATIONS
 *-----------------------------------------------
 */

Quat_t* QUAT_Init(Quat_t* result, float x, float y, float z, float w)
{
    result->x = x;
    result->y = y;
    result->z = z;
    result->w = w;

    return result;
}

Quat_t* QUAT_Identity(Quat_t* result)
{
    return QUAT_Init(result, 0.0f, 0.0f, 0.0f, 1.0f);
}

Quat_t* QUAT_Assign(Quat_t* result, const Quat_t* source)
{
    result->x = source->x;
    result->y = source->y;
    result->z = source->z;
    result->w = source->w;

    return result;
}

Quat_t* QUAT_Add(Quat_t* result, const Quat_t* a, const Quat_t* b)
{
    result->x = a->x + b->x;
    result->y = a->y + b->y;
    result->z = a->z + b->z;
    result->w = a->w + b->w;

    return QUAT_Normalize(result, result);
}

Quat_t* QUAT_Substract(Quat_t* result, const Quat_t* a, const Quat_t* b)
{
    result->x = a->x - b->x;
    result->y = a->y - b->y;
    result->z = a->z - b->z;
    result->w = a->w - b->w;

    return QUAT_Normalize(result, result);
}

Quat_t* QUAT_Multiply(Quat_t* result, const Quat_t* a, const Quat_t* b)
{
    result->x = a->x * b->w + a->w * b->x + a->y * b->z - a->z * b->y;
    result->y = a->y * b->w + a->w * b->y + a->z * b->x - a->x * b->z;
    result->z = a->z * b->w + a->w * b->z + a->x * b->y - a->y * b->x;
    result->w = a->w * b->w - a->x * b->x - a->y * b->y - a->z * b->z;

    return QUAT_Normalize(result, result);
}

Quat_t* QUAT_MultiplyVec3(
        Quat_t* result,
        const Quat_t* source,
        const Vec3_t* vec)
{
    result->x =  source->w * vec->x + source->y * vec->z - source->z * vec->y;
    result->y =  source->w * vec->y + source->z * vec->x - source->x * vec->z;
    result->z =  source->w * vec->z + source->x * vec->y - source->y * vec->x;
    result->w = -source->x * vec->x - source->y * vec->y - source->z * vec->z;

    return QUAT_Normalize(result, result);
}

Quat_t* QUAT_Scale(Quat_t* result, const Quat_t* source, float scale)
{
    result->x = source->x * scale;
    result->y = source->y * scale;
    result->z = source->z * scale;
    result->w = source->w * scale;

    return QUAT_Normalize(result, result);
}

float QUAT_InnerProduct(const Quat_t* a, const Quat_t* b)
{
    float sum = 0.0f;

    sum += a->x * b->x;
    sum += a->y * b->y;
    sum += a->z * b->z;
    sum += a->w * b->w;

    return sum;
}

Quat_t* QUAT_Conjugate(Quat_t* result, const Quat_t* source)
{
    result->x = -source->x;
    result->y = -source->y;
    result->z = -source->z;
    result->w =  source->w;

    return result;
}

Quat_t* QUAT_InitRotateVec3(Quat_t* result, const Vec3_t* axis, float angle)
{
    float sinHalfAngle = sinf(angle / 2);
    float cosHalfAngle = cosf(angle / 2);

    QUAT_Init(
            result,
            axis->x * sinHalfAngle,
            axis->y * sinHalfAngle,
            axis->z * sinHalfAngle,
            cosHalfAngle);

    return QUAT_Normalize(result, result);
}

Quat_t* QUAT_InitRotateMat4(Quat_t* result, const Mat4_t* rot)
{
    float trace = rot->x.x + rot->y.y + rot->z.z;

    if (trace > 0.0f) {
        float scale = sqrt(trace + 1.0f) * 2.0f;

        result->x = (rot->z.y - rot->y.z) / scale;
        result->y = (rot->x.z - rot->z.x) / scale;
        result->z = (rot->y.x - rot->x.y) / scale;
        result->w = 0.25f * scale;

    } else if ((rot->x.x > rot->y.y)
            && (rot->x.x > rot->z.z)) {
        float scale = sqrtf(1.0f + rot->x.x - rot->y.y - rot->z.z) * 2.0f;

        result->x = 0.25f * scale;
        result->y = (rot->x.y + rot->y.x) / scale;
        result->z = (rot->x.z + rot->z.x) / scale;
        result->w = (rot->z.y - rot->y.z) / scale;

    } else if (rot->y.y > rot->z.z) {
        float scale = sqrtf(1.0f + rot->y.y - rot->x.x - rot->z.z) * 2.0f;

        result->x = (rot->x.y + rot->y.x) / scale;
        result->y = 0.25f * scale;
        result->z = (rot->y.z + rot->z.y) / scale;
        result->w = (rot->x.z - rot->z.x) / scale;

    } else {
        float scale = sqrtf(1.0f + rot->z.z - rot->x.x - rot->y.y) * 2.0f;

        result->x = (rot->x.z + rot->z.x) / scale;
        result->y = (rot->y.z + rot->z.y) / scale;
        result->z = 0.25f * scale;
        result->w = (rot->y.x - rot->x.y) / scale;
    }

    return QUAT_Normalize(result, result);
}

float QUAT_Length(const Quat_t* source)
{
    float x = source->x * source->x;
    float y = source->y * source->y;
    float z = source->z * source->z;
    float w = source->w * source->w;

    return sqrtf(x + y + z + w);
}

Quat_t* QUAT_Normalize(Quat_t* result, const Quat_t* source)
{
    float length = QUAT_Length(source);

    return QUAT_Init(
            result,
            source->x / length,
            source->y / length,
            source->z / length,
            source->w / length);
}

boolean QUAT_Equals(const Quat_t* a, const Quat_t* b)
{
    return (FLOATS_EQ(a->x, b->x)
            && FLOATS_EQ(a->y, b->y)
            && FLOATS_EQ(a->z, b->z)
            && FLOATS_EQ(a->w, b->w));
}
