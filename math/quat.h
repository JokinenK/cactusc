#ifndef QUAT_H
#define QUAT_H

#include "common/types.h"

/*-----------------------------------------------
 * FORWARD TYPE DECLARATIONS
 *-----------------------------------------------
 */

typedef union Vec3_s Vec3_t;
typedef union Mat4_s Mat4_t;

/*-----------------------------------------------
 * PUBLIC TYPE DEFINITIONS
 *-----------------------------------------------
 */

typedef union Quat_s {
    struct {
        float x;
        float y;
        float z;
        float w;
    };
    float data[4];
} Quat_t;

/*-----------------------------------------------
 * PUBLIC METHOD DECLARATIONS
 *-----------------------------------------------
 */

extern Quat_t* QUAT_Init(Quat_t* result, float x, float y, float z, float w);

extern Quat_t* QUAT_Identity(Quat_t* result);

extern Quat_t* QUAT_Assign(Quat_t* result, const Quat_t* source);

extern Quat_t* QUAT_Add(Quat_t* result, const Quat_t* a, const Quat_t* b);

extern Quat_t* QUAT_Substract(Quat_t* result, const Quat_t* a, const Quat_t* b);

extern Quat_t* QUAT_Multiply(Quat_t* result, const Quat_t* a, const Quat_t* b);

extern Quat_t* QUAT_MultiplyVec3(Quat_t* result, const Quat_t* quat, const Vec3_t* vec);

extern Quat_t* QUAT_Scale(Quat_t* result, const Quat_t* source, float scale);

extern float QUAT_InnerProduct(const Quat_t* a, const Quat_t* b);

extern Quat_t* QUAT_Conjugate(Quat_t* result, const Quat_t* quat);

extern Quat_t* QUAT_InitRotateVec3(Quat_t* result, const Vec3_t* axis, float angle);

extern Quat_t* QUAT_InitRotateMat4(Quat_t* result, const Mat4_t* rot);

extern float QUAT_Length(const Quat_t* source);

extern Quat_t* QUAT_Normalize(Quat_t* result, const Quat_t* source);

extern boolean QUAT_Equals(const Quat_t* a, const Quat_t* b);

#endif /* QUAT_H */
