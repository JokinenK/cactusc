#include <stdlib.h>
#include <math.h>
#include "vec2.h"

/*-----------------------------------------------
 * PUBLIC METHOD DECLARATIONS
 *-----------------------------------------------
 */

Vec2_t* VEC2_Init(Vec2_t* result, float x, float y)
{
    result->x = x;
    result->y = y;

    return result;
}

Vec2_t* VEC2_Assign(Vec2_t* result, const Vec2_t* source)
{
    result->x = source->x;
    result->y = source->y;

    return result;
}

Vec2_t* VEC2_Add(Vec2_t* result, const Vec2_t* a, const Vec2_t* b)
{
    result->x = a->x + b->x;
    result->y = a->y + b->y;

    return result;
}

Vec2_t* VEC2_Substract(Vec2_t* result, const Vec2_t* a, const Vec2_t* b)
{
    result->x = a->x - b->x;
    result->y = a->y - b->y;

    return result;
}

Vec2_t* VEC2_Scale(Vec2_t* result, const Vec2_t* a, const float s)
{
    result->x = a->x * s;
    result->y = a->y * s;

    return result;
}

Vec2_t* VEC2_Multiply(Vec2_t* result, const Vec2_t* a, const Vec2_t* b)
{
    result->x = a->x * b->x;
    result->y = a->y * b->y;

    return result;
}

Vec2_t* VEC2_Divide(Vec2_t* result, const Vec2_t* a, const Vec2_t* b)
{
    if (!FLOATS_EQ(b->x, 0.0f)
            && !FLOATS_EQ(b->y, 0.0f)) {
        result->x = a->x / b->x;
        result->y = a->y / b->y;
    }

    return result;
}

Vec2_t* VEC2_Normalize(Vec2_t* result, const Vec2_t* v)
{
    float coefficient = 1.0 / VEC2_Length(v);
    return VEC2_Scale(result, v, coefficient);
}

Vec2_t* VEC2_Min(Vec2_t* result, const Vec2_t* a, const Vec2_t* b)
{
    result->x = MIN(a->x, b->x);
    result->y = MIN(a->y, b->y);

    return result;
}

Vec2_t* VEC2_Max(Vec2_t* result, const Vec2_t* a, const Vec2_t* b)
{
    result->x = MAX(a->x, b->x);
    result->y = MAX(a->y, b->y);

    return result;
}

Vec2_t* VEC2_Invert(Vec2_t* result, const Vec2_t* v)
{
    result->x = -v->x;
    result->y = -v->y;

    return result;
}

float VEC2_Cross(const Vec2_t* a, const Vec2_t* b)
{
    return (a->x * b->y) - (a->y * b->x);
}

float VEC2_Dot(const Vec2_t* a, const Vec2_t* b)
{
    Vec2_t result;
    VEC2_Multiply(&result, a, b);
    return (result.x + result.y);
}

float VEC2_Length(const Vec2_t* v)
{
    return sqrtf(VEC2_Dot(v, v));
}

Vec2_t* VEC2_Reflect(Vec2_t* result, const Vec2_t* vector, const Vec2_t* normal)
{
    float coefficient = 2.f * VEC2_Dot(vector, normal);
    result->x = vector->x - (coefficient * normal->x);
    result->y = vector->y - (coefficient * normal->y);

    return result;
}

Vec2_t* VEC2_Lerp(Vec2_t* result, const Vec2_t* a, const Vec2_t* b, float t) {
    result->x = a->x + (t * ( b->x - a->x));
    result->y = a->y + (t * ( b->y - a->y));

    return result;
}

boolean VEC2_Equals(const Vec2_t* a, const Vec2_t* b)
{
    return (FLOATS_EQ(a->x, b->x) && FLOATS_EQ(a->y, b->y));
}
