#ifndef VEC2_H
#define VEC2_H

#include "common/types.h"

/*-----------------------------------------------
 * PUBLIC TYPE DEFINITIONS
 *-----------------------------------------------
 */

typedef union Vec2_s {
    struct {
        float x;
        float y;
    };
    float data[2];
} Vec2_t;

/*-----------------------------------------------
 * PUBLIC METHOD DECLARATIONS
 *-----------------------------------------------
 */

extern Vec2_t* VEC2_Init(Vec2_t* result, float x, float y);

extern Vec2_t* VEC2_Assign(Vec2_t* result, const Vec2_t* source);

extern Vec2_t* VEC2_Add(Vec2_t* result, const Vec2_t* a, const Vec2_t* b);

extern Vec2_t* VEC2_Substract(Vec2_t* result, const Vec2_t* a, const Vec2_t* b);

extern Vec2_t* VEC2_Scale(Vec2_t* result, const Vec2_t* a, const float s);

extern Vec2_t* VEC2_Multiply(Vec2_t* result, const Vec2_t* a, const Vec2_t* b);

extern Vec2_t* VEC2_Divide(Vec2_t* result, const Vec2_t* a, const Vec2_t* b);

extern Vec2_t* VEC2_Normalize(Vec2_t* result, const Vec2_t* v);

extern Vec2_t* VEC2_Min(Vec2_t* result, const Vec2_t* a, const Vec2_t* b);

extern Vec2_t* VEC2_Max(Vec2_t* result, const Vec2_t* a, const Vec2_t* b);

extern Vec2_t* VEC2_Invert(Vec2_t* result, const Vec2_t* v);

extern float VEC2_Cross(const Vec2_t* a, const Vec2_t* b);

extern float VEC2_Dot(const Vec2_t* a, const Vec2_t* b);

extern float VEC2_Length(const Vec2_t* v);

extern Vec2_t* VEC2_Reflect(Vec2_t* result, const Vec2_t* v, const Vec2_t* n);

extern Vec2_t* VEC2_Lerp(Vec2_t* result, const Vec2_t* a, const Vec2_t* b, float t);

extern boolean VEC2_Equals(const Vec2_t* a, const Vec2_t* b);

#endif /* VEC2_H */
