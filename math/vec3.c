#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "vec3.h"
#include "quat.h"
#include "mat4.h"

/*-----------------------------------------------
 * PUBLIC VARIABLE DECLARATIONS
 *-----------------------------------------------
 */

const Vec3_t VEC3_X_POS = {.x =  1.0f, .y =  0.0f, .z =  0.0f};
const Vec3_t VEC3_X_NEG = {.x = -1.0f, .y =  0.0f, .z =  0.0f};

const Vec3_t VEC3_Y_POS = {.x =  0.0f, .y =  1.0f, .z =  0.0f};
const Vec3_t VEC3_Y_NEG = {.x =  0.0f, .y = -1.0f, .z =  0.0f};

const Vec3_t VEC3_Z_POS = {.x =  0.0f, .y =  0.0f, .z =  1.0f};
const Vec3_t VEC3_Z_NEG = {.x =  0.0f, .y =  0.0f, .z = -1.0f};

/*-----------------------------------------------
 * PUBLIC METHOD DECLARATIONS
 *-----------------------------------------------
 */

Vec3_t* VEC3_Init(Vec3_t* result, float x, float y, float z)
{
    result->x = x;
    result->y = y;
    result->z = z;

    return result;
}

Vec3_t* VEC3_Assign(Vec3_t* result, const Vec3_t* source)
{
    result->x = source->x;
    result->y = source->y;
    result->z = source->z;

    return result;
}

Vec3_t* VEC3_Add(Vec3_t* result, const Vec3_t* a, const Vec3_t* b)
{
    result->x = a->x + b->x;
    result->y = a->y + b->y;
    result->z = a->z + b->z;

    return result;
}

Vec3_t* VEC3_Substract(Vec3_t* result, const Vec3_t* a, const Vec3_t* b)
{
    result->x = a->x - b->x;
    result->y = a->y - b->y;
    result->z = a->z - b->z;

    return result;
}

Vec3_t* VEC3_Scale(Vec3_t* result, const Vec3_t* a, const float s)
{
    result->x = a->x * s;
    result->y = a->y * s;
    result->z = a->z * s;

    return result;
}

Vec3_t* VEC3_Multiply(Vec3_t* result, const Vec3_t* a, const Vec3_t* b)
{
    result->x = a->x * b->x;
    result->y = a->y * b->y;
    result->z = a->z * b->z;

    return result;
}

Vec3_t* VEC3_Divide(Vec3_t* result, const Vec3_t* a, const Vec3_t* b)
{
    if (!FLOATS_EQ(b->x, 0.0f)
            && !FLOATS_EQ(b->y, 0.0f)
            && !FLOATS_EQ(b->z, 0.0f)) {
        result->x = a->x / b->x;
        result->y = a->y / b->y;
        result->z = a->z / b->z;
    }

    return result;
}

Vec3_t* VEC3_Normalize(Vec3_t* result, const Vec3_t* v)
{
    float coefficient = 1.0 / VEC3_Length(v);
    return VEC3_Scale(result, v, coefficient);
}

Vec3_t* VEC3_Min(Vec3_t* result, const Vec3_t* a, const Vec3_t* b)
{
    result->x = MIN(a->x, b->x);
    result->y = MIN(a->y, b->y);
    result->z = MIN(a->z, b->z);

    return result;
}

Vec3_t* VEC3_Max(Vec3_t* result, const Vec3_t* a, const Vec3_t* b)
{
    result->x = MAX(a->x, b->x);
    result->y = MAX(a->y, b->y);
    result->z = MAX(a->z, b->z);

    return result;
}

Vec3_t* VEC3_Invert(Vec3_t* result, const Vec3_t* v)
{
    result->x = -v->x;
    result->y = -v->y;
    result->z = -v->z;

    return result;
}

Vec3_t* VEC3_Cross(Vec3_t* result, const Vec3_t* a, const Vec3_t* b)
{
    Vec3_t tempVector;
    tempVector.x = (a->y * b->z) - (a->z * b->y);
    tempVector.y = (a->z * b->x) - (a->x * b->z);
    tempVector.z = (a->x * b->y) - (a->y * b->x);

    return VEC3_Assign(result, &tempVector);
}

float VEC3_Dot(const Vec3_t* a, const Vec3_t* b)
{
    Vec3_t result;
    VEC3_Multiply(&result, a, b);

    return result.x + result.y + result.z;
}

float VEC3_Length(const Vec3_t* v)
{
    return sqrtf(VEC3_Dot(v, v));
}

Vec3_t* VEC3_Reflect(Vec3_t* result, const Vec3_t* v, const Vec3_t* n)
{
    float coefficient = 2.f * VEC3_Dot(v, n);

    result->x = v->x - (coefficient * n->x);
    result->y = v->y - (coefficient * n->y);
    result->z = v->z - (coefficient * n->z);

    return result;
}

Vec3_t* VEC3_Lerp(Vec3_t* result, const Vec3_t* a, const Vec3_t* b, float t)
{
    result->x = a->x + (t * ( b->x - a->x));
    result->y = a->y + (t * ( b->y - a->y));
    result->z = a->z + (t * ( b->z - a->z));

    return result;
}

Vec3_t* VEC3_RotateQuat(Vec3_t* result, const Quat_t* rotation, const Vec3_t* vector)
{
    Vec3_t uv;
    Vec3_t uuv;
    Vec3_t qvec;

    qvec.x = rotation->x;
    qvec.y = rotation->y;
    qvec.z = rotation->z;

    VEC3_Cross(&uv, &qvec, vector);
    VEC3_Cross(&uuv, &qvec, &uv);

    VEC3_Scale(&uv, &uv, (2.0f * rotation->w));
    VEC3_Scale(&uuv, &uuv, 2.0f);

    VEC3_Add(result, vector, &uv);
    VEC3_Add(result, result, &uuv);

    return result;
}

Vec3_t* VEC3_Transform(Vec3_t* result, const Vec3_t* source, const Mat4_t* matrix)
{
    return VEC3_Init(
            result,
            matrix->x.x * source->x + matrix->x.y * source->y + matrix->x.z * source->z + matrix->x.w,
            matrix->y.x * source->x + matrix->y.y * source->y + matrix->y.z * source->z + matrix->y.w,
            matrix->z.x * source->x + matrix->z.y * source->y + matrix->z.z * source->z + matrix->z.w);
}

void VEC3_Print(const Vec3_t* vec, const char* message)
{
    printf("%s: (%f, %f, %f)\n", message, vec->x, vec->y, vec->z);
}

boolean VEC3_Equals(const Vec3_t* a, const Vec3_t* b)
{
    return (FLOATS_EQ(a->x, b->x)
            && FLOATS_EQ(a->y, b->y)
            && FLOATS_EQ(a->z, b->z));
}
