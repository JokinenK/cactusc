#ifndef VEC3_H
#define VEC3_H

#include "common/types.h"

#define VEC3_FORWARD    (VEC3_Z_NEG)
#define VEC3_BACK       (VEC3_Z_POS)

#define VEC3_LEFT       (VEC3_X_NEG)
#define VEC3_RIGHT      (VEC3_X_POS)

#define VEC3_UP         (VEC3_Y_POS)
#define VEC3_DOWN       (VEC3_Y_NEG)

/*-----------------------------------------------
 * FORWARD TYPE DECLARATIONS
 *-----------------------------------------------
 */

typedef union Quat_s Quat_t;
typedef union Mat4_s Mat4_t;

/*-----------------------------------------------
 * PUBLIC TYPE DEFINITIONS
 *-----------------------------------------------
 */

typedef union Vec3_s {
    struct {
        float x;
        float y;
        float z;
    };
    float data[3];
} Vec3_t;

/*-----------------------------------------------
 * PUBLIC VARIABLE DEFINITIONS
 *-----------------------------------------------
 */

extern const Vec3_t VEC3_X_POS;
extern const Vec3_t VEC3_X_NEG;

extern const Vec3_t VEC3_Y_POS;
extern const Vec3_t VEC3_Y_NEG;

extern const Vec3_t VEC3_Z_POS;
extern const Vec3_t VEC3_Z_NEG;

/*-----------------------------------------------
 * PUBLIC METHOD DECLARATIONS
 *-----------------------------------------------
 */

extern Vec3_t* VEC3_Init(Vec3_t* result, float x, float y, float z);

extern Vec3_t* VEC3_Assign(Vec3_t* result, const Vec3_t* source);

extern Vec3_t* VEC3_Add(Vec3_t* result, const Vec3_t* a, const Vec3_t* b);

extern Vec3_t* VEC3_Substract(Vec3_t* result, const Vec3_t* a, const Vec3_t* b);

extern Vec3_t* VEC3_Scale(Vec3_t* result, const Vec3_t* a, const float s);

extern Vec3_t* VEC3_Multiply(Vec3_t* result, const Vec3_t* a, const Vec3_t* b);

extern Vec3_t* VEC3_Divide(Vec3_t* result, const Vec3_t* a, const Vec3_t* b);

extern Vec3_t* VEC3_Normalize(Vec3_t* result, const Vec3_t* v);

extern Vec3_t* VEC3_Min(Vec3_t* result, const Vec3_t* a, const Vec3_t* b);

extern Vec3_t* VEC3_Max(Vec3_t* result, const Vec3_t* a, const Vec3_t* b);

extern Vec3_t* VEC3_Invert(Vec3_t* result, const Vec3_t* v);

extern Vec3_t* VEC3_Cross(Vec3_t* result, const Vec3_t* a, const Vec3_t* b);

extern float VEC3_Dot(const Vec3_t* a, const Vec3_t* b);

extern float VEC3_Length(const Vec3_t* v);

extern Vec3_t* VEC3_Reflect(Vec3_t* result, const Vec3_t* v, const Vec3_t* n);

extern Vec3_t* VEC3_Lerp(Vec3_t* result, const Vec3_t* a, const Vec3_t* b, float t);

extern Vec3_t* VEC3_RotateQuat(Vec3_t* result, const Quat_t* quat, const Vec3_t* vector);

extern Vec3_t* VEC3_Transform(Vec3_t* result, const Vec3_t* source, const Mat4_t* matrix);

extern void VEC3_Print(const Vec3_t* vec, const char* message);

extern boolean VEC3_Equals(const Vec3_t* a, const Vec3_t* b);

#endif /* VEC3_H */
