#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "vec4.h"
#include "mat4.h"

/*-----------------------------------------------
 * PUBLIC METHOD DECLARATIONS
 *-----------------------------------------------
 */

Vec4_t* VEC4_Init(Vec4_t* result, float x, float y, float z, float w)
{
    result->x = x;
    result->y = y;
    result->z = z;
    result->w = z;

    return result;
}

Vec4_t* VEC4_Assign(Vec4_t* result, const Vec4_t* source)
{
    result->x = source->x;
    result->y = source->y;
    result->z = source->z;
    result->w = source->w;

    return result;
}

Vec4_t* VEC4_Add(Vec4_t* result, const Vec4_t* a, const Vec4_t* b)
{
    result->x = a->x + b->x;
    result->y = a->y + b->y;
    result->z = a->z + b->z;
    result->w = a->w + b->w;

    return result;
}

Vec4_t* VEC4_Substract(Vec4_t* result, const Vec4_t* a, const Vec4_t* b)
{
    result->x = a->x - b->x;
    result->y = a->y - b->y;
    result->z = a->z - b->z;
    result->w = a->w - b->w;

    return result;
}

Vec4_t* VEC4_Scale(Vec4_t* result, const Vec4_t* a, const float s)
{
    result->x = a->x * s;
    result->y = a->y * s;
    result->z = a->z * s;
    result->w = a->w * s;

    return result;
}

Vec4_t* VEC4_Multiply(Vec4_t* result, const Vec4_t* a, const Vec4_t* b)
{
    result->x = a->x * b->x;
    result->y = a->y * b->y;
    result->z = a->z * b->z;
    result->w = a->w * b->w;

    return result;
}

Vec4_t* VEC4_MultiplyMat4(Vec4_t* result, const Mat4_t* a, const Vec4_t* b)
{
    Vec4_t matrixRow;

    uint16_t row = 0;
    for (row = 0; row < MAT4_ROW_SIZE; row++) {
        MAT4_Row(&matrixRow, a, row);
        result->data[row] = VEC4_Dot(&matrixRow, b);
    }
}

Vec4_t* VEC4_Divide(Vec4_t* result, const Vec4_t* a, const Vec4_t* b)
{
    if (!FLOATS_EQ(b->x, 0.0f)
            && !FLOATS_EQ(b->y, 0.0f)
            && !FLOATS_EQ(b->z, 0.0f)
            && !FLOATS_EQ(b->w, 0.0f)) {
        result->x = a->x / b->x;
        result->y = a->y / b->y;
        result->z = a->z / b->z;
        result->w = a->w / b->w;
    }

    return result;
}

Vec4_t* VEC4_Normalize(Vec4_t* result, const Vec4_t* v)
{
    float coefficient = 1.0 / VEC4_Length(v);
    return VEC4_Scale(result, v, coefficient);
}

Vec4_t* VEC4_Min(Vec4_t* result, const Vec4_t* a, const Vec4_t* b)
{
    result->x = MIN(a->x, b->x);
    result->y = MIN(a->y, b->y);
    result->z = MIN(a->z, b->z);
    result->w = MIN(a->w, b->w);

    return result;
}

Vec4_t* VEC4_Max(Vec4_t* result, const Vec4_t* a, const Vec4_t* b)
{
    result->x = MAX(a->x, b->x);
    result->y = MAX(a->y, b->y);
    result->z = MAX(a->z, b->z);
    result->w = MAX(a->w, b->w);

    return result;
}

Vec4_t* VEC4_Invert(Vec4_t* result, const Vec4_t* v)
{
    result->x = -v->x;
    result->y = -v->y;
    result->z = -v->z;
    result->w = -v->w;

    return result;
}

Vec4_t* VEC4_Cross(Vec4_t* result, const Vec4_t* a, const Vec4_t* b)
{
    Vec4_t tempVector;
    tempVector.x = (a->y * b->z) - (a->z * b->y);
    tempVector.y = (a->z * b->x) - (a->x * b->z);
    tempVector.z = (a->x * b->y) - (a->y * b->x);
    tempVector.w = 1.f;

    return VEC4_Assign(result, &tempVector);
}

float VEC4_Dot(const Vec4_t* a, const Vec4_t* b)
{
    Vec4_t result;
    VEC4_Multiply(&result, a, b);
    return (result.x + result.y + result.z + result.w);
}

float VEC4_Length(const Vec4_t* v)
{
    return sqrtf(VEC4_Dot(v, v));
}

Vec4_t* VEC4_Reflect(Vec4_t* result, const Vec4_t* v, const Vec4_t* n)
{
    float coefficient = 2.f * VEC4_Dot(v, n);
    result->x = v->x - (coefficient * n->x);
    result->y = v->y - (coefficient * n->y);
    result->z = v->z - (coefficient * n->z);
    result->w = v->w - (coefficient * n->w);

    return result;
}

Vec4_t* VEC4_Lerp(Vec4_t* result, const Vec4_t* a, const Vec4_t* b, float t) {
    result->x = a->x + (t * ( b->x - a->x));
    result->y = a->y + (t * ( b->y - a->y));
    result->z = a->z + (t * ( b->z - a->z));
    result->w = a->w + (t * ( b->w - a->w));

    return result;
}

void VEC4_Print(const Vec4_t* vec, const char* message)
{
    printf("%s: (%f, %f, %f, %f)\n", message, vec->x, vec->y, vec->z, vec->w);
}

boolean VEC4_Equals(const Vec4_t* a, const Vec4_t* b)
{
    return (FLOATS_EQ(a->x, b->x)
            && FLOATS_EQ(a->y, b->y)
            && FLOATS_EQ(a->z, b->z)
            && FLOATS_EQ(a->w, b->w));
}
