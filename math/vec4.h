#ifndef VEC4_H
#define VEC4_H

#include "common/types.h"

/*-----------------------------------------------
 * FORWARD TYPE DECLARATIONS
 *-----------------------------------------------
 */

typedef union Mat4_s Mat4_t;

/*-----------------------------------------------
 * PUBLIC TYPE DEFINITIONS
 *-----------------------------------------------
 */

typedef union Vec4_s {
    struct {
        float x;
        float y;
        float z;
        float w;
    };
    float data[4];
} Vec4_t;

/*-----------------------------------------------
 * PUBLIC METHOD DECLARATIONS
 *-----------------------------------------------
 */

extern Vec4_t* VEC4_Init(Vec4_t* result, float x, float y, float z, float w);

extern Vec4_t* VEC4_Assign(Vec4_t* result, const Vec4_t* source);

extern Vec4_t* VEC4_Add(Vec4_t* result, const Vec4_t* a, const Vec4_t* b);

extern Vec4_t* VEC4_Substract(Vec4_t* result, const Vec4_t* a, const Vec4_t* b);

extern Vec4_t* VEC4_Scale(Vec4_t* result, const Vec4_t* a, const float s);

extern Vec4_t* VEC4_Multiply(Vec4_t* result, const Vec4_t* a, const Vec4_t* b);

extern Vec4_t* VEC4_MultiplyMat4(Vec4_t* result, const Mat4_t* a, const Vec4_t* b);

extern Vec4_t* VEC4_Divide(Vec4_t* result, const Vec4_t* a, const Vec4_t* b);

extern Vec4_t* VEC4_Normalize(Vec4_t* result, const Vec4_t* v);

extern Vec4_t* VEC4_Min(Vec4_t* result, const Vec4_t* a, const Vec4_t* b);

extern Vec4_t* VEC4_Max(Vec4_t* result, const Vec4_t* a, const Vec4_t* b);

extern Vec4_t* VEC4_Invert(Vec4_t* result, const Vec4_t* v);

extern Vec4_t* VEC4_Cross(Vec4_t* result, const Vec4_t* a, const Vec4_t* b);

extern float VEC4_Dot(const Vec4_t* a, const Vec4_t* b);

extern float VEC4_Length(const Vec4_t* v);

extern Vec4_t* VEC4_Reflect(Vec4_t* result, const Vec4_t* v, const Vec4_t* n);

extern Vec4_t* VEC4_Lerp(Vec4_t* result, const Vec4_t* a, const Vec4_t* b, float t);

extern void VEC4_Print(const Vec4_t* vec, const char* message);

extern boolean VEC4_Equals(const Vec4_t* a, const Vec4_t* b);

#endif /* VEC4_H */
