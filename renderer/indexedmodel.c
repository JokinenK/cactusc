/*
 * indexedobj.h
 *
 *  Created on: 14.3.2016
 *      Author: Kalle Jokinen
 */

#include "indexedmodel.h"

#include <stdio.h>
#include <stdlib.h>
#include "model.h"
#include "common/vector.h"
#include "common/defvectors.h"
#include "types/triangle.h"
#include "types/vertex.h"
#include "obj/objparser.h"

/*-----------------------------------------------
 * PRIVATE METHOD DECLARATIONS
 *-----------------------------------------------
 */

static INDEXEDModel_t* CopyVectorsToResult(
        VECTOR(uint32_t)* indexes,
        VECTOR(Vec3_t)* positions,
        VECTOR(Vec2_t)* texCoords,
        VECTOR(Vec3_t)* normals);

static boolean FindSimilarVertex(
        VECTOR(Vertex_t)* lookupTable,
        Vertex_t* vertex,
        uint32_t* index);

static boolean VertexCompare(
        const Vertex_t* a,
        const Vertex_t* b);

/*-----------------------------------------------
 * PUBLIC METHOD DEFINITIONS
 *-----------------------------------------------
 */

INDEXEDModel_t* INDEXEDMODEL_ParseObjFile(const char* filepath)
{
    INDEXEDModel_t* result = NULL;
    Model_t* rawObject = OBJPARSER_Parse(filepath);

    if (rawObject != NULL) {
        result = INDEXEDMODEL_ParseModel(rawObject);
        OBJPARSER_Free(&rawObject);
    }

    return result;
}

INDEXEDModel_t* INDEXEDMODEL_ParseModel(const Model_t* model)
{
    uint32_t i = 0;
    uint32_t j = 0;
    uint32_t index = 0;
    INDEXEDModel_t* result = NULL;

    if (model != NULL && VECTOR_Size(model->triangles) > 0) {
        VECTOR_Create(Vertex_t, lookupTable);
        VECTOR_Create(uint32_t, indexes);
        VECTOR_Create(Vec3_t, positions);
        VECTOR_Create(Vec2_t, texCoords);
        VECTOR_Create(Vec3_t, normals);

        for (i = 0; i < VECTOR_Size(model->triangles); i++) {
            Triangle_t* triangle = VECTOR_At(model->triangles, i);

            for (j = 0; j < TRIANGLE_NUM_INDEXES; j++) {
                Vec3_t* position = VECTOR_At(
                        model->vertices,
                        triangle->position[j] - 1);

                Vec2_t* texCoord = VECTOR_At(
                        model->texCoords,
                        triangle->texCoord[j] - 1);

                Vec3_t* normal = VECTOR_At(
                        model->normals,
                        triangle->normal[j] - 1);

                Vertex_t vertex = {0};

                if (position != NULL) {
                    vertex.position = *position;
                }

                if (texCoord != NULL) {
                    vertex.texCoord = *texCoord;
                }

                if (normal != NULL) {
                    vertex.normal = *normal;
                }

                if (!FindSimilarVertex(&lookupTable, &vertex, &index)) {
                    VECTOR_Push(Vertex_t, lookupTable, vertex);
                    VECTOR_Push(Vec3_t, positions, vertex.position);
                    VECTOR_Push(Vec2_t, texCoords, vertex.texCoord);
                    VECTOR_Push(Vec3_t, normals, vertex.normal);
                }

                VECTOR_Push(uint32_t, indexes, index);
            }
        }

        result = CopyVectorsToResult(&indexes, &positions, &texCoords, &normals);

        VECTOR_Free(lookupTable);
        VECTOR_Free(indexes);
        VECTOR_Free(positions);
        VECTOR_Free(texCoords);
        VECTOR_Free(normals);
    }

    return result;
}

void INDEXEDMODEL_Free(INDEXEDModel_t** indexedModel)
{
    if ((*indexedModel) != NULL) {
        free((*indexedModel)->positions);
        free((*indexedModel)->texCoords);
        free((*indexedModel)->normals);
        free((*indexedModel));

        *indexedModel = NULL;
    }
}

/*-----------------------------------------------
 * PRIVATE METHOD DEFINITIONS
 *-----------------------------------------------
 */

static INDEXEDModel_t* CopyVectorsToResult(
        VECTOR(uint32_t)* indexes,
        VECTOR(Vec3_t)* positions,
        VECTOR(Vec2_t)* texCoords,
        VECTOR(Vec3_t)* normals)
{
    INDEXEDModel_t* result = NEW(INDEXEDModel_t);

    result->numIndexes = VECTOR_Size(*indexes);
    result->numPositions = VECTOR_Size(*positions);
    result->numTexCoords = VECTOR_Size(*texCoords);
    result->numNormals = VECTOR_Size(*normals);

    uint32_t sizeIndexes   = result->numIndexes * sizeof(uint32_t);
    uint32_t sizePositions = result->numPositions * sizeof(Vec3_t);
    uint32_t sizeTexCoords = result->numTexCoords * sizeof(Vec2_t);
    uint32_t sizeNormals   = result->numNormals * sizeof(Vec3_t);

    result->indexes = (uint32_t*)(malloc(sizeIndexes));
    result->positions = (Vec3_t*)(malloc(sizePositions));
    result->texCoords = (Vec2_t*)(malloc(sizeTexCoords));
    result->normals = (Vec3_t*)(malloc(sizeNormals));

    memcpy(result->indexes,   VECTOR_Data(*indexes),   sizeIndexes);
    memcpy(result->positions, VECTOR_Data(*positions), sizePositions);
    memcpy(result->texCoords, VECTOR_Data(*texCoords), sizeTexCoords);
    memcpy(result->normals,   VECTOR_Data(*normals),   sizeNormals);

    return result;
}

static boolean FindSimilarVertex(
        VECTOR(Vertex_t)* lookupTable,
        Vertex_t* vertex,
        uint32_t* index)
{
    uint32_t itemIndex = 0;
    Vertex_t* item = NULL;
    boolean similarFound = FALSE;

    VECTOR_ForEach(*lookupTable, item) {
        if (VertexCompare(vertex, item)) {
            similarFound = TRUE;
            break;
        }

        itemIndex++;
    }

    *index = itemIndex;
    return similarFound;
}

static boolean VertexCompare(
        const Vertex_t* a,
        const Vertex_t* b)
{
    return memcmp(a, b, sizeof(Vertex_t)) == 0;
}
