/*
 * indexedmodel.h
 *
 *  Created on: 14.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef INDEXEDMODEL_H
#define INDEXEDMODEL_H

#include "math/vec2.h"
#include "math/vec3.h"

typedef struct Model_s Model_t;

typedef struct INDEXEDModel_s {
    uint32_t numIndexes;
    uint32_t* indexes;

    uint32_t numPositions;
    Vec3_t* positions;

    uint32_t numTexCoords;
    Vec2_t* texCoords;

    uint32_t numNormals;
    Vec3_t* normals;
} INDEXEDModel_t;

extern INDEXEDModel_t* INDEXEDMODEL_ParseObjFile(const char* filepath);

extern INDEXEDModel_t* INDEXEDMODEL_ParseModel(const Model_t* model);

extern void INDEXEDMODEL_Free(INDEXEDModel_t** indexedModel);

#endif /* INDEXEDMODEL_H */
