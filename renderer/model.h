#ifndef MODEL_H
#define MODEL_H

#include <stdlib.h>
#include "common/types.h"
#include "common/vector.h"
#include "common/defvectors.h"
#include "math/vec2.h"
#include "math/vec3.h"
#include "types/triangle.h"

typedef struct Model_s {
    VECTOR(Triangle_t) triangles;
    VECTOR(Vec3_t) vertices;
    VECTOR(Vec2_t) texCoords;
    VECTOR(Vec3_t) normals;
} Model_t;

static inline void MODEL_Free(Model_t** model)
{
    if ((*model) != NULL) {
        VECTOR_Free((*model)->triangles);
        VECTOR_Free((*model)->vertices);
        VECTOR_Free((*model)->texCoords);
        VECTOR_Free((*model)->normals);
        free((*model));

        *model = NULL;
    }
}

#endif /* MODEL_H */
