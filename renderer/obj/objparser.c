/*
 * fileio.c
 *
 *  Created on: 12.3.2016
 *      Author: Kalle Jokinen
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "objparser.h"
#include "common/linkedlist.h"
#include "math/vec2.h"
#include "math/vec3.h"

/*-----------------------------------------------
 * MACRO DEFINITIONS
 *-----------------------------------------------
 */

#define READ_BUFFER_SIZE                (0x7F)

#define PREFIX_VERTICE                  ("v")
#define FORMAT_VERTICE                  ("%f %f %f\n")
#define VALUES_VERTICE                  (3)

#define PREFIX_TEXCOORD                 ("vt")
#define FORMAT_TEXCOORD                 ("%f %f\n")
#define VALUES_TEXCOORD                 (2)

#define PREFIX_NORMAL                   ("vn")
#define FORMAT_NORMAL                   ("%f %f %f\n")
#define VALUES_NORMAL                   (3)

#define PREFIX_FACE                     ("f")
#define TOKEN_FACE_SPLIT                (" ")
#define FORMAT_FACE_ALL                 ("%d/%d/%d")
#define VALUES_FACE_ALL                 (3)
#define FORMAT_FACE_WITH_NORMAL         ("%d//%d")
#define VALUES_FACE_WITH_NORMAL         (2)
#define FORMAT_FACE_WITH_TEXCOORD       ("%d/%d")
#define VALUES_FACE_WITH_TEXCOORD       (2)

/*-----------------------------------------------
 * PRIVATE TYPE DEFINITIONS
 *-----------------------------------------------
 */

typedef struct Face_s {
    uint32_t positionIndex;
    uint32_t texCoordIndex;
    uint32_t normalIndex;
} Face_t;

typedef boolean (*LineHandler)(
        FILE* handle,
        Model_t* objData);

typedef boolean (*FaceHandler)(
        const char* token,
        Face_t* face);

/*-----------------------------------------------
 * PRIVATE METHOD DECLARATIONS
 *-----------------------------------------------
 */

static void ParseData(
        FILE* handle,
        Model_t* objectData);

static boolean ParseVertice(
        FILE* handle,
        Model_t* objData);

static boolean ParseTexCoord(
        FILE* handle,
        Model_t* objData);

static boolean ParseNormal(
        FILE* handle,
        Model_t* objData);

static boolean ParseFace(
        FILE* handle,
        Model_t* objData);

static boolean ParseFaceCustom(
        const char* line,
        FaceHandler handler,
        Model_t* objData);

static boolean FaceHandlerComplete(const char* token, Face_t* face);

static boolean FaceHandlerTexCoord(const char* token, Face_t* face);

static boolean FaceHandlerNormal(const char* token, Face_t* face);

static uint32_t CalcTokens(const char* line, const char* separator);

static void PushTriangle(
        Model_t* objData,
        const Face_t* face,
        uint32_t i0,
        uint32_t i1,
        uint32_t i2);

static void CalculateNormals(Model_t* objData);

/*-----------------------------------------------
 * PRIVATE VARIABLE DEFINITIONS
 *-----------------------------------------------
 */

static const struct {
    LineHandler lineHandler;
    const char* typeMatcher;
} f_lineParsers[] = {
        {
                .lineHandler = ParseVertice,
                .typeMatcher = PREFIX_VERTICE
        },
        {
                .lineHandler = ParseTexCoord,
                .typeMatcher = PREFIX_TEXCOORD
        },
        {
                .lineHandler = ParseNormal,
                .typeMatcher = PREFIX_NORMAL
        },
        {
                .lineHandler = ParseFace,
                .typeMatcher = PREFIX_FACE
        }
};

static char f_readBuffer[128] = {0};

/*-----------------------------------------------
 * PUBLIC METHOD DEFINITIONS
 *-----------------------------------------------
 */

Model_t* OBJPARSER_Parse(const char* filePath)
{
    FILE* handle = fopen(filePath, "r");
    Model_t* objectData = NULL;

    if (handle != NULL) {
        objectData = NEW(Model_t);

        VECTOR_Init(Triangle_t, objectData->triangles);
        VECTOR_Init(Vec3_t, objectData->vertices);
        VECTOR_Init(Vec2_t, objectData->texCoords);
        VECTOR_Init(Vec3_t, objectData->normals);

        ParseData(handle, objectData);

        if (VECTOR_Size(objectData->normals) == 0) {
            CalculateNormals(objectData);
        }

        fclose(handle);
    }
    else {
        printf("Unable to parse obj: %s\n", filePath);
    }

    return objectData;
}

void OBJPARSER_Free(Model_t** model)
{
    MODEL_Free(model);
}

/*-----------------------------------------------
 * PRIVATE METHOD DEFINITIONS
 *-----------------------------------------------
 */

static void ParseData(
        FILE* handle,
        Model_t* objectData)
{
    uint16_t i = 0;
    int32_t res = 0;
    boolean lineHandled = FALSE;
    uint16_t numParsers = ARRAY_SIZE(f_lineParsers);

    rewind(handle);

    while (TRUE) {
        lineHandled = FALSE;

        res = fscanf(handle, "%s", f_readBuffer);
        if (res == EOF) {
            break;
        }

        for (i = 0; i < numParsers && !lineHandled; i++) {
            if (strcmp(f_readBuffer, f_lineParsers[i].typeMatcher) == 0) {
                f_lineParsers[i].lineHandler(handle, objectData);
                lineHandled = TRUE;
            }
        }

        if (!lineHandled) {
            fgets(f_readBuffer, READ_BUFFER_SIZE, handle);
        }
    }
}

static boolean ParseVertice(
        FILE* handle,
        Model_t* objData)
{
    boolean success = FALSE;
    Vec3_t vertice;

    uint32_t matches = fscanf(
            handle,
            FORMAT_VERTICE,
            &vertice.x,
            &vertice.y,
            &vertice.z);

    if (matches == VALUES_VERTICE) {
        VECTOR_Push(Vec3_t, objData->vertices, vertice);
        success = TRUE;
    }

    return success;
}

static boolean ParseTexCoord(
        FILE* handle,
        Model_t* objData)
{
    boolean success = FALSE;
    Vec2_t texCoord;

    uint32_t matches = fscanf(
            handle,
            FORMAT_TEXCOORD,
            &texCoord.x,
            &texCoord.y);

    if (matches == VALUES_TEXCOORD) {
        VECTOR_Push(Vec2_t, objData->texCoords, texCoord);
        success = TRUE;
    }

    return success;
}

static boolean ParseNormal(
        FILE* handle,
        Model_t* objData)
{
    boolean success = FALSE;
    Vec3_t normal;

    uint32_t matches = fscanf(
            handle,
            FORMAT_NORMAL,
            &normal.x,
            &normal.y,
            &normal.z);

    if (matches == VALUES_NORMAL) {
        VECTOR_Push(Vec3_t, objData->normals, normal);
        success = TRUE;
    }

    return success;
}

static boolean ParseFace(
        FILE* handle,
        Model_t* objData)
{
    boolean success = TRUE;

    fgets(f_readBuffer, READ_BUFFER_SIZE, handle);

    if (!ParseFaceCustom(&f_readBuffer[0], FaceHandlerComplete, objData)
            && !ParseFaceCustom(&f_readBuffer[0], FaceHandlerTexCoord, objData)
            && !ParseFaceCustom(&f_readBuffer[0], FaceHandlerNormal, objData)) {
        /* Unable to match any combinations */
        success = FALSE;
    }

    return success;
}

static uint32_t CalcTokens(const char* line, const char* separator)
{
    uint32_t numTokens = 0;
    char* copyOfLine = strdup(line);
    char* token = strtok(copyOfLine, separator);

    while (token) {
        numTokens++;
        token = strtok(NULL, separator);
    }

    free(copyOfLine);
    return numTokens;
}

static void PushTriangle(
        Model_t* objData,
        const Face_t* faces,
        uint32_t i0,
        uint32_t i1,
        uint32_t i2)
{
    const Face_t* faceOne = faces + i0;
    const Face_t* faceTwo = faces + i1;
    const Face_t* faceThree = faces + i2;

    Triangle_t triangle = {
            .position = {
                    faceOne->positionIndex,
                    faceTwo->positionIndex,
                    faceThree->positionIndex
            },
            .texCoord = {
                    faceOne->texCoordIndex,
                    faceTwo->texCoordIndex,
                    faceThree->texCoordIndex
            },
            .normal = {
                    faceOne->normalIndex,
                    faceTwo->normalIndex,
                    faceThree->normalIndex
            }
    };

    VECTOR_Push(Triangle_t, objData->triangles, triangle);
}

static boolean ParseFaceCustom(
        const char* line,
        FaceHandler handler,
        Model_t* objData)
{
    boolean success = TRUE;

    uint16_t faceIndex = 0;
    uint16_t numFaces = CalcTokens(line, TOKEN_FACE_SPLIT);
    Face_t faces[numFaces];

    char* copyOfLine = strdup(line);
    char* token = strtok(copyOfLine, TOKEN_FACE_SPLIT);

    while (token) {
        if (!handler(token, &faces[faceIndex])) {
            success = FALSE;
            break;
        }

        token = strtok(NULL, TOKEN_FACE_SPLIT);
        faceIndex++;
    }

    if (success) {
        PushTriangle(objData, &faces[0], 0, 1, 2);

        if (numFaces > 3) {
            PushTriangle(objData, &faces[0], 0, 2, 3);
        }
    }

    free(copyOfLine);
    return success;
}

static boolean FaceHandlerComplete(
        const char* token,
        Face_t* face)
{
    boolean success = FALSE;

    uint32_t matches = sscanf(
            token,
            FORMAT_FACE_ALL,
            &face->positionIndex,
            &face->texCoordIndex,
            &face->normalIndex);

    if (matches == VALUES_FACE_ALL) {
        success = TRUE;
    }

    return success;
}

static boolean FaceHandlerTexCoord(
        const char* token,
        Face_t* face)
{
    boolean success = FALSE;

    uint32_t matches = sscanf(
            token,
            FORMAT_FACE_WITH_TEXCOORD,
            &face->positionIndex,
            &face->texCoordIndex);

    face->normalIndex = 0;

    if (matches == VALUES_FACE_WITH_TEXCOORD) {
        success = TRUE;
    }

    return success;
}

static boolean FaceHandlerNormal(
        const char* token,
        Face_t* face)
{
    boolean success = FALSE;

    uint32_t matches = sscanf(
            token,
            FORMAT_FACE_WITH_NORMAL,
            &face->positionIndex,
            &face->normalIndex);

    face->texCoordIndex = 0;

    if (matches == VALUES_FACE_WITH_NORMAL) {
        success = TRUE;
    }

    return success;
}

static void CalculateNormals(Model_t* objData)
{
    printf("Calculating normals from %d vertices and %d faces...\n",
            VECTOR_Size(objData->vertices),
            VECTOR_Size(objData->triangles));

    uint32_t i = 0;

    for (i = 0; i < VECTOR_Size(objData->vertices); i++) {
        Vec3_t normal;
        VEC3_Init(&normal, 0.0f, 0.0f, 0.0f);
        VECTOR_Push(Vec3_t, objData->normals, normal);
    }

    Triangle_t* triangle = NULL;
    VECTOR_ForEach(objData->triangles, triangle)
    {
        uint32_t i0 = triangle->position[0] - 1;
        uint32_t i1 = triangle->position[1] - 1;
        uint32_t i2 = triangle->position[2] - 1;

        Vec3_t* v0 = VECTOR_At(objData->vertices, i0);
        Vec3_t* v1 = VECTOR_At(objData->vertices, i1);
        Vec3_t* v2 = VECTOR_At(objData->vertices, i2);

        Vec3_t a;
        VEC3_Substract(&a, v1, v0);

        Vec3_t b;
        VEC3_Substract(&b, v2, v0);

        Vec3_t result;
        VEC3_Cross(&result, &a, &b);

        VEC3_Add(
                VECTOR_At(objData->normals, i0),
                VECTOR_At(objData->normals, i0),
                &result);

        VEC3_Add(
                VECTOR_At(objData->normals, i1),
                VECTOR_At(objData->normals, i1),
                &result);

        VEC3_Add(
                VECTOR_At(objData->normals, i2),
                VECTOR_At(objData->normals, i2),
                &result);
    }

    for (i = 0; i < VECTOR_Size(objData->vertices); i++) {
        VEC3_Normalize(
                VECTOR_At(objData->normals, i),
                VECTOR_At(objData->normals, i));
    }
}
