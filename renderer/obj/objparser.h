/*
 * fileio.h
 *
 *  Created on: 12.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef OBJPARSER_H
#define OBJPARSER_H

#include "renderer/model.h"

/*-----------------------------------------------
 * PUBLIC TYPE DEFINITIONS
 *-----------------------------------------------
 */

extern Model_t* OBJPARSER_Parse(const char* filepath);

extern void OBJPARSER_Free(Model_t** obj);

#endif /* OBJPARSER_H */
