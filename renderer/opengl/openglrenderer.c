#include <stdio.h>

#include "engine/gameobject.h"
#include "renderer/opengl/openglrenderer.h"
#include "renderer/model.h"
#include "renderer/indexedmodel.h"
#include "common/types.h"
#include "common/linkedlist.h"
#include "math/vec2.h"
#include "math/vec3.h"

/*-----------------------------------------------
 * MACRO DEFINITIONS
 *-----------------------------------------------
 */

#define ITEM_OFFSET_POSITIONS 3
#define ITEM_OFFSET_TEXCOORDS 2
#define ITEM_OFFSET_NORMALS 3

/*-----------------------------------------------
 * PRIVATE TYPE DEFINITIONS
 *-----------------------------------------------
 */

typedef enum {
    VB_POSITION,
    VB_TEXCOORD,
    VB_NORMAL,
    VB_INDEX,
    NUM_BUFFERS
} VertexBufferType_t;

/*-----------------------------------------------
 * PRIVATE METHOD DECLARATIONS
 *-----------------------------------------------
 */

static Renderer_t* Init(
        Renderer_t* parent);

static RENDERER_Status_t Clear(
        const Renderer_t* parent);

static RENDERER_Status_t Render(
        Renderer_t* parent,
        GameObject_t* gameObject);

static RENDERER_Status_t RequestRender(
        const Renderer_t* parent,
        ModelHandle_t modelHandle);

static RENDERER_Status_t SetBlending(
        const Renderer_t* parent,
        RENDERER_Blending_t blending);

static RENDERER_Status_t SetClearColor(
        const Renderer_t* parent,
        Color_t clearColor);

static ModelHandle_t AddModel(
        Renderer_t* parent,
        const Model_t* model);

static boolean FreeModel(
        Renderer_t* parent,
        ModelHandle_t modelHandle);

static Light_t* GetAmbientLight(Renderer_t* parent);

static void BufferIndexedModel(
        OPENGLRENDERER_VertexBuffer_t* this,
        const INDEXEDModel_t* indexedModel);

/*-----------------------------------------------
 * PUBLIC METHOD IMPLEMENTATIONS
 *-----------------------------------------------
 */

Renderer_t* OPENGLRENDERER_Init(OPENGL_Renderer_t* this)
{
    Renderer_t* parent = NULL;

    if (this != NULL) {
        parent = &this->parent.renderer;

        parent->init = Init;
        parent->clear = Clear;
        parent->render = Render;
        parent->requestRender = RequestRender;
        parent->setBlending = SetBlending;
        parent->setClearColor = SetClearColor;
        parent->addModel = AddModel;
        parent->freeModel = FreeModel;
        parent->getAmbientLight = GetAmbientLight;
    }

    return parent;
}

/*-----------------------------------------------
 * PRIVATE METHOD IMPLEMENTATIONS
 *-----------------------------------------------
 */

static Renderer_t* Init(Renderer_t* parent)
{
    if (parent != NULL) {
        OPENGL_Renderer_t* instance = (OPENGL_Renderer_t*)parent;

        GLenum status = glewInit();

        if (status == GLEW_OK)
        {
            /* Enable depth */
            glEnable(GL_DEPTH_TEST);
            glEnable(GL_DEPTH_CLAMP);
            glEnable(GL_TEXTURE_2D);

            /* Enable culling */
            glCullFace(GL_BACK);
            glEnable(GL_CULL_FACE);

            /* Initialize vector to store model buffers */
            VECTOR_Init(OPENGLRENDERER_VertexBuffer_t, instance->bufferList);

            Color_t ambientColor;
            COLOR_SetRgb(&ambientColor, 200, 200, 200);

            LIGHT_Init(
                    &parent->ambientLight,
                    SHADERFORWARDAMBIENT_Init(&instance->defaultShader),
                    ambientColor,
                    1.0f);
        }
        else {
            printf("GLEW initialization failed: %s\n",
                    glewGetErrorString(status));
        }
    }

    return parent;
}

static RENDERER_Status_t Clear(const Renderer_t* parent)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    return RENDERER_STATUS_OK;
}

static RENDERER_Status_t Render(
        Renderer_t* parent,
        GameObject_t* gameObject)
{
    if (parent != NULL && gameObject != NULL) {
        OPENGL_Renderer_t* instance = (OPENGL_Renderer_t*)parent;

        GAMEOBJECT_Render(
                gameObject,
                parent,
                parent->ambientLight.shader,
                &parent->ambientLight);

        RENDERER_SetBlending(parent, RENDERER_BLENDING_ONE);

        Light_t* light = NULL;
        LINKEDLIST_ForEach(parent->listLights, light) {
            GAMEOBJECT_Render(gameObject, parent, light->shader, light);
        }

        RENDERER_SetBlending(parent, RENDERER_BLENDING_DISABLE);
    }

    return RENDERER_STATUS_OK;
}

static RENDERER_Status_t RequestRender(
        const Renderer_t* parent,
        ModelHandle_t modelHandle)
{
    if (parent != NULL) {
        OPENGL_Renderer_t* this = (OPENGL_Renderer_t*)parent;

        OPENGLRENDERER_VertexBuffer_t* vertexBuffer = VECTOR_At(
                this->bufferList,
                modelHandle);

        if (vertexBuffer != NULL) {
            glBindVertexArray(vertexBuffer->objectHandle);

            glDrawElements(
                    GL_TRIANGLES,
                    vertexBuffer->numIndices,
                    GL_UNSIGNED_INT,
                    0);

            glBindVertexArray(0);
        }
    }

    return RENDERER_STATUS_OK;
}

static RENDERER_Status_t SetBlending(
        const Renderer_t* renderer,
        RENDERER_Blending_t blending)
{
    RENDERER_Status_t status = RENDERER_STATUS_FAILURE;

    switch (blending) {
    case RENDERER_BLENDING_ONE:
        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE);
        glDepthMask(GL_FALSE);
        glDepthFunc(GL_EQUAL);
        status = RENDERER_STATUS_OK;
        break;

    case RENDERER_BLENDING_DISABLE:
        glDepthFunc(GL_LESS);
        glDepthMask(GL_TRUE);
        glDisable(GL_BLEND);
        status = RENDERER_STATUS_OK;
        break;
    }

    return status;
}

static RENDERER_Status_t SetClearColor(
        const Renderer_t* parent,
        Color_t clearColor)
{
    glClearColor(
            clearColor.red,
            clearColor.green,
            clearColor.blue,
            clearColor.alpha);

    return RENDERER_STATUS_OK;
}

static ModelHandle_t AddModel(
        Renderer_t* parent,
        const Model_t* model)
{
    uint32_t result = 0;

    if (parent != NULL) {
        OPENGL_Renderer_t* this = (OPENGL_Renderer_t*)parent;

        INDEXEDModel_t* indexedModel = INDEXEDMODEL_ParseModel(model);

        if (indexedModel != NULL) {
            OPENGLRENDERER_VertexBuffer_t vertexBuffer = {0};

            vertexBuffer.numIndices = indexedModel->numIndexes;
            vertexBuffer.buffers = (GLuint*)(calloc(
                    NUM_BUFFERS,
                    sizeof(GLuint)));

            /* Generate and bind arrays */
            glGenVertexArrays(1, &vertexBuffer.objectHandle);
            glBindVertexArray(vertexBuffer.objectHandle);

            /* Buffer data in to those ararys */
            BufferIndexedModel(&vertexBuffer, indexedModel);

            /* Release binding */
            glBindVertexArray(0);

            VECTOR_Push(
                    OPENGLRENDERER_VertexBuffer_t,
                    this->bufferList,
                    vertexBuffer);

            INDEXEDMODEL_Free(&indexedModel);

            /* Return next model handle */
            result = this->modelHandleIndex++;
        }
    }

    return result;
}

static boolean FreeModel(
        Renderer_t* parent,
        ModelHandle_t modelHandle)
{
    boolean success = FALSE;

    if (parent != NULL) {
        OPENGL_Renderer_t* this = (OPENGL_Renderer_t*)parent;

        OPENGLRENDERER_VertexBuffer_t* vertexBuffer = VECTOR_At(
                this->bufferList,
                modelHandle);

        if (vertexBuffer != NULL) {
            glDeleteBuffers(NUM_BUFFERS, vertexBuffer->buffers);
            glDeleteVertexArrays(1, &vertexBuffer->objectHandle);
            free(vertexBuffer->buffers);

            success = TRUE;
        }
    }

    return success;
}

static Light_t* GetAmbientLight(Renderer_t* parent)
{
    return &parent->ambientLight;
}

static void BufferIndexedModel(
        OPENGLRENDERER_VertexBuffer_t* vertexBuffer,
        const INDEXEDModel_t* indexedModel)
{
    glGenBuffers(NUM_BUFFERS, vertexBuffer->buffers);

    struct {
        VertexBufferType_t type;
        const void* data;
        uint32_t size;
        uint16_t offset;
    } bufferData[] = {
            {
                    /* Positions */
                    .type = VB_POSITION,
                    .data = indexedModel->positions,
                    .size = sizeof(Vec3_t) * indexedModel->numPositions,
                    .offset = ITEM_OFFSET_POSITIONS,
            },
            {
                    /* Texture coordinates */
                    .type = VB_TEXCOORD,
                    .data = indexedModel->texCoords,
                    .size = sizeof(Vec2_t) * indexedModel->numTexCoords,
                    .offset = ITEM_OFFSET_TEXCOORDS,
            },
            {
                    /* Normals */
                    .type = VB_NORMAL,
                    .data = indexedModel->normals,
                    .size = sizeof(Vec3_t) * indexedModel->numNormals,
                    .offset = ITEM_OFFSET_NORMALS,
            }
    };

    uint16_t i = 0;
    for (i = 0; i < ARRAY_SIZE(bufferData); i++) {
        glBindBuffer(
                GL_ARRAY_BUFFER,
                vertexBuffer->buffers[bufferData[i].type]);

        glBufferData(
                GL_ARRAY_BUFFER,
                bufferData[i].size,
                bufferData[i].data,
                GL_STATIC_DRAW);

        glEnableVertexAttribArray(bufferData[i].type);

        glVertexAttribPointer(
                bufferData[i].type,
                bufferData[i].offset,
                GL_FLOAT,
                GL_FALSE,
                0,
                0);
    }

    /* Indexes */
    glBindBuffer(
            GL_ELEMENT_ARRAY_BUFFER,
            vertexBuffer->buffers[VB_INDEX]);

    glBufferData(
            GL_ELEMENT_ARRAY_BUFFER,
            sizeof(uint32_t) * indexedModel->numIndexes,
            indexedModel->indexes,
            GL_STATIC_DRAW);
}
