#ifndef OPENGLRENDERER_H
#define OPENGLRENDERER_H

#include "renderer/renderer.h"
#include "shaders/opengl/forwardambient.h"
#include "common/vector.h"
#include "GL/glew.h"

typedef struct OPENGLRENDERER_VertexBuffer_s {
    GLuint objectHandle;
    GLuint* buffers;
    uint32_t numIndices;
} OPENGLRENDERER_VertexBuffer_t;

VECTOR_Declare(OPENGLRENDERER_VertexBuffer_t);

typedef struct OPENGL_Renderer_s {
    union {
        Renderer_t renderer;
    } parent;

    ShaderForwardAmbient_t defaultShader;
    ModelHandle_t modelHandleIndex;
    VECTOR(OPENGLRENDERER_VertexBuffer_t) bufferList;
} OPENGL_Renderer_t;

extern Renderer_t* OPENGLRENDERER_Init(OPENGL_Renderer_t* this);

#endif /* OPENGLRENDERER_H */
