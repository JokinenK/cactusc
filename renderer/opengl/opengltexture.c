#include "opengltexture.h"
#include "stb_image.h"

/*-----------------------------------------------
 * PRIVATE METHOD DECLARATIONS
 *-----------------------------------------------
 */

static Texture_t* Init(Texture_t* parent, const char* filePath);

static void Free(Texture_t* parent);

static void Bind(const Texture_t* parent, uint32_t textureUnit);

/*-----------------------------------------------
 * PUBLIC METHOD IMPLEMENTATIONS
 *-----------------------------------------------
 */

Texture_t* OPENGLTEXTURE_Init(OPENGL_Texture_t* instance)
{
    Texture_t* parent = NULL;

    if (instance != NULL) {
        parent = &instance->parent.texture;

        parent->init = Init;
        parent->bind = Bind;
        parent->free = Free;
    }

    return parent;
}

/*-----------------------------------------------
 * PRIVATE METHOD IMPLEMENTATIONS
 *-----------------------------------------------
 */

Texture_t* Init(Texture_t* parent, const char* filePath)
{
    if (parent != NULL && filePath != NULL) {
        OPENGL_Texture_t* instance = (OPENGL_Texture_t*) parent;

        int width, height, numComponents;

        /* Read image with stbi_load */
        unsigned char* data = stbi_load(
                filePath,
                &width,
                &height,
                &numComponents,
                4);

        if (data != NULL)
        {
            glBindTexture(GL_TEXTURE_2D, 0);
            glGenTextures(1, &instance->handle);
            glBindTexture(GL_TEXTURE_2D, instance->handle);

            /* Wrap around */
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            glTexImage2D(
                    GL_TEXTURE_2D,
                    0,
                    GL_RGBA,
                    width,
                    height,
                    0,
                    GL_RGBA,
                    GL_UNSIGNED_BYTE,
                    data);

            stbi_image_free(data);
        }
        else
        {
            printf("Unable to load texture: %s\n", filePath);
        }
    }

    return parent;
}

static void Free(Texture_t* parent)
{
    if (parent != NULL) {
        OPENGL_Texture_t* instance = (OPENGL_Texture_t*) parent;

        glDeleteTextures(1, &instance->handle);
    }
}

static void Bind(const Texture_t* parent, uint32_t textureUnit)
{
    if (parent != NULL && textureUnit >= 0 && textureUnit <= 31) {
        OPENGL_Texture_t* instance = (OPENGL_Texture_t*) parent;

        glActiveTexture(GL_TEXTURE0 + textureUnit);
        glBindTexture(GL_TEXTURE_2D, instance->handle);
    }
}
