#ifndef OPENGLTEXTURE_H
#define OPENGLTEXTURE_H

#include "renderer/texture.h"
#include "GL/glew.h"

typedef struct OPENGL_Texture_s {
    union {
        Texture_t texture;
    } parent;

    GLuint handle;
} OPENGL_Texture_t;

extern Texture_t* OPENGLTEXTURE_Init(OPENGL_Texture_t* this);

#endif /* OPENGLTEXTURE_H */
