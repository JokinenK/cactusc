#ifndef RENDERER_H
#define RENDERER_H

#include "types/color.h"
#include "engine/components/light/light.h"

typedef struct GameObject_s GameObject_t;
typedef struct Shader_s Shader_t;
typedef struct Model_s Model_t;

/*-----------------------------------------------
 * PUBLIC TYPE DEFINITIONS
 *-----------------------------------------------
 */

typedef enum {
    RENDERER_BLENDING_ONE,
    RENDERER_BLENDING_DISABLE,
} RENDERER_Blending_t;

typedef enum {
    RENDERER_STATUS_OK,
    RENDERER_STATUS_FAILURE
} RENDERER_Status_t;

typedef uint32_t ModelHandle_t;

typedef struct Renderer_s {
    struct Renderer_s* (*init)(
            struct Renderer_s* this);

    RENDERER_Status_t (*render)(
            struct Renderer_s* this,
            GameObject_t* rootObject);

    RENDERER_Status_t (*requestRender)(
            const struct Renderer_s* this,
            ModelHandle_t modelHandle);

    RENDERER_Status_t (*clear)(
            const struct Renderer_s* this);

    RENDERER_Status_t (*setClearColor)(
            const struct Renderer_s* this,
            Color_t clearColor);

    RENDERER_Status_t (*setBlending)(
            const struct Renderer_s* this,
            RENDERER_Blending_t blending);

    ModelHandle_t (*addModel)(
            struct Renderer_s* this,
            const Model_t* model);

    boolean (*freeModel)(
            struct Renderer_s* this,
            ModelHandle_t modelHandle);

    Light_t* (*getAmbientLight)(struct Renderer_s* this);

    Light_t ambientLight;
    Light_t* listLights;
    Shader_t* defaultShader;
} Renderer_t;

/*-----------------------------------------------
 * PUBLIC METHOD IMPLEMENTATIONS
 *-----------------------------------------------
 */

#define RENDERER_Init(this) \
    this->init(this);

#define RENDERER_Render(this, gameObject) \
    this->render(this, gameObject)

#define RENDERER_RequestRender(this, modelHandle) \
    this->requestRender(this, modelHandle)

#define RENDERER_Clear(this) \
    this->clear(this)

#define RENDERER_SetClearColor(this, color) \
    this->setClearColor(this, color)

#define RENDERER_SetBlending(this, blending) \
    this->setBlending(this, blending)

#define RENDERER_AddModel(this, model) \
    this->addModel(this, model)

#define RENDERER_FreeModel(this, modelHandle) \
    this->freeModel(this, modelHandle)

#endif /* RENDERER_H */
