#ifndef TEXTURE_H
#define TEXTURE_H

#include "common/types.h"

typedef struct Texture_s {
    struct Texture_s* (*init)(struct Texture_s* texture, const char* filePath);

    void (*free)(struct Texture_s* texture);

    void (*bind)(const struct Texture_s* texture, uint32_t textureUnit);

    struct Texture_s* next;
}Texture_t;

#define TEXTURE_Init(texture, filePath) \
    texture->init(texture, filePath)

#define TEXTURE_Free(texture) \
    texture->free(texture)

#define TEXTURE_Bind(texture, textureUnit) \
    texture->bind(texture, textureUnit)

#endif /* TEXTURE_H */
