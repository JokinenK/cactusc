/*
 * forwardambient.c
 *
 *  Created on: 18.3.2016
 *      Author: Kalle Jokinen
 */

#include "forwardambient.h"
#include "math/mat4.h"
#include "engine/components/camera.h"

/*-----------------------------------------------
 * PRIVATE METHOD DECLARATIONS
 *-----------------------------------------------
 */

static void Update(
        Shader_t* parent,
        Camera_t* camera,
        Light_t* light,
        Transform_t* transform,
        Material_t* material);

/*-----------------------------------------------
 * PUBLIC METHOD DEFINITIONS
 *-----------------------------------------------
 */

Shader_t* SHADERFORWARDAMBIENT_Init(ShaderForwardAmbient_t* instance)
{
    OPENGL_Shader_t* parent = NULL;
    Shader_t* grandParent = NULL;

    if (instance != NULL) {
        parent = (OPENGL_Shader_t*)&instance->parent.openglShader;
        grandParent = OPENGLSHADER_Init(parent);

        grandParent->update = Update;

        OPENGLSHADER_AddVertexShader(parent, "forward-ambient.vs");
        OPENGLSHADER_AddFragmentShader(parent, "forward-ambient.fs");

        SHADER_BindAttributeLocation(grandParent, "attrPosition", 0);
        SHADER_BindAttributeLocation(grandParent, "attrTexCoords", 1);

        OPENGLSHADER_CompileShaders(parent);

        SHADER_Bind(grandParent);

        UNIFORM_Init(
                &instance->uniformModelViewPosition,
                grandParent,
                "uniformModelViewPosition");

        UNIFORMLIGHT_Init(
                &instance->uniformAmbientLight,
                grandParent,
                "uniformAmbientLight");
    }

    return grandParent;
}

/*-----------------------------------------------
 * PRIVATE METHOD DEFINITIONS
 *-----------------------------------------------
 */

static void Update(
        Shader_t* parent,
        Camera_t* camera,
        Light_t* light,
        Transform_t* transform,
        Material_t* material)
{
    if (parent != NULL) {
        ShaderForwardAmbient_t* instance = (ShaderForwardAmbient_t*)parent;

        Mat4_t modelViewPosition = {0};
        CAMERA_GetModelViewProjection(camera, transform, &modelViewPosition);

        UNIFORM_SetMat4(
                &instance->uniformModelViewPosition,
                &modelViewPosition);

        UNIFORMLIGHT_SetLight(&instance->uniformAmbientLight, light);
    }
}
