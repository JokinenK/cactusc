/*
 * forwardambient.h
 *
 *  Created on: 18.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef FORWARDAMBIENT_H
#define FORWARDAMBIENT_H

#include "openglshader.h"
#include "shaders/uniform/uniform.h"
#include "shaders/uniform/uniformlight.h"
#include "engine/components/light/light.h"

typedef struct Light_s Light_t;

typedef struct ShaderForwardAmbient_s {
    union {
        Shader_t shader;
        OPENGL_Shader_t openglShader;
    } parent;

    Uniform_t uniformModelViewPosition;
    UniformLight_t uniformAmbientLight;
} ShaderForwardAmbient_t;

extern Shader_t* SHADERFORWARDAMBIENT_Init(ShaderForwardAmbient_t* this);

#endif /* FORWARDAMBIENT_H */
