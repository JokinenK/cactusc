/*
 * openglbaseshader.c
 *
 *  Created on: 17.3.2016
 *      Author: Kalle Jokinen
 */

#include <stdio.h>
#include "openglshader.h"
#include "core/fileio.h"
#include "common/linkedlist.h"
#include "engine/transform.h"
#include "engine/components/camera.h"
#include "engine/components/material.h"

/*-----------------------------------------------
 * PRIVATE METHOD DECLARATIONS
 *-----------------------------------------------
 */

static Shader_t* Init(Shader_t* parent);

static void Free(Shader_t* parent);

static void Bind(Shader_t* parent);

static void AddShaderSource(
        OPENGL_Shader_t* this,
        const char* filePath,
        GLuint shaderType);

static boolean ValidateShader(
        GLuint shaderProgram,
        GLuint parameterType,
        boolean isProgram,
        const char* errorMessage);

static void BindAttributeLocation(
        Shader_t* parent,
        const char* attribName,
        uint32_t location);

static UniformHandle_t AddUniform(
        Shader_t* parent,
        const char* uniformName);

static void SetUniform1i(
        Shader_t* parent,
        UniformHandle_t uniformHandle,
        int32_t value);

static void SetUniform1f(
        Shader_t* parent,
        UniformHandle_t uniformHandle,
        float value);

static void SetUniform3f(
        Shader_t* parent,
        UniformHandle_t uniformHandle,
        float a,
        float b,
        float c);

static void SetUniform4f(
        Shader_t* parent,
        UniformHandle_t uniformHandle,
        float a,
        float b,
        float c,
        float d);

static void SetUniformMatrix4f(
        Shader_t* parent,
        UniformHandle_t uniformHandle,
        const float* value);

/*-----------------------------------------------
 * PUBLIC METHOD DEFINITIONS
 *-----------------------------------------------
 */

Shader_t* OPENGLSHADER_Init(OPENGL_Shader_t* this)
{
    Shader_t* parent = NULL;

    if (this != NULL) {
        parent = &this->parent.shader;
        memset(parent, 0, sizeof(Shader_t));

        parent->init = Init;
        parent->free = Free;
        parent->bind = Bind;

        parent->bindAttributeLocation = BindAttributeLocation;
        parent->addUniform = AddUniform;
        parent->setUniform1i = SetUniform1i;
        parent->setUniform1f = SetUniform1f;
        parent->setUniform3f = SetUniform3f;
        parent->setUniform4f = SetUniform4f;
        parent->setUniformMatrix4f = SetUniformMatrix4f;

        /* Child classes must setup the update method */
        parent->update = NULL;
    }

    return parent;
}

void OPENGLSHADER_AddVertexShader(OPENGL_Shader_t* this, const char* filePath)
{
    AddShaderSource(this, filePath, GL_VERTEX_SHADER);
}

void OPENGLSHADER_AddGeometryShader(OPENGL_Shader_t* this, const char* filePath)
{
    AddShaderSource(this, filePath, GL_GEOMETRY_SHADER);
}

void OPENGLSHADER_AddFragmentShader(OPENGL_Shader_t* this, const char* filePath)
{
    AddShaderSource(this, filePath, GL_FRAGMENT_SHADER);
}

boolean OPENGLSHADER_CompileShaders(OPENGL_Shader_t* this)
{
    boolean success = FALSE;

    if (this != NULL) {
        success = TRUE;
        this->program = glCreateProgram();

        OPENGLSHADER_ShaderSource_t* node;
        LINKEDLIST_ForEach(this->listShaderSources, node) {
            uint32_t sourceLength = 0;
            FILEIO_Status_t readStatus;

            char* shaderSource = FILEIO_ReadFile(
                    node->shaderFile,
                    &sourceLength,
                    &readStatus);

            if (readStatus == FILEIO_OK) {
                node->shaderProgram = OPENGLSHADER_CreateShader(
                        this,
                        shaderSource,
                        sourceLength,
                        node->shaderType);
                glAttachShader(this->program, node->shaderProgram);

                free(shaderSource);
            }
        }

        /* Link program */
        glLinkProgram(this->program);

        success = ValidateShader(
                this->program,
                GL_LINK_STATUS,
                TRUE,
                "Error linking shader program") && success;

        /* Validate program */
        glValidateProgram(this->program);

        success = ValidateShader(
                this->program,
                GL_LINK_STATUS,
                TRUE,
                "Invalid shader program") && success;
    }

    return success;
}

GLuint OPENGLSHADER_CreateShader(
        OPENGL_Shader_t* this,
        const char* sourceCode,
        uint32_t sourceLength,
        GLuint shaderType)
{
    GLuint shader = glCreateShader(shaderType);

    if (shader != 0)
    {
        const GLchar* sourcePointers[] = { sourceCode };
        GLint sourceLengths[] = { sourceLength };

        glShaderSource(shader, 1, sourcePointers, sourceLengths);
        glCompileShader(shader);

        // Check status
        ValidateShader(
                shader,
                GL_COMPILE_STATUS,
                FALSE,
                "Error compiling shader");
    }
    else {
        printf("Error compiling shader with type: %d\n", shaderType);
    }

    return shader;
}

/*-----------------------------------------------
 * PRIVATE METHOD DEFINITIONS
 *-----------------------------------------------
 */

static Shader_t* Init(Shader_t* parent)
{
    if (parent != NULL) {
            OPENGL_Shader_t* this = (OPENGL_Shader_t*)parent;

            this->listShaderSources = NULL;
    }

    return parent;
}

static void Free(Shader_t* parent)
{
    if (parent != NULL) {
        OPENGL_Shader_t* this = (OPENGL_Shader_t*)parent;

        OPENGLSHADER_ShaderSource_t* list = this->listShaderSources;
        OPENGLSHADER_ShaderSource_t* current = NULL;

        /**
         * Cannot use basic foreach here, must
         * read next before freeing previous
         */
        while (list != NULL) {
            current = list;
            list = list->next;

            glDetachShader(this->program, current->shaderProgram);
            glDeleteShader(current->shaderProgram);

            free(current);
        }

        this->listShaderSources = NULL;
        glDeleteProgram(this->program);
    }
}

static void Bind(Shader_t* parent)
{
    if (parent != NULL) {
        OPENGL_Shader_t* this = (OPENGL_Shader_t*)parent;

        glUseProgram(this->program);
    }
}

static void AddShaderSource(
        OPENGL_Shader_t* this,
        const char* filePath,
        GLuint shaderType)
{
    OPENGLSHADER_ShaderSource_t* source = NEW(OPENGLSHADER_ShaderSource_t);

    source->shaderFile = filePath;
    source->shaderType = shaderType;

    LINKEDLIST_Prepend(this->listShaderSources, source);
}

static boolean ValidateShader(
        GLuint shaderProgram,
        GLuint parameterType,
        boolean isProgram,
        const char* errorMessage)
{
    boolean success = TRUE;
    GLint shaderSuccess = 0;
    GLchar shaderError[1024] = { 0 };

    if (isProgram) {
        /* Check against program */
        glGetProgramiv(shaderProgram, parameterType, &shaderSuccess);
    }
    else {
        /* Check against shader */
        glGetShaderiv(shaderProgram, parameterType, &shaderSuccess);
    }

    if (shaderSuccess == GL_FALSE) {
        if (isProgram) {
            /* Read error message from program */
            glGetProgramInfoLog(
                    shaderProgram,
                    sizeof(shaderError),
                    NULL,
                    shaderError);
        }
        else {
            /* Read error message from shader */
            glGetShaderInfoLog(
                    shaderProgram,
                    sizeof(shaderError),
                    NULL,
                    shaderError);
        }

        printf("%s: '%s'\n", errorMessage, shaderError);
        success = FALSE;
    }

    return success;
}

static void BindAttributeLocation(
        Shader_t* parent,
        const char* attribName,
        uint32_t location)
{
    if (parent != NULL) {
        OPENGL_Shader_t* this = (OPENGL_Shader_t*)parent;
        glBindAttribLocation(this->program, location, attribName);
    }
}

static UniformHandle_t AddUniform(
        Shader_t* parent,
        const char* uniformName)
{
    UniformHandle_t handle = SHADER_INVALID_UNIFORM_HANDLE;

    if (parent != NULL) {
        OPENGL_Shader_t* this = (OPENGL_Shader_t*)parent;
        handle = glGetUniformLocation(this->program, uniformName);
    }

    return handle;
}

static void SetUniform1i(
        Shader_t* parent,
        UniformHandle_t uniformHandle,
        int32_t value)
{
    glUniform1i(uniformHandle, value);
}

static void SetUniform1f(
        Shader_t* parent,
        UniformHandle_t uniformHandle,
        float value)
{
    glUniform1f(uniformHandle, value);
}

static void SetUniform3f(
        Shader_t* parent,
        UniformHandle_t uniformHandle,
        float a,
        float b,
        float c)
{
    glUniform3f(uniformHandle, a, b, c);
}

static void SetUniform4f(
        Shader_t* parent,
        UniformHandle_t uniformHandle,
        float a,
        float b,
        float c,
        float d)
{
    glUniform4f(uniformHandle, a, b, c, d);
}

static void SetUniformMatrix4f(
        Shader_t* parent,
        UniformHandle_t uniformHandle,
        const float* value)
{
    glUniformMatrix4fv(uniformHandle, 1, GL_FALSE, value);
}
