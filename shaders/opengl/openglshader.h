/*
 * openglbaseshader.h
 *
 *  Created on: 17.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef OPENGLSHADER_H
#define OPENGLSHADER_H

#include "shaders/shader.h"
#include "common/types.h"
#include "GL/glew.h"

typedef struct OPENGLSHADER_ShaderSource_s {
    GLuint shaderType;
    const char* shaderFile;
    GLuint shaderProgram;
    struct OPENGLSHADER_ShaderSource_s* next;
} OPENGLSHADER_ShaderSource_t;

typedef struct OPENGL_Shader_s {
    union {
        Shader_t shader;
    } parent;

    GLuint program;
    OPENGLSHADER_ShaderSource_t* listShaderSources;

} OPENGL_Shader_t;

extern Shader_t* OPENGLSHADER_Init(OPENGL_Shader_t* this);

extern void OPENGLSHADER_AddVertexShader(
        OPENGL_Shader_t* this,
        const char* filePath);

extern void OPENGLSHADER_AddGeometryShader(
        OPENGL_Shader_t* this,
        const char* filePath);

extern void OPENGLSHADER_AddFragmentShader(
        OPENGL_Shader_t* this,
        const char* filePath);

extern boolean OPENGLSHADER_CompileShaders(OPENGL_Shader_t* this);

extern GLuint OPENGLSHADER_CreateShader(
        OPENGL_Shader_t* this,
        const char* sourceCode,
        uint32_t sourceLength,
        GLuint shaderType);

#endif /* OPENGLSHADER_H */
