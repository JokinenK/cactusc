/*
 * shader.h
 *
 *  Created on: 17.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef SHADER_H
#define SHADER_H

#include "common/types.h"
#include "common/vector.h"

#define SHADER_INVALID_UNIFORM_HANDLE (-1)

typedef struct Camera_s Camera_t;
typedef struct Light_s Light_t;
typedef struct Transform_s Transform_t;
typedef struct Material_s Material_t;

typedef int32_t UniformHandle_t;

VECTOR_Declare(UniformHandle_t);

typedef struct Shader_s {
    struct Shader_s* (*init)(struct Shader_s* this);

    void (*free)(struct Shader_s* this);

    void (*update)(
            struct Shader_s* this,
            Camera_t* camera,
            Light_t* light,
            Transform_t* transform,
            Material_t* material);

    void (*bind)(struct Shader_s* this);


    void (*bindAttributeLocation)(
            struct Shader_s* this,
            const char* attribName,
            uint32_t location);

    UniformHandle_t (*addUniform)(
            struct Shader_s* this,
            const char* uniformName);

    void (*setUniform1i)(
            struct Shader_s* this,
            UniformHandle_t uniformHandle,
            int32_t value);

    void (*setUniform1f)(
            struct Shader_s* this,
            UniformHandle_t uniformHandle,
            float value);

    void (*setUniform3f)(
            struct Shader_s* this,
            UniformHandle_t uniformHandle,
            float a,
            float b,
            float c);

    void (*setUniform4f)(
            struct Shader_s* this,
            UniformHandle_t uniformHandle,
            float a,
            float b,
            float c,
            float d);

    void (*setUniformMatrix4f)(
            struct Shader_s* this,
            UniformHandle_t uniformHandle,
            const float* matrix);

} Shader_t;

#define SHADER_Init(this) \
    this->init(this)

#define SHADER_Free(this) \
    this->free(this)

#define SHADER_Update(this, camera, light, transform, material) \
    this->update(this, camera, light, transform, material)

#define SHADER_Bind(this) \
    this->bind(this)

#define SHADER_BindAttributeLocation(this, attribName, location) \
    this->bindAttributeLocation(this, attribName, location)

#define SHADER_AddUniform(this, uniformName) \
    this->addUniform(this, uniformName)

#define SHADER_SetUniform1i(this, uniformHandle, value) \
    this->setUniform1i(this, uniformHandle, value)

#define SHADER_SetUniform1f(this, uniformHandle, value) \
    this->setUniform1f(this, uniformHandle, value)

#define SHADER_SetUniform3f(this, uniformHandle, a, b, c) \
    this->setUniform3f(this, uniformHandle, a, b, c)

#define SHADER_SetUniform4f(this, uniformHandle, a, b, c, d) \
    this->setUniform4f(this, uniformHandle, a, b, c, d)

#define SHADER_SetUniformMatrix4f(this, uniformHandle, value) \
    this->setUniformMatrix4f(this, uniformHandle, value)

#endif /* SHADER_H */
