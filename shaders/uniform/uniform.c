/*
 * uniforms.c
 *
 *  Created on: 18.3.2016
 *      Author: Kalle Jokinen
 */

#include "uniform.h"
#include "types/color.h"
#include "math/vec3.h"
#include "math/vec4.h"
#include "math/mat4.h"

/*-----------------------------------------------
 * PUBLIC METHOD DEFINITIONS
 *-----------------------------------------------
 */

Uniform_t* UNIFORM_Init(
        Uniform_t* this,
        Shader_t* shader,
        const char* uniformName)
{
    if (this != NULL && shader != NULL && uniformName != NULL) {
        this->shader = shader;
        this->handle = SHADER_AddUniform(shader, uniformName);
    }

    return this;
}

void UNIFORM_SetInteger(Uniform_t* this, int32_t value)
{
    if (this != NULL) {
        SHADER_SetUniform1i(this->shader, this->handle, value);
    }
}

void UNIFORM_SetFloat(Uniform_t* this, float value)
{
    if (this != NULL) {
        SHADER_SetUniform1f(this->shader, this->handle, value);
    }
}

void UNIFORM_SetVec3(Uniform_t* this, const Vec3_t* value)
{
    if (this != NULL && value != NULL) {
        SHADER_SetUniform3f(
                this->shader,
                this->handle,
                value->x,
                value->y,
                value->z);
    }
}

void UNIFORM_SetVec4(Uniform_t* this, const Vec4_t* value)
{
    if (this != NULL && value != NULL) {
        SHADER_SetUniform4f(
                this->shader,
                this->handle,
                value->x,
                value->y,
                value->z,
                value->w);
    }
}

void UNIFORM_SetColor(Uniform_t* this, const Color_t* value)
{
    if (this != NULL && value != NULL) {
        SHADER_SetUniform3f(
                this->shader,
                this->handle,
                value->red,
                value->green,
                value->blue);
    }
}

void UNIFORM_SetMat4(Uniform_t* this, const Mat4_t* value)
{
    if (this != NULL && value != NULL) {
        SHADER_SetUniformMatrix4f(
                this->shader,
                this->handle,
                MAT4_Data(value));
    }
}
