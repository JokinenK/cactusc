/*
 * uniforms.h
 *
 *  Created on: 18.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef UNIFORMS_H
#define UNIFORMS_H

#include "shaders/shader.h"
#include "common/types.h"

#define UNIFORM_MAX_NAME_LEN (128u)

typedef union Vec3_s Vec3_t;
typedef union Vec4_s Vec4_t;
typedef union Mat4_s Mat4_t;
typedef struct Color_s Color_t;

typedef struct Uniform_s {
    Shader_t* shader;
    UniformHandle_t handle;
} Uniform_t;

extern Uniform_t* UNIFORM_Init(
        Uniform_t* this,
        Shader_t* shader,
        const char* uniformName);

extern void UNIFORM_SetInt(Uniform_t* this, int32_t value);

extern void UNIFORM_SetFloat(Uniform_t* this, float value);

extern void UNIFORM_SetVec3(Uniform_t* this, const Vec3_t* value);

extern void UNIFORM_SetVec4(Uniform_t* this, const Vec4_t* value);

extern void UNIFORM_SetColor(Uniform_t* this, const Color_t* value);

extern void UNIFORM_SetMat4(Uniform_t* this, const Mat4_t* value);

#endif /* UNIFORMS_H */
