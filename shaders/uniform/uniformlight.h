/*
 * uniformlight.h
 *
 *  Created on: 18.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef UNIFORMLIGHT_H
#define UNIFORMLIGHT_H

#include <stdio.h>
#include "uniform.h"
#include "engine/components/light/light.h"

typedef struct UniformLight_s {
    Uniform_t uniformColor;
    Uniform_t uniformPosition;
    Uniform_t uniformIntensity;
    Vec3_t position;
} UniformLight_t;

static inline UniformLight_t* UNIFORMLIGHT_Init(
        UniformLight_t* this,
        Shader_t* shader,
        const char* uniformName)
{
    char uniformNameColor[UNIFORM_MAX_NAME_LEN] = {0};
    char uniformNamePosition[UNIFORM_MAX_NAME_LEN] = {0};
    char uniformNameIntensity[UNIFORM_MAX_NAME_LEN] = {0};

    sprintf(&uniformNameColor[0],     "%s.color",     uniformName);
    sprintf(&uniformNamePosition[0],  "%s.position",  uniformName);
    sprintf(&uniformNameIntensity[0], "%s.intensity", uniformName);

    UNIFORM_Init(&this->uniformColor,     shader, &uniformNameColor[0]);
    UNIFORM_Init(&this->uniformPosition,  shader, &uniformNamePosition[0]);
    UNIFORM_Init(&this->uniformIntensity, shader, &uniformNameIntensity[0]);

    return this;
}

static inline void UNIFORMLIGHT_SetLight(
        UniformLight_t* this,
        const Light_t* light)
{
    UNIFORM_SetColor(
            &this->uniformColor,
            &light->color);

    UNIFORM_SetVec3(
            &this->uniformPosition,
            LIGHT_GetPosition(light, &this->position));

    UNIFORM_SetFloat(
            &this->uniformIntensity,
            light->intensity);
}

#endif /* UNIFORMLIGHT_H */
