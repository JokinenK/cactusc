#ifndef SINGLETON_HPP
#define SINGLETON_HPP

template <class T>
class Singleton
{
private:
    static T* m_instance;

public:
    static T* instance()
    {
        if (!m_instance)
        {
            // Create instance
            m_instance = new T();
        }

        // Return instance
        return m_instance;
    }

    static void release()
    {
        if (m_instance)
        {
            delete m_instance;
            m_instance = 0;
        }
    }
};

template <class T>
T* Singleton<T>::m_instance = NULL;

#endif // SINGLETON_HPP
