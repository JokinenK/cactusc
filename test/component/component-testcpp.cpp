#include <gtest/gtest.h>
#include <stdio.h>
#include <string.h>
#include "../mockups/mock_component.hpp"

#define TEST_GROUP  Component

extern "C" {
    struct Renderer_s {
        uint16_t value;
    };

    struct Shader_s {
        uint16_t value;
    };

    struct Light_s {
        uint16_t value;
    };
}

class TEST_GROUP : public ::testing::Test
{
public:
    NiceMock<MockComponent>* m_mock;
    MOCKComponent_t m_mockComponent;
    Component_t* m_component;

    void SetUp()
    {
        m_mock = new NiceMock<MockComponent>();
        m_component = MOCKCOMPONENT_Init(&m_mockComponent, m_mock);
    }

    void TearDown()
    {
        delete m_mock;
        m_mock = 0;
    }
};

TEST_F(TEST_GROUP, Type)
{
    EXPECT_NE(0, m_component->componentTypeId);
}

TEST_F(TEST_GROUP, Start)
{
    EXPECT_CALL(
            *m_mock,
            Start()
    );

    COMPONENT_ReceiveState(m_component, COMPONENTSTATE_START);
}

TEST_F(TEST_GROUP, Stop)
{
    EXPECT_CALL(
            *m_mock,
            Stop()
    );

    COMPONENT_ReceiveState(m_component, COMPONENTSTATE_STOP);
}

TEST_F(TEST_GROUP, FixedUpdate)
{
    EXPECT_CALL(
            *m_mock,
            FixedUpdate()
    );

    COMPONENT_ReceiveState(m_component, COMPONENTSTATE_FIXEDUPDATE);
}

TEST_F(TEST_GROUP, Update)
{
    EXPECT_CALL(
            *m_mock,
            Update()
    );

    COMPONENT_ReceiveState(m_component, COMPONENTSTATE_UPDATE);
}

TEST_F(TEST_GROUP, LateUpdate)
{
    EXPECT_CALL(
            *m_mock,
            LateUpdate()
    );

    COMPONENT_ReceiveState(m_component, COMPONENTSTATE_LATEUPDATE);
}

TEST_F(TEST_GROUP, Pause)
{
    EXPECT_CALL(
            *m_mock,
            Pause()
    );

    COMPONENT_ReceiveState(m_component, COMPONENTSTATE_PAUSE);
}

TEST_F(TEST_GROUP, Unpause)
{
    EXPECT_CALL(
            *m_mock,
            Unpause()
    );

    COMPONENT_ReceiveState(m_component, COMPONENTSTATE_UNPAUSE);
}

TEST_F(TEST_GROUP, Render)
{
    Renderer_t dummyRenderer = {0xDEAD};
    Shader_t dummyShader = {0xBEEF};
    Light_t dummyLight = {0xABCD};

    EXPECT_CALL(
            *m_mock,
            Render(&dummyRenderer, &dummyShader, &dummyLight)
    );

    COMPONENT_Render(m_component, &dummyRenderer, &dummyShader, &dummyLight);
}
