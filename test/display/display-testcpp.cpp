#include <gtest/gtest.h>
#include <stdio.h>

#include "../mockups/mock_display.hpp"

extern "C" {
    #include "core/display.h"
}

#define TEST_GROUP      DisplayTest

using ::testing::Return;
using ::testing::NiceMock;

class TEST_GROUP : public ::testing::Test
{
public:
    NiceMock<MockDisplay>* m_mock;
    MOCKDisplay_t m_mockDisplay;
    Display_t* m_display;

    void SetUp()
    {
        m_mock = new NiceMock<MockDisplay>();
        m_mockDisplay.mock = m_mock;

        m_display = (Display_t*)&m_mockDisplay;

        MOCKDISPLAY_Init(m_display);
    }

    void TearDown()
    {
        delete m_mock;
        m_mock = 0;
    }
};

TEST_F(TEST_GROUP, TestWidth)
{
    /* Stupid test again mock */
    uint32_t width = 800;

    EXPECT_CALL(
            *m_mock,
            width()
    ).WillOnce(Return(width));

    EXPECT_EQ(m_display->width(m_display), width);
}

TEST_F(TEST_GROUP, TestHeight)
{
    /* Stupid test again mock */
    uint32_t height = 600;

    EXPECT_CALL(
            *m_mock,
            height()
    ).WillOnce(Return(height));

    EXPECT_EQ(m_display->height(m_display), height);
}

TEST_F(TEST_GROUP, TestAspectRatio)
{
    /* Stupid test again mock */
    float ratio = 16.0f / 9.0f;

    EXPECT_CALL(
            *m_mock,
            aspectRatio()
    ).WillOnce(Return(ratio));

    EXPECT_EQ(m_display->aspectRatio(m_display), ratio);
}

