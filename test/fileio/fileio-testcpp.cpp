#include <gtest/gtest.h>
#include <stdio.h>
#include <string.h>

extern "C" {
    #include "common/types.h"
    #include "../../core/fileio.h"

    static const char* testPath = TEST_DATA_PATH;
}

#define TEST_GROUP  FileReader

class TEST_GROUP : public ::testing::Test
{
public:
    void SetUp()
    {
    }

    void TearDown()
    {
    }
};

TEST_F(TEST_GROUP, ReadValidFile)
{
    char filePath[256] = {0};
    sprintf(filePath, "%s/test.txt", testPath);

    uint32_t bufferLength;
    FILEIO_Status_t status;
    char* buffer = FILEIO_ReadFile(filePath, &bufferLength, &status);

    EXPECT_EQ(FILEIO_OK, status);
    EXPECT_TRUE(buffer != NULL);
    EXPECT_TRUE(bufferLength > 0);

    free(buffer);
}

TEST_F(TEST_GROUP, ReadInValidFile)
{
    char filePath[256] = {0};
    sprintf(filePath, "%s/invalid.txt", testPath);

    uint32_t bufferLength;
    FILEIO_Status_t status;
    char* buffer = FILEIO_ReadFile(filePath, &bufferLength, &status);

    EXPECT_EQ(FILEIO_FILE_NOT_FOUND, status);
    EXPECT_TRUE(buffer == NULL);
    EXPECT_TRUE(bufferLength == 0);

    free(buffer);
}
