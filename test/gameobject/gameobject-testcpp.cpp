#include <gtest/gtest.h>
#include <stdio.h>
#include <string.h>
#include "../mockups/mock_component.hpp"

#define TEST_GROUP  GameObject

extern "C" {
    #include "common/linkedlist.h"
    #include "engine/gameobject.h"
    #include "../../engine/customstring.h"

    struct Renderer_s {
        uint16_t value;
    };

    struct Shader_s {
        uint16_t value;
    };

    static char f_tagValue[] = "Hello World!";
}

class TEST_GROUP : public ::testing::Test
{
public:
    CustomString_t m_tag;

    GameObject_t m_gameObjectOne;
    GameObject_t m_gameObjectTwo;
    GameObject_t m_gameObjectThree;

    NiceMock<MockComponent>* m_mockComponentOne;
    NiceMock<MockComponent>* m_mockComponentTwo;
    NiceMock<MockComponent>* m_mockComponentThree;
    MOCKComponent_t m_componentContainerOne;
    MOCKComponent_t m_componentContainerTwo;
    MOCKComponent_t m_componentContainerThree;
    Component_t* m_componentOne;
    Component_t* m_componentTwo;
    Component_t* m_componentThree;

    void SetUp()
    {
        GAMEOBJECT_Init(&m_gameObjectOne, f_tagValue);
        GAMEOBJECT_Init(&m_gameObjectTwo, f_tagValue);
        GAMEOBJECT_Init(&m_gameObjectThree, f_tagValue);

        m_mockComponentOne = new NiceMock<MockComponent>();
        m_mockComponentTwo = new NiceMock<MockComponent>();
        m_mockComponentThree = new NiceMock<MockComponent>();

        m_componentOne = MOCKCOMPONENT_Init(
                &m_componentContainerOne,
                m_mockComponentOne);

        m_componentTwo = MOCKCOMPONENT_Init(
                &m_componentContainerTwo,
                m_mockComponentTwo);

        m_componentThree = MOCKCOMPONENT_Init(
                &m_componentContainerThree,
                m_mockComponentThree);
    }

    void TearDown()
    {
        delete m_mockComponentOne;
        m_mockComponentOne = 0;

        delete m_mockComponentTwo;
        m_mockComponentTwo = 0;

        delete m_mockComponentThree;
        m_mockComponentThree = 0;
    }

    void ExpectStart(NiceMock<MockComponent>* mock)
    {

    }
};

TEST_F(TEST_GROUP, TestParentObjects)
{
    EXPECT_TRUE(GAMEOBJECT_AddComponent(&m_gameObjectOne, m_componentOne));
    EXPECT_TRUE(GAMEOBJECT_AddComponent(&m_gameObjectTwo, m_componentTwo));
    EXPECT_TRUE(GAMEOBJECT_AddComponent(&m_gameObjectThree, m_componentThree));

    EXPECT_TRUE(GAMEOBJECT_AddChild(&m_gameObjectTwo, &m_gameObjectThree));
    EXPECT_TRUE(GAMEOBJECT_AddChild(&m_gameObjectOne, &m_gameObjectTwo));

    EXPECT_EQ(m_componentThree->parent, &m_gameObjectThree);
    EXPECT_EQ(m_componentTwo->parent, &m_gameObjectTwo);
    EXPECT_EQ(m_componentOne->parent, &m_gameObjectOne);

    EXPECT_TRUE(m_gameObjectThree.parent == &m_gameObjectTwo);
    EXPECT_TRUE(m_gameObjectTwo.parent == &m_gameObjectOne);
    EXPECT_TRUE(m_gameObjectOne.parent == NULL);
}

TEST_F(TEST_GROUP, TestSendStateToComponents)
{
    EXPECT_TRUE(GAMEOBJECT_AddComponent(&m_gameObjectOne, m_componentOne));
    EXPECT_TRUE(GAMEOBJECT_AddComponent(&m_gameObjectOne, m_componentTwo));
    EXPECT_TRUE(GAMEOBJECT_AddComponent(&m_gameObjectOne, m_componentThree));

    EXPECT_CALL(*m_mockComponentOne, Start());
    EXPECT_CALL(*m_mockComponentTwo, Start());
    EXPECT_CALL(*m_mockComponentThree, Start());

    GAMEOBJECT_SendState(&m_gameObjectOne, COMPONENTSTATE_START);
}

TEST_F(TEST_GROUP, TestSendStateToChildren)
{
    EXPECT_TRUE(GAMEOBJECT_AddComponent(&m_gameObjectTwo, m_componentOne));
    EXPECT_TRUE(GAMEOBJECT_AddComponent(&m_gameObjectTwo, m_componentTwo));
    EXPECT_TRUE(GAMEOBJECT_AddComponent(&m_gameObjectTwo, m_componentThree));

    EXPECT_TRUE(GAMEOBJECT_AddChild(&m_gameObjectOne, &m_gameObjectTwo));

    EXPECT_CALL(*m_mockComponentOne, Start());
    EXPECT_CALL(*m_mockComponentTwo, Start());
    EXPECT_CALL(*m_mockComponentThree, Start());

    GAMEOBJECT_SendState(&m_gameObjectOne, COMPONENTSTATE_START);
}

TEST_F(TEST_GROUP, TestSendStateToGrandChildren)
{
    EXPECT_TRUE(GAMEOBJECT_AddComponent(&m_gameObjectThree, m_componentOne));
    EXPECT_TRUE(GAMEOBJECT_AddComponent(&m_gameObjectThree, m_componentTwo));
    EXPECT_TRUE(GAMEOBJECT_AddComponent(&m_gameObjectThree, m_componentThree));

    EXPECT_TRUE(GAMEOBJECT_AddChild(&m_gameObjectTwo, &m_gameObjectThree));
    EXPECT_TRUE(GAMEOBJECT_AddChild(&m_gameObjectOne, &m_gameObjectTwo));

    EXPECT_CALL(*m_mockComponentOne, Start());
    EXPECT_CALL(*m_mockComponentTwo, Start());
    EXPECT_CALL(*m_mockComponentThree, Start());

    GAMEOBJECT_SendState(&m_gameObjectOne, COMPONENTSTATE_START);
}

TEST_F(TEST_GROUP, TestSendStateMixed)
{
    EXPECT_TRUE(GAMEOBJECT_AddComponent(&m_gameObjectOne, m_componentOne));
    EXPECT_TRUE(GAMEOBJECT_AddComponent(&m_gameObjectTwo, m_componentTwo));
    EXPECT_TRUE(GAMEOBJECT_AddComponent(&m_gameObjectThree, m_componentThree));

    EXPECT_TRUE(GAMEOBJECT_AddChild(&m_gameObjectTwo, &m_gameObjectThree));
    EXPECT_TRUE(GAMEOBJECT_AddChild(&m_gameObjectOne, &m_gameObjectTwo));

    EXPECT_CALL(*m_mockComponentOne, Start());
    EXPECT_CALL(*m_mockComponentTwo, Start());
    EXPECT_CALL(*m_mockComponentThree, Start());

    GAMEOBJECT_SendState(&m_gameObjectOne, COMPONENTSTATE_START);
}

TEST_F(TEST_GROUP, FindWithTag)
{
    EXPECT_TRUE(GAMEOBJECT_AddChild(&m_gameObjectTwo, &m_gameObjectThree));
    EXPECT_TRUE(GAMEOBJECT_AddChild(&m_gameObjectOne, &m_gameObjectTwo));

    GameObject_t* found = GAMEOBJECT_FindGameObjectsWithTag(
            &m_gameObjectOne,
            f_tagValue);

    ASSERT_TRUE(found != NULL);

    uint16_t counter = 0;
    GameObject_t* elem;
    LINKEDLIST_CountWithField(found, elem, counter, search);

    EXPECT_EQ(counter, 3);
}

TEST_F(TEST_GROUP, TestFindComponent)
{
    EXPECT_TRUE(GAMEOBJECT_AddComponent(&m_gameObjectOne, m_componentOne));

    Component_t dummyComponent;

    ASSERT_NE(
            COMPONENT_Type(MOCKComponent_t),
            dummyComponent.componentTypeId);

    MOCKComponent_t* component = GAMEOBJECT_GetComponent(
                &m_gameObjectOne,
                MOCKComponent_t);

    ASSERT_FALSE(component == NULL);

    ASSERT_EQ(
            COMPONENT_Type(MOCKComponent_t),
            component->parent.component.componentTypeId);
}
