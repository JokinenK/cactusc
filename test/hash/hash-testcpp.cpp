#include <gtest/gtest.h>
#include <stdio.h>
#include <string.h>

extern "C" {
    #include "common/hash.h"
    #include "common/types.h"
}

#define TEST_GROUP  Hash

class TEST_GROUP : public ::testing::Test
{
public:
    void SetUp()
    {
    }

    void TearDown()
    {
    }
};

TEST_F(TEST_GROUP, TestHashGeneration)
{
    char strOne[] = "Hello World1";
    char strTwo[] = "Hello World2";

    uint32_t hashOne = HASH_String(strOne);
    uint32_t hashTwo = HASH_String(strTwo);

    EXPECT_NE(hashOne, hashTwo);
}
