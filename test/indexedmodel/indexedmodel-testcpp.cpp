#include <gtest/gtest.h>
#include <stdio.h>
#include <string.h>

extern "C" {
    #include "common/types.h"
    #include "math/vec2.h"
    #include "math/vec3.h"
    #include "renderer/indexedmodel.h"

    static const char* testPath = TEST_DATA_PATH;
}

#define TEST_GROUP  IndexedObject

class TEST_GROUP : public ::testing::Test
{
public:
    void SetUp()
    {
    }

    void TearDown()
    {
    }
};

TEST_F(TEST_GROUP, ParseValidObject)
{
    uint32_t i = 0;
    char filePath[256] = {0};
    sprintf(filePath, "%s/cube.obj", testPath);

    INDEXEDModel_t* objData = INDEXEDMODEL_ParseObjFile(filePath);

    EXPECT_GT(objData->numIndexes, 0);
    EXPECT_GT(objData->numPositions, 0);

    INDEXEDMODEL_Free(&objData);
}

TEST_F(TEST_GROUP, ParseValidObjectComplex)
{
    uint32_t i = 0;
    char filePath[256] = {0};
    sprintf(filePath, "%s/suzanne.obj", testPath);

    INDEXEDModel_t* objData = INDEXEDMODEL_ParseObjFile(filePath);

    EXPECT_TRUE(objData != NULL);

    INDEXEDMODEL_Free(&objData);
}
