#include <gtest/gtest.h>
#include <stdio.h>
#include <string.h>

extern "C" {
    #include "common/linkedlist.h"
    #include "common/types.h"

    typedef struct Lista_s {
        uint16_t num;
        struct Lista_s* next;
        struct Lista_s* child;
    } Lista_t;

    bool CompareElements(const Lista_t* a, const Lista_t* b)
    {
        return memcmp(a, b, sizeof(Lista_t)) == 0;
    }
}

#define TEST_GROUP  LinkedList

class TEST_GROUP : public ::testing::Test
{
public:
    Lista_t* lista;

    void SetUp()
    {
        lista = NULL;
    }

    void TearDown()
    {
    }
};

TEST_F(TEST_GROUP, Append)
{
    Lista_t a = {1};
    Lista_t b = {2};
    Lista_t c = {3};
    LINKEDLIST_Append(lista, &a);
    LINKEDLIST_Append(lista, &b);
    LINKEDLIST_Append(lista, &c);

    EXPECT_EQ(lista, &a);
    EXPECT_EQ(lista->next, &b);
    EXPECT_EQ(lista->next->next, &c);
}

TEST_F(TEST_GROUP, AppendWithField)
{
    Lista_t a = {1};
    Lista_t b = {2};
    Lista_t c = {3};
    LINKEDLIST_AppendWithField(lista, &a, child);
    LINKEDLIST_AppendWithField(lista, &b, child);
    LINKEDLIST_AppendWithField(lista, &c, child);

    EXPECT_EQ(lista, &a);
    EXPECT_EQ(lista->child, &b);
    EXPECT_EQ(lista->child->child, &c);
}

TEST_F(TEST_GROUP, Prepend)
{
    Lista_t a = {1};
    Lista_t b = {2};
    Lista_t c = {3};
    LINKEDLIST_Prepend(lista, &a);
    LINKEDLIST_Prepend(lista, &b);
    LINKEDLIST_Prepend(lista, &c);

    EXPECT_EQ(lista, &c);
    EXPECT_EQ(lista->next, &b);
    EXPECT_EQ(lista->next->next, &a);
}

TEST_F(TEST_GROUP, ForEach)
{
    Lista_t a = {1};
    Lista_t b = {2};
    Lista_t c = {3};
    LINKEDLIST_Prepend(lista, &a);
    LINKEDLIST_Prepend(lista, &b);
    LINKEDLIST_Prepend(lista, &c);

    uint16_t sum = 0;
    Lista_t* elem;
    LINKEDLIST_ForEach(lista, elem) {
        sum += elem->num;
    }

    EXPECT_EQ(sum, a.num + b.num + c.num);
}

TEST_F(TEST_GROUP, Count)
{
    Lista_t a = {1};
    Lista_t b = {2};
    Lista_t c = {3};
    LINKEDLIST_Prepend(lista, &a);
    LINKEDLIST_Prepend(lista, &b);
    LINKEDLIST_Prepend(lista, &c);

    uint16_t count = 0;
    Lista_t* elem;
    LINKEDLIST_Count(lista, elem, count);

    EXPECT_EQ(count, 3);
}

TEST_F(TEST_GROUP, Search)
{
    Lista_t a = {1};
    Lista_t b = {2};
    Lista_t c = {3};
    LINKEDLIST_Prepend(lista, &a);
    LINKEDLIST_Prepend(lista, &b);
    LINKEDLIST_Prepend(lista, &c);

    uint16_t count = 0;
    Lista_t* result;
    LINKEDLIST_Search(lista, &b, result, CompareElements);

    EXPECT_EQ(result, &b);
}

TEST_F(TEST_GROUP, SearchScalar)
{
    Lista_t a = {1};
    Lista_t b = {2};
    Lista_t c = {3};
    LINKEDLIST_Prepend(lista, &a);
    LINKEDLIST_Prepend(lista, &b);
    LINKEDLIST_Prepend(lista, &c);

    Lista_t* elem;
    LINKEDLIST_SearchScalar(lista, elem, num, b.num);

    EXPECT_EQ(elem, &b);
}

TEST_F(TEST_GROUP, Delete)
{
    Lista_t a = {1};
    Lista_t b = {2};
    Lista_t c = {3};
    LINKEDLIST_Prepend(lista, &a);
    LINKEDLIST_Prepend(lista, &b);
    LINKEDLIST_Prepend(lista, &c);

    LINKEDLIST_Delete(lista, &b);

    EXPECT_EQ(lista, &c);
    EXPECT_EQ(lista->next, &a);

    uint16_t count = 0;
    Lista_t* elem;
    LINKEDLIST_Count(lista, elem, count);

    EXPECT_EQ(count, 2);
}
