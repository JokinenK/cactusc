/*
 * glmhelper.hpp
 *
 *  Created on: 4.4.2016
 *      Author: Kalle Jokinen
 */

#ifndef GLMHELPER_HPP
#define GLMHELPER_HPP

#include "glm/glm.hpp"
#include "glm/gtx/transform.hpp"
#include "glm/gtx/quaternion.hpp"
#include "glm/gtc/type_ptr.hpp"

extern "C" {
    #include "common/types.h"
    #include "math/vec2.h"
    #include "math/vec3.h"
    #include "math/mat4.h"
    #include "math/quat.h"
}

bool operator==(const Vec2_t a, const Vec2_t b) {
    return VEC2_Equals(&a, &b);
}

bool operator==(const Vec3_t a, const Vec3_t b) {
    return VEC3_Equals(&a, &b);
}

bool operator==(const Vec4_t a, const Vec4_t b) {
    return VEC4_Equals(&a, &b);
}

bool operator==(const Quat_t a, const Quat_t b) {
    return QUAT_Equals(&a, &b);
}

bool operator==(const Mat4_t a, const Mat4_t b) {
    return MAT4_Equals(&a, &b);
}

glm::vec2 ToGlm(Vec2_t vec)
{
    return glm::vec2(vec.x, vec.y);
}

glm::vec3 ToGlm(Vec3_t vec)
{
    return glm::vec3(vec.x, vec.y, vec.z);
}

glm::vec4 ToGlm(Vec4_t vec)
{
    return glm::vec4(vec.x, vec.y, vec.z, vec.w);
}

glm::quat ToGlm(Quat_t quat)
{
    return glm::quat(quat.w, quat.x, quat.y, quat.z);
}

glm::mat4 ToGlm(Mat4_t mat)
{
    return glm::make_mat4(&mat.data[0].data[0]);
}

Vec2_t FromGlm(glm::vec2 vec)
{
    Vec2_t result = {vec.x, vec.y};
    return result;
}

Vec3_t FromGlm(glm::vec3 vec)
{
    Vec3_t result = {vec.x, vec.y, vec.z};
    return result;
}

Vec4_t FromGlm(glm::vec4 vec)
{
    Vec4_t result = {vec.x, vec.y, vec.z, vec.w};
    return result;
}

Quat_t FromGlm(glm::quat quat)
{
    Quat_t result = {quat.x, quat.y, quat.z, quat.w};
    return result;
}

template <class T>
Mat4_t FromGlmMatrix(T mat)
{
    Mat4_t result;
    MAT4_CastToMat4(
            &result,
            ARRAY_SIZE(mat),
            ARRAY_SIZE(mat[0]),
            (float*)&mat[0][0]);

    return result;
}

#endif /* GLMHELPER_HPP */
