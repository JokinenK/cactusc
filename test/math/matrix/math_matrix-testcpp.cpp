#include <gtest/gtest.h>
#include <stdio.h>

#include "../common/glmhelper.hpp"

#define TEST_GROUP MathMatrixTest

class TEST_GROUP : public ::testing::Test
{
public:
    uint16_t m_matrixSize;
    float* m_matrixData;

    void SetUp()
    {
        m_matrixData = GetMatrixData(m_matrixSize);
    }

    void TearDown()
    {
    }

    float* GetMatrixData(uint16_t& matrixSize) {
        static float matrixData[16] = {
                0.0f, 0.1f, 0.2f, 0.3f,
                1.0f, 1.1f, 1.2f, 1.3f,
                2.0f, 2.1f, 2.2f, 2.3f,
                3.0f, 3.1f, 3.2f, 3.3f
        };

        matrixSize = ARRAY_SIZE(matrixData);
        return &matrixData[0];
    }
};

TEST_F(TEST_GROUP, TestMatrixRows)
{
    Mat4_t matrix;
    MAT4_Init(&matrix, m_matrixData);

    Vec4_t expectedRow1 = {0.0f, 1.0f, 2.0f, 3.0f};
    Vec4_t expectedRow2 = {0.1f, 1.1f, 2.1f, 3.1f};
    Vec4_t expectedRow3 = {0.2f, 1.2f, 2.2f, 3.2f};
    Vec4_t expectedRow4 = {0.3f, 1.3f, 2.3f, 3.3f};

    Vec4_t actualRow1;
    MAT4_Row(&actualRow1, &matrix, 0);
    EXPECT_EQ(expectedRow1, actualRow1);

    Vec4_t actualRow2;
    MAT4_Row(&actualRow2, &matrix, 1);
    EXPECT_EQ(expectedRow2, actualRow2);

    Vec4_t actualRow3;
    MAT4_Row(&actualRow3, &matrix, 2);
    EXPECT_EQ(expectedRow3, actualRow3);

    Vec4_t actualRow4;
    MAT4_Row(&actualRow4, &matrix, 3);
    EXPECT_EQ(expectedRow4, actualRow4);
}

TEST_F(TEST_GROUP, TestMatrixCols)
{
    Mat4_t matrix;
    MAT4_Init(&matrix, m_matrixData);

    Vec4_t expectedCol1 = {0.0f, 0.1f, 0.2f, 0.3f};
    Vec4_t expectedCol2 = {1.0f, 1.1f, 1.2f, 1.3f};
    Vec4_t expectedCol3 = {2.0f, 2.1f, 2.2f, 2.3f};
    Vec4_t expectedCol4 = {3.0f, 3.1f, 3.2f, 3.3f};


    Vec4_t actualCol1;
    MAT4_Column(&actualCol1, &matrix, 0);
    EXPECT_EQ(expectedCol1, actualCol1);

    Vec4_t actualCol2;
    MAT4_Column(&actualCol2, &matrix, 1);
    EXPECT_EQ(expectedCol2, actualCol2);

    Vec4_t actualCol3;
    MAT4_Column(&actualCol3, &matrix, 2);
    EXPECT_EQ(expectedCol3, actualCol3);

    Vec4_t actualCol4;
    MAT4_Column(&actualCol4, &matrix, 3);
    EXPECT_EQ(expectedCol4, actualCol4);
}

TEST_F(TEST_GROUP, TestMatrixTranspose)
{
    Mat4_t matrix;
    MAT4_Init(&matrix, m_matrixData);

    Vec4_t expectedRow1 = {0.0f, 1.0f, 2.0f, 3.0f};
    Vec4_t expectedRow2 = {0.1f, 1.1f, 2.1f, 3.1f};
    Vec4_t expectedRow3 = {0.2f, 1.2f, 2.2f, 3.2f};
    Vec4_t expectedRow4 = {0.3f, 1.3f, 2.3f, 3.3f};

    Vec4_t actualRow1;
    MAT4_Row(&actualRow1, &matrix, 0);
    EXPECT_EQ(expectedRow1, actualRow1);

    Vec4_t actualRow2;
    MAT4_Row(&actualRow2, &matrix, 1);
    EXPECT_EQ(expectedRow2, actualRow2);

    Vec4_t actualRow3;
    MAT4_Row(&actualRow3, &matrix, 2);
    EXPECT_EQ(expectedRow3, actualRow3);

    Vec4_t actualRow4;
    MAT4_Row(&actualRow4, &matrix, 3);
    EXPECT_EQ(expectedRow4, actualRow4);

    MAT4_Transpose(&matrix, &matrix);

    Vec4_t expectedTransposedRow1 = {0.0f, 0.1f, 0.2f, 0.3f};
    Vec4_t expectedTransposedRow2 = {1.0f, 1.1f, 1.2f, 1.3f};
    Vec4_t expectedTransposedRow3 = {2.0f, 2.1f, 2.2f, 2.3f};
    Vec4_t expectedTransposedRow4 = {3.0f, 3.1f, 3.2f, 3.3f};

    Vec4_t transposedRow1;
    MAT4_Row(&transposedRow1, &matrix, 0);
    EXPECT_EQ(expectedTransposedRow1, transposedRow1);

    Vec4_t transposedRow2;
    MAT4_Row(&transposedRow2, &matrix, 1);
    EXPECT_EQ(expectedTransposedRow2, transposedRow2);

    Vec4_t transposedRow3;
    MAT4_Row(&transposedRow3, &matrix, 2);
    EXPECT_EQ(expectedTransposedRow3, transposedRow3);

    Vec4_t transposedRow4;
    MAT4_Row(&transposedRow4, &matrix, 3);
    EXPECT_EQ(expectedTransposedRow4, transposedRow4);
}

TEST_F(TEST_GROUP, TestMatrixAdd)
{
    Mat4_t matrix1;
    MAT4_Init(&matrix1, m_matrixData);

    Mat4_t matrix2;
    MAT4_Init(&matrix2, m_matrixData);

    static float expectedMatrixData[16];

    uint16_t i = 0;
    for (i = 0; i < m_matrixSize; i++) {
        expectedMatrixData[i] = m_matrixData[i] + m_matrixData[i];
    }

    Mat4_t expectedMatrix;
    MAT4_Init(&expectedMatrix, &expectedMatrixData[0]);

    Mat4_t actualMatrix;
    MAT4_Add(&actualMatrix, &matrix1, &matrix2);

    EXPECT_EQ(expectedMatrix, actualMatrix);
}

TEST_F(TEST_GROUP, TestMatrixSubstract)
{
    Mat4_t matrix1;
    MAT4_Init(&matrix1, m_matrixData);

    Mat4_t matrix2;
    MAT4_Init(&matrix2, m_matrixData);

    static float expectedMatrixData[16];

    uint16_t i = 0;
    for (i = 0; i < m_matrixSize; i++) {
        expectedMatrixData[i] = m_matrixData[i] - m_matrixData[i];
    }

    Mat4_t expectedMatrix;
    MAT4_Init(&expectedMatrix, &expectedMatrixData[0]);

    Mat4_t actualMatrix;
    MAT4_Substract(&actualMatrix, &matrix1, &matrix2);

    EXPECT_EQ(expectedMatrix, actualMatrix);
}

TEST_F(TEST_GROUP, TestMatrixScale)
{
    Mat4_t matrix;
    MAT4_Init(&matrix, m_matrixData);

    static float scale = 2.0f;
    glm::mat4 expectedMatrix = glm::scale(ToGlm(matrix), glm::vec3(scale));

    Mat4_t actualMatrix;
    MAT4_Scale(&actualMatrix, &matrix, scale);

    EXPECT_EQ(FromGlmMatrix(expectedMatrix), actualMatrix);
}

TEST_F(TEST_GROUP, TestMatrixMultiplyWithVector)
{
    Mat4_t matrix;
    MAT4_Init(&matrix, m_matrixData);

    Vec4_t vector1 = {2.0f, 2.0f, 2.0f, 2.0f};
    glm::vec4 expectedVector = ToGlm(matrix) * ToGlm(vector1);

    Vec4_t actualVector;
    VEC4_MultiplyMat4(&actualVector, &matrix, &vector1);

    EXPECT_EQ(FromGlm(expectedVector), actualVector);
}

TEST_F(TEST_GROUP, TestMatrixTranslate)
{
    /**
     * Expected matrix calculated with:
     * http://www.bluebit.gr/matrix-calculator/multiply.aspx
     */
    Mat4_t matrix1;
    MAT4_Init(&matrix1, m_matrixData);

    Mat4_t translatedMatrix;
    MAT4_TranslateXYZ(&translatedMatrix, &matrix1, 1, 2, 3);
}

TEST_F(TEST_GROUP, TestMatrixMultiply)
{
    float matrixDataA[16] = {
            0.263f, 0.012f, 0.430f, 0.262f,
            0.941f, 0.471f, 0.350f, 0.568f,
            0.391f, 0.637f, 0.286f, 0.501f,
            0.467f, 0.728f, 0.727f, 0.694f
    };

    float matrixDataB[16] = {
            0.032f, 0.392f, 0.516f, 0.079f,
            0.432f, 0.551f, 0.543f, 0.295f,
            0.291f, 0.886f, 0.472f, 0.471f,
            0.876f, 0.119f, 0.875f, 0.569f
    };

    float matrixDataC[16] = {
            0.804f, 0.646f, 0.396f, 0.094f,
            0.939f, 0.040f, 0.887f, 0.576f,
            0.482f, 0.434f, 0.805f, 0.732f,
            0.287f, 0.489f, 0.993f, 0.557f
    };

    Mat4_t a;
    MAT4_Init(&a, &matrixDataA[0]);

    Mat4_t b;
    MAT4_Init(&b, &matrixDataB[0]);

    Mat4_t c;
    MAT4_Init(&c, &matrixDataC[0]);

    glm::mat4 expected = ToGlm(a) * ToGlm(b) * ToGlm(c);

    Mat4_t actual;
    MAT4_Multiply(&actual, &b, &c);
    MAT4_Multiply(&actual, &a, &actual);

    EXPECT_EQ(FromGlmMatrix(expected), actual);
}

TEST_F(TEST_GROUP, TestQuatToMatrix)
{
    Quat_t quat = {0.1f, 0.5f, 1.0f, 2.0f};

    Mat4_t expectedMatrix = FromGlmMatrix(glm::mat4_cast(ToGlm(quat)));

    Mat4_t actualMatrix;
    MAT4_RotateMatrixFromQuat(&actualMatrix, &quat);

    EXPECT_EQ(expectedMatrix, actualMatrix);

    MAT4_Print(&actualMatrix, "actualMatrix:");
    MAT4_Print(&expectedMatrix, "expectedMatrix:");
}

TEST_F(TEST_GROUP, TestOuterProductVector2)
{
    Vec2_t a = {0.1f, 0.5f};
    Vec2_t b = {1.0f, 2.0f};

    Mat4_t actual;
    MAT4_OuterProductVec2(&actual, &a, &b);

    Mat4_t expected = FromGlmMatrix(
            glm::outerProduct(ToGlm(a), ToGlm(b)));

    EXPECT_EQ(expected, actual);
}

TEST_F(TEST_GROUP, TestOuterProductVector3)
{
    Vec3_t a = {0.1f, 0.5f, 1.0f};
    Vec3_t b = {1.0f, 2.0f, 3.0f};

    Mat4_t actual;
    MAT4_OuterProductVec3(&actual, &a, &b);

    Mat4_t expected = FromGlmMatrix(
            glm::outerProduct(ToGlm(a), ToGlm(b)));

    EXPECT_EQ(expected, actual);
}

TEST_F(TEST_GROUP, TestOuterProductVector4)
{
    Vec4_t a = {0.1f, 0.5f, 1.0f, 2.0f};
    Vec4_t b = {1.0f, 2.0f, 3.0f, 0.5f};

    Mat4_t actual;
    MAT4_OuterProductVec4(&actual, &a, &b);

    Mat4_t expected = FromGlmMatrix(
            glm::outerProduct(ToGlm(a), ToGlm(b)));

    EXPECT_EQ(expected, actual);
}

/*TEST_F(TEST_GROUP, TestRotateMatrix)
{
    Mat4_t matrix;
    MAT4_Init(&matrix, m_matrixData);

    float angle = DEGREES_TO_RADIANS(45);
    Quat_t rotation = {0.1f, 0.5f, 1.0f, 2.0f};
}*/
