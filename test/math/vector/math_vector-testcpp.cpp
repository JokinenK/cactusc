#include <gtest/gtest.h>
#include <stdio.h>

#include "../common/glmhelper.hpp"

#define TEST_GROUP MathVectorTest

class TEST_GROUP : public ::testing::Test
{
public:
    void SetUp()
    {
    }

    void TearDown()
    {
    }
};

TEST_F(TEST_GROUP, TestVectorAdd)
{
    Vec3_t a = {1.0f, 2.0f, 3.0f};
    Vec3_t b = {1.0f, 2.0f, 3.0f};

    glm::vec3 expected = ToGlm(a) + ToGlm(b);

    Vec3_t actual;
    VEC3_Add(&actual, &a, &b);

    EXPECT_EQ(expected, ToGlm(actual));
}

TEST_F(TEST_GROUP, TestVectorCast)
{
    Vec4_t a = {1.0f, 2.0f, 3.0f, 4.0f};
    Vec3_t b = {1.0f, 2.0f, 3.0f};
    Vec2_t c = {1.0f, 2.0f};

    Vec3_t* d = (Vec3_t*)&a;
    Vec2_t* e = (Vec2_t*)&b;

    EXPECT_EQ(b, *d);
    EXPECT_EQ(c, *e);
}

TEST_F(TEST_GROUP, TestQuatCast)
{
    Quat_t a = {1.0f, 2.0f, 3.0f, 4.0f};
    Vec3_t b = {1.0f, 2.0f, 3.0f};
    Vec3_t* c = (Vec3_t*)&a;

    EXPECT_EQ(b, *c);
}

TEST_F(TEST_GROUP, TestVectorSubstract)
{
    Vec3_t a = {2.0f, 3.0f, 4.0f};
    Vec3_t b = {1.0f, 2.0f, 3.0f};

    glm::vec3 expected = ToGlm(a) - ToGlm(b);

    Vec3_t actual;
    VEC3_Substract(&actual, &a, &b);

    EXPECT_EQ(expected, ToGlm(actual));
}

TEST_F(TEST_GROUP, TestVectorMultiply)
{
    Vec3_t a = {1.0f, 2.0f, 3.0f};
    Vec3_t b = {3.0f, 2.0f, 1.0f};

    glm::vec3 expected = ToGlm(a) * ToGlm(b);

    Vec3_t actual;
    VEC3_Multiply(&actual, &a, &b);

    EXPECT_EQ(expected, ToGlm(actual));
}

TEST_F(TEST_GROUP, TestVectorDivide)
{
    Vec3_t a = {1.0f, 2.0f, 3.0f};
    Vec3_t b = {2.0f, 2.0f, 2.0f};

    glm::vec3 expected = ToGlm(a) / ToGlm(b);

    Vec3_t actual;
    VEC3_Divide(&actual, &a, &b);

    EXPECT_EQ(expected, ToGlm(actual));
}

TEST_F(TEST_GROUP, TestVectorNormalize)
{
    Vec3_t a = {1.0f, 2.0f, 3.0f};

    glm::vec3 expected = glm::normalize(ToGlm(a));

    Vec3_t actual;
    VEC3_Normalize(&actual, &a);

    EXPECT_EQ(expected, ToGlm(actual));
}
