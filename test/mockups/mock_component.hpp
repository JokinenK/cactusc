/*
 * mock_display.h
 *
 *  Created on: 8.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef MOCK_COMPONENT_HPP
#define MOCK_COMPONENT_HPP

#include <gmock/gmock.h>
#include "../common/singleton.hpp"

using ::testing::NiceMock;

class MockComponent;

extern "C" {
    #include "engine/components/component.h"

    struct MOCKComponent_t{
        union {
            Component_t component;
        } parent;

        NiceMock<MockComponent>* mock;
    };
}

class MockComponent
{
public:
    MOCK_METHOD0(Start, void());
    MOCK_METHOD0(Stop, void());
    MOCK_METHOD0(FixedUpdate, void());
    MOCK_METHOD0(Update, void());
    MOCK_METHOD0(LateUpdate, void());
    MOCK_METHOD0(Pause, void());
    MOCK_METHOD0(Unpause, void());
    MOCK_METHOD3(Render, void(
            Renderer_t* renderer,
            Shader_t* shader,
            Light_t* light));
};

void Start(Component_t* handle)
{
    MOCKComponent_t* component = (MOCKComponent_t*)handle;
    component->mock->Start();
}

void Stop(Component_t* handle)
{
    MOCKComponent_t* component = (MOCKComponent_t*)handle;
    component->mock->Stop();
}

void FixedUpdate(Component_t* handle)
{
    MOCKComponent_t* component = (MOCKComponent_t*)handle;
    component->mock->FixedUpdate();
}

void Update(Component_t* handle)
{
    MOCKComponent_t* component = (MOCKComponent_t*)handle;
    component->mock->Update();
}

void LateUpdate(Component_t* handle)
{
    MOCKComponent_t* component = (MOCKComponent_t*)handle;
    component->mock->LateUpdate();
}

void Pause(Component_t* handle)
{
    MOCKComponent_t* component = (MOCKComponent_t*)handle;
    component->mock->Pause();
}

void Unpause(Component_t* handle)
{
    MOCKComponent_t* component = (MOCKComponent_t*)handle;
    component->mock->Unpause();
}

void Render(
        Component_t* handle,
        Renderer_t* renderer,
        Shader_t* shader,
        Light_t* light)
{
    MOCKComponent_t* component = (MOCKComponent_t*)handle;
    component->mock->Render(renderer, shader, light);
}

Component_t* MOCKCOMPONENT_Init(
        MOCKComponent_t* component,
        NiceMock<MockComponent>* mock)
{
    Component_t* parent = NULL;

    if (component) {
        parent = COMPONENT_Init(&component->parent.component, MOCKComponent_t);
        parent->start           = Start;
        parent->stop            = Stop;
        parent->fixedUpdate     = FixedUpdate;
        parent->update          = Update;
        parent->lateUpdate      = LateUpdate;
        parent->pause           = Pause;
        parent->unpause         = Unpause;
        parent->render          = Render;

        component->mock = mock;
    }

    return parent;
}

#endif /* MOCK_COMPONENT_HPP */
