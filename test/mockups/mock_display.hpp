/*
 * mock_display.h
 *
 *  Created on: 8.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef MOCK_DISPLAY_HPP
#define MOCK_DISPLAY_HPP

#include <gmock/gmock.h>
#include "../common/singleton.hpp"

using ::testing::NiceMock;

class MockDisplay;

extern "C" {
    #include "core/display.h"

    struct MOCKDisplay_t{
        Display_t display;
        NiceMock<MockDisplay>* mock;
    };
}

class MockDisplay
{
public:
    MOCK_METHOD3(init, void(char* title, uint32_t width, uint32_t height));
    MOCK_METHOD0(release, void());
    MOCK_METHOD0(swapBuffers, void());
    MOCK_METHOD0(isClosed, boolean());
    MOCK_METHOD0(width, uint32_t());
    MOCK_METHOD0(height, uint32_t());
    MOCK_METHOD0(aspectRatio, float());
};

void Init(Display_t* handle, char* title, uint32_t width, uint32_t height)
{
    MOCKDisplay_t* device = (MOCKDisplay_t*)handle;
    device->mock->init(title, width, height);
}

void Release(Display_t* handle)
{
    MOCKDisplay_t* device = (MOCKDisplay_t*)handle;
    device->mock->release();
}

void SwapBuffers(const Display_t* handle)
{
    MOCKDisplay_t* device = (MOCKDisplay_t*)handle;
    device->mock->swapBuffers();
}

boolean IsClosed(const Display_t* handle)
{
    MOCKDisplay_t* device = (MOCKDisplay_t*)handle;
    return device->mock->isClosed();
}

uint32_t Width(const Display_t* handle)
{
    MOCKDisplay_t* device = (MOCKDisplay_t*)handle;
    return device->mock->width();
}

uint32_t Height(const Display_t* handle)
{
    MOCKDisplay_t* device = (MOCKDisplay_t*)handle;
    return device->mock->height();
}

float AspectRatio(const Display_t* handle)
{
    MOCKDisplay_t* device = (MOCKDisplay_t*)handle;
    return device->mock->aspectRatio();
}

Display_t* MOCKDISPLAY_Init(Display_t* handle)
{
    handle->init = Init;
    handle->release = Release;
    handle->swapBuffers = SwapBuffers;
    handle->isClosed = IsClosed;
    handle->width = Width;
    handle->height = Height;
    handle->aspectRatio = AspectRatio;
    return handle;
}

#endif /* MOCK_DISPLAY_HPP */
