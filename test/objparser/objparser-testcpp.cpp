#include <gtest/gtest.h>
#include <stdio.h>
#include <string.h>

extern "C" {
    #include "common/types.h"
    #include "math/vec3.h"
    #include "math/vec2.h"
    #include "renderer/obj/objparser.h"

    static const char* testPath = TEST_DATA_PATH;
}

#define TEST_GROUP  ObjectParser

class TEST_GROUP : public ::testing::Test
{
public:
    void SetUp()
    {
    }

    void TearDown()
    {
    }
};

TEST_F(TEST_GROUP, ParseValidObject)
{
    uint32_t i = 0;
    char filePath[256] = {0};
    sprintf(filePath, "%s/cube.obj", testPath);

    Model_t* objData = OBJPARSER_Parse(filePath);

    ASSERT_TRUE(objData != NULL);
    EXPECT_EQ(8, VECTOR_Size(objData->vertices));
    EXPECT_EQ(14, VECTOR_Size(objData->texCoords));
    EXPECT_EQ(8, VECTOR_Size(objData->normals));
    EXPECT_EQ(12, VECTOR_Size(objData->triangles));

    OBJPARSER_Free(&objData);
}

TEST_F(TEST_GROUP, ParseValidObjectComplex)
{
    uint32_t i = 0;
    char filePath[256] = {0};
    sprintf(filePath, "%s/suzanne.obj", testPath);

    Model_t* objData = OBJPARSER_Parse(filePath);

    ASSERT_TRUE(objData != NULL);
    EXPECT_NE(0, VECTOR_Size(objData->vertices));
    EXPECT_NE(0, VECTOR_Size(objData->triangles));

    /* Texture coordinates and normals can be zero */
    EXPECT_NE(0, VECTOR_Size(objData->texCoords));
    //EXPECT_NE(0, objData->numNormals);

    OBJPARSER_Free(&objData);
}
