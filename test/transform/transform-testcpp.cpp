#include <gtest/gtest.h>
#include <stdio.h>

extern "C" {
    #include "engine/transform.h"
    #include "common/types.h"
}

#define TEST_GROUP TransformTest

class TEST_GROUP : public ::testing::Test
{
public:
    Transform_t m_transform;

    void SetUp()
    {
        Vec3_t pos = {0, 0, 0};
        Quat_t rot = {0, 0, 0, 1};
        Vec3_t scale = {1, 1, 1};

        TRANSFORM_Init(&m_transform, &pos, &rot, &scale);
    }

    void TearDown()
    {
    }

    void ExpectVectorsToMatch(Vec3_t* a, Vec3_t* b)
    {
        EXPECT_TRUE(VEC3_Equals(a, b));
    }

    void ExpectQuatsToMatch(Quat_t* a, Quat_t* b)
    {
        EXPECT_TRUE(QUAT_Equals(a, b));
    }
};

TEST_F(TEST_GROUP, TestSetPosition)
{
    Vec3_t newPos = {3.0f, 3.0f, 3.0f};
    TRANSFORM_SetPosition(&m_transform, &newPos);
    ExpectVectorsToMatch(&m_transform.position, &newPos);
}

TEST_F(TEST_GROUP, TestSetRotation)
{
    Quat_t newRot = {3.0f, 3.0f, 3.0f, 3.0f};
    TRANSFORM_SetRotation(&m_transform, &newRot);
    ExpectQuatsToMatch(&m_transform.rotation, &newRot);
}

TEST_F(TEST_GROUP, TestSetScale)
{
    Vec3_t newScale = {3.0f, 3.0f, 3.0f};
    TRANSFORM_SetScale(&m_transform, &newScale);
    ExpectVectorsToMatch(&m_transform.scale, &newScale);
}

TEST_F(TEST_GROUP, TestTranslate)
{
    float amount = 0.5f;

    Vec3_t directionOne = {1.0f, 0.0f, 0.0f};
    Vec3_t expectedOne = {0.5f, 0.0f, 0.0f};

    TRANSFORM_Translate(&m_transform, &directionOne, amount);
    ExpectVectorsToMatch(&m_transform.position, &expectedOne);

    Vec3_t directionTwo = {0.0f, 0.0f, 2.0f};
    Vec3_t expectedTwo = {0.5f, 0.0f, 1.0f};

    TRANSFORM_Translate(&m_transform, &directionTwo, amount);
    ExpectVectorsToMatch(&m_transform.position, &expectedTwo);
}
