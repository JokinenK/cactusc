#include <gtest/gtest.h>
#include <stdio.h>
#include <string.h>

extern "C" {
    #include "common/vector.h"
    #include "common/types.h"

    typedef struct Dummy_s {
        uint32_t a;
        uint32_t b;
    } Dummy_t;

    VECTOR_Declare(Dummy_t);

    bool CompareElements(const Dummy_t* a, const Dummy_t* b)
    {
        return memcmp(a, b, sizeof(Dummy_t)) == 0;
    }
}

#define TEST_GROUP  VectorTest

class TEST_GROUP : public ::testing::Test
{
public:
    VECTOR(Dummy_t) m_vector;

    void SetUp()
    {
        VECTOR_Init(Dummy_t, m_vector);
    }

    void TearDown()
    {
        VECTOR_Free(m_vector);
    }
};

TEST_F(TEST_GROUP, Push)
{
    Dummy_t dummy1 = {
            0xDEAD,
            0xBEEF
    };

    Dummy_t* dummy2 = NEW(Dummy_t);
    dummy2->a = 0x1234;
    dummy2->b = 0x5678;

    Dummy_t dummy3 = {
            0x8765,
            0x4321
    };

    VECTOR_Push(Dummy_t, m_vector, dummy1);
    VECTOR_Push(Dummy_t, m_vector, *dummy2);
    VECTOR_Push(Dummy_t, m_vector, dummy3);

    //free(dummy2);

    EXPECT_EQ(0xDEAD, VECTOR_At(m_vector, 0)->a);
    EXPECT_EQ(0xBEEF, VECTOR_At(m_vector, 0)->b);

    EXPECT_EQ(0x1234, VECTOR_At(m_vector, 1)->a);
    EXPECT_EQ(0x5678, VECTOR_At(m_vector, 1)->b);

    EXPECT_EQ(0x8765, VECTOR_At(m_vector, 2)->a);
    EXPECT_EQ(0x4321, VECTOR_At(m_vector, 2)->b);
}

TEST_F(TEST_GROUP, RemoveObjectMiddle)
{
    Dummy_t dummy1 = {
            0xDEAD,
            0xBEEF
    };

    Dummy_t dummy2 = {
            0x1234,
            0x5678
    };

    Dummy_t dummy3 = {
            0x8765,
            0x4321
    };

    Dummy_t dummy4 = {
            0xABCD,
            0xEF01
    };

    VECTOR_Push(Dummy_t, m_vector, dummy1);
    VECTOR_Push(Dummy_t, m_vector, dummy2);
    VECTOR_Push(Dummy_t, m_vector, dummy3);
    VECTOR_Push(Dummy_t, m_vector, dummy4);

    VECTOR_RemoveIndex(m_vector, 1);

    EXPECT_EQ(0xDEAD, VECTOR_At(m_vector, 0)->a);
    EXPECT_EQ(0xBEEF, VECTOR_At(m_vector, 0)->b);

    EXPECT_EQ(0x8765, VECTOR_At(m_vector, 1)->a);
    EXPECT_EQ(0x4321, VECTOR_At(m_vector, 1)->b);

    EXPECT_EQ(0xABCD, VECTOR_At(m_vector, 2)->a);
    EXPECT_EQ(0xEF01, VECTOR_At(m_vector, 2)->b);
}

TEST_F(TEST_GROUP, ForEach)
{
    Dummy_t dummy1 = {
            0xDEAD,
            0xBEEF
    };

    Dummy_t dummy2 = {
            0x1234,
            0x5678
    };

    Dummy_t dummy3 = {
            0x8765,
            0x4321
    };

    Dummy_t dummy4 = {
            0xABCD,
            0xEF01
    };

    VECTOR_Push(Dummy_t, m_vector, dummy1);
    VECTOR_Push(Dummy_t, m_vector, dummy2);
    VECTOR_Push(Dummy_t, m_vector, dummy3);
    VECTOR_Push(Dummy_t, m_vector, dummy4);

    uint32_t expectedSize = 4;
    uint32_t countedSize = 0;

    Dummy_t* item = NULL;
    VECTOR_ForEach(m_vector, item) {
        countedSize++;
    }

    EXPECT_EQ(expectedSize, countedSize);
    EXPECT_EQ(expectedSize, VECTOR_Size(m_vector));
}

TEST_F(TEST_GROUP, Search)
{
    Dummy_t dummy1 = {
            0xDEAD,
            0xBEEF
    };

    Dummy_t dummy2 = {
            0x1234,
            0x5678
    };

    Dummy_t dummy3 = {
            0x8765,
            0x4321
    };

    Dummy_t dummy4 = {
            0xABCD,
            0xEF01
    };

    VECTOR_Push(Dummy_t, m_vector, dummy1);
    VECTOR_Push(Dummy_t, m_vector, dummy2);
    VECTOR_Push(Dummy_t, m_vector, dummy3);

    Dummy_t* item = NULL;

    VECTOR_Find(m_vector, &dummy2, item, CompareElements);
    EXPECT_TRUE(item != NULL);

    VECTOR_Find(m_vector, &dummy4, item, CompareElements);
    EXPECT_TRUE(item == NULL);


}
