#ifndef ATTENUATION_H
#define ATTENUATION_H

typedef struct Attenuation_s {
    float constant;
    float linear;
    float exponent;
} Attenuation_t;

#endif /* ATTENUATION_H */
