#ifndef COLOR_H
#define COLOR_H

#include <stdio.h>
#include "common/types.h"

#define COLOR_BYTES_PER_COLOR   (8u)
#define COLOR_MAX_VALUE         ((1 << COLOR_BYTES_PER_COLOR) - 1)

/*-----------------------------------------------
 * PUBLIC TYPE DEFINITIONS
 *-----------------------------------------------
 */

typedef struct Color_s {
    float alpha;
    float red;
    float green;
    float blue;
} Color_t;

/*-----------------------------------------------
 * PUBLIC VARIABLE DEFINITIONS
 *-----------------------------------------------
 */

static const Color_t COLOR_RED = {
        .red = 1.0f,
        .green = 0.0f,
        .blue = 0.0f,
        .alpha = 1.0f
};

static const Color_t COLOR_GREEN = {
        .red = 0.0f,
        .green = 1.0f,
        .blue = 0.0f,
        .alpha = 1.0f
};

static const Color_t COLOR_BLUE = {
        .red = 0.0f,
        .green = 0.0f,
        .blue = 1.0f,
        .alpha = 1.0f
};

static const Color_t COLOR_WHITE = {
        .red = 1.0f,
        .green = 1.0f,
        .blue = 1.0f,
        .alpha = 1.0f
};

static const Color_t COLOR_BLACK = {
        .red = 0.0f,
        .green = 0.0f,
        .blue = 0.0f,
        .alpha = 1.0f
};

/*-----------------------------------------------
 * PUBLIC METHOD DEFINITIONS
 *-----------------------------------------------
 */

static inline void COLOR_SetRgba(
        Color_t* color,
        uint16_t red,
        uint16_t green,
        uint16_t blue,
        uint16_t alpha)
{
    uint16_t scale = COLOR_MAX_VALUE;
    color->alpha = ((float)alpha / scale);
    color->red   = ((float)red   / scale);
    color->green = ((float)green / scale);
    color->blue  = ((float)blue  / scale);
}

static inline void COLOR_SetRgb(
        Color_t* color,
        uint16_t red,
        uint16_t green,
        uint16_t blue)
{
    uint16_t alpha = COLOR_MAX_VALUE;
    COLOR_SetRgba(color, red, green, blue, alpha);
}

static inline void COLOR_SetHex(
        Color_t* color,
        uint32_t hex)
{
    uint16_t alpha = (hex >> 24) & 0xFF;
    uint16_t red   = (hex >> 16) & 0xFF;
    uint16_t green = (hex >>  8) & 0xFF;
    uint16_t blue  = (hex      ) & 0xFF;

    COLOR_SetRgba(color, red, green, blue, alpha);
}

static inline void COLOR_GetRgba(
        const Color_t* color,
        uint16_t* red,
        uint16_t* green,
        uint16_t* blue,
        uint16_t* alpha)
{
    uint16_t scale = COLOR_MAX_VALUE;
    *alpha = (int16_t)(color->alpha * scale);
    *red   = (int16_t)(color->red * scale);
    *green = (int16_t)(color->green * scale);
    *blue  = (int16_t)(color->blue * scale);
}

static inline void COLOR_GetRgb(
        const Color_t* color,
        uint16_t* red,
        uint16_t* green,
        uint16_t* blue)
{
    uint16_t alpha;
    COLOR_GetRgba(color, red, green, blue, &alpha);
}

static inline uint32_t COLOR_GetHex(const Color_t* color)
{
    uint16_t alpha;
    uint16_t red;
    uint16_t green;
    uint16_t blue;

    COLOR_GetRgba(color, &red, &green, &blue, &alpha);

    return ((uint32_t)alpha << 24)
            | ((uint32_t)red << 16)
            | ((uint32_t)green << 8)
            | ((uint32_t)blue);
}

static inline void COLOR_Print(const Color_t* color)
{
    uint16_t red;
    uint16_t green;
    uint16_t blue;
    uint16_t alpha;
    COLOR_GetRgba(color, &red, &green, &blue, &alpha);

    printf("R: %d G: %d B: %d A: %d\n", red, green, blue, alpha);
}

#endif /* COLOR_H */
