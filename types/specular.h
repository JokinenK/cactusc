#ifndef SPECULAR_H
#define SPECULAR_H

typedef struct Specular_s {
    float intensity;
    float power;
} Specular_t;

#endif /* SPECULAR_H */
