#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "common/types.h"

#define TRIANGLE_NUM_INDEXES    (3)

typedef struct Triangle_s {
    uint32_t position[TRIANGLE_NUM_INDEXES];
    uint32_t texCoord[TRIANGLE_NUM_INDEXES];
    uint32_t normal[TRIANGLE_NUM_INDEXES];
} Triangle_t;

#endif /* TRIANGLE_H */
