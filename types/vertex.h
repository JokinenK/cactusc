#ifndef VERTEX_H
#define VERTEX_H

typedef union Vec3_s Vec3_t;
typedef union Vec2_s Vec2_t;

typedef struct Vertex_s {
    Vec3_t position;
    Vec2_t texCoord;
    Vec3_t normal;
} Vertex_t;

#endif /* VERTEX_H */
